package sibek.balancing.wizard;

import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import sibek.balancing.fragments.BaseFragment;

public class MainStep extends BaseFragment {
    public MainStep() {
    }

    protected void rotateBob(ImageView view, float from, float to) {
        RotateAnimation rAnim;

        FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) view.getLayoutParams();
        rAnim = new RotateAnimation(from, to, view.getWidth() / 2, lp.bottomMargin + (view.getHeight() / 2));

        rAnim.setFillEnabled(true);
        rAnim.setFillAfter(true);
        rAnim.setInterpolator(new LinearInterpolator());
        rAnim.setDuration(0);
        view.startAnimation(rAnim);
    }

    protected void rotateWheel(ImageView view, float from, float to) {
        RotateAnimation rAnim = new RotateAnimation(from, to, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rAnim.setFillEnabled(true);
        rAnim.setFillAfter(true);
        rAnim.setInterpolator(new LinearInterpolator());
        rAnim.setDuration(0);
        view.startAnimation(rAnim);
    }

    protected void rotateBleRuler(ImageView view, float from, float to) {
        RotateAnimation rAnim;

        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) view.getLayoutParams();

        float xPoint = (view.getWidth() * 0.33f) / view.getWidth();
        float yPoint = (view.getHeight() * 0.066f) / view.getHeight(); //расчет центра вращения линейки в пропорциях

        rAnim = new RotateAnimation(from, to, Animation.RELATIVE_TO_SELF, xPoint, Animation.RELATIVE_TO_SELF, yPoint);

        rAnim.setFillEnabled(true);
        rAnim.setFillAfter(true);
        rAnim.setInterpolator(new LinearInterpolator());
        rAnim.setDuration(0);
        view.startAnimation(rAnim);
    }
}