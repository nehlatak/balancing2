package sibek.balancing.wizard.balance;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.otto.Subscribe;

import java.util.HashMap;

import sibek.balancing.App;
import sibek.balancing.R;
import sibek.balancing.tools.BalanceParams;
import sibek.balancing.wizard.MainStep;

public class BalanceStep2 extends MainStep {
    private TextView qep;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.balance_step_2, container, false);
        qep = (TextView) v.findViewById(R.id.qep_angle);

        return v;
    }

    @Subscribe
    public void setParams(HashMap<String, Float> params) {
        if (App.balanceSubstate == BalanceParams.BALANCE_IDLE || App.balanceSubstate >= BalanceParams.BALANCE_DECEL) {
            qep.setText(App.getParamString("wheelangle"));
        }
    }
}
