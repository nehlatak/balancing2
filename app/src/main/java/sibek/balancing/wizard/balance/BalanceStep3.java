package sibek.balancing.wizard.balance;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.squareup.otto.Subscribe;

import java.util.HashMap;

import sibek.balancing.App;
import sibek.balancing.R;
import sibek.balancing.tools.BalanceParams;
import sibek.balancing.tools.TypesParams;
import sibek.balancing.wizard.MainStep;

public class BalanceStep3 extends MainStep {
    private TextView width;
    private TextView offset;
    private TextView diam;
    private Spinner spinner;
    private LayoutInflater inflater;
    private HashMap<Integer, Integer> spinnerImages = new HashMap<>();
    private String[] spinnerStrings = new String[2];
    private int modeValue = BalanceParams.MODE_ALU;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        spinnerStrings[0] = App.getStringFromResources(R.string.disk_type_alu);
        spinnerStrings[1] = App.getStringFromResources(R.string.disk_type_steel);
        spinnerImages.put(BalanceParams.MODE_ALU, R.drawable.alu_disk_default);
        spinnerImages.put(BalanceParams.MODE_STEEL, R.drawable.steel_disk_default);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.inflater = inflater;
        View v = inflater.inflate(R.layout.balance_step_3, container, false);
        width = (TextView) v.findViewById(R.id.width);
        offset = (TextView) v.findViewById(R.id.offset);
        diam = (TextView) v.findViewById(R.id.diam);
        spinner = (Spinner) v.findViewById(R.id.spinner);
        spinner.setAdapter(new SpinnerAdapter(App.getContext(), R.layout.spinner_row, spinnerStrings));

        App.setIntParam("mode", BalanceParams.MODE_ALU);

        offset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                App.openKeyboard("offset", offset.getText().toString(), TypesParams.INT, getFragmentManager());
            }
        });

        width.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                App.openKeyboard("width", width.getText().toString(), TypesParams.FLOAT, getFragmentManager());
            }
        });

        diam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                App.openKeyboard("diam", diam.getText().toString(), TypesParams.FLOAT, getFragmentManager());
            }
        });

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (modeValue != position) {
                    App.setIntParam("mode", position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        return v;
    }

    public class SpinnerAdapter extends ArrayAdapter<String> {

        public SpinnerAdapter(Context context, int textViewResourceId, String[] objects) {
            super(context, textViewResourceId, objects);
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {
            View row = inflater.inflate(R.layout.spinner_row, parent, false);
            TextView label = (TextView) row.findViewById(R.id.spinner_row);

            label.setText(spinnerStrings[position]);
            label.setBackgroundResource(spinnerImages.get(position));

            return row;
        }
    }

    @Subscribe
    public void setParams(HashMap<String, Float> params) {
        width.setText(App.getParamString("width"));
        offset.setText(App.getParamString("offset"));
        diam.setText(App.getParamString("diam"));

        int mode = App.getIntParam("mode");
        if (modeValue != mode && mode != BalanceParams.MODE_STAT) {
            modeValue = mode;
            spinner.setSelection(modeValue);
        }
    }
}