package sibek.balancing.wizard.balance;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.squareup.otto.Subscribe;

import java.util.HashMap;

import sibek.balancing.App;
import sibek.balancing.R;
import sibek.balancing.tools.BalanceParams;
import sibek.balancing.wizard.MainStep;

public class BalanceStep4 extends MainStep {
    private Button weight;
    private ImageView bob;
    private ImageView wheel;
    private ImageView wheelWrapper;
    private ImageView arrowUp;
    private ImageView arrowDown;
    private int wheelangle = 0;
    private int wheelangle0 = 0;
    private int angleDelta = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.balance_step_4, container, false);
        weight = (Button) v.findViewById(R.id.weight);
        bob = (ImageView) v.findViewById(R.id.bob);
        wheel = (ImageView) v.findViewById(R.id.wheel);
        wheelWrapper = (ImageView) v.findViewById(R.id.wheel_wrapper);
        arrowUp = (ImageView) v.findViewById(R.id.arrow_up);
        arrowDown = (ImageView) v.findViewById(R.id.arrow_down);

        return v;
    }

    @Subscribe
    public void setParams(HashMap<String, Float> params) {
        String weightParam = App.getParamString("calweight");
        if (!weight.getText().toString().equals(weightParam)) {
            weight.setText(weightParam);
        }

        wheelangle = App.getIntParam("wheelangle") / 2;
        wheelangle0 = App.getIntParam("wheelangle0") / 2;
        angleDelta = App.getIntParam("wheelangle") - App.getIntParam("wheelangle0");

        int wheelangleMachine = App.getIntParam("wheelangle") / 2;
        int wheelangleMachine0 = App.getIntParam("wheelangle0") / 2;

        if (App.balanceSubstate == BalanceParams.BALANCE_IDLE || App.balanceSubstate >= BalanceParams.BALANCE_DECEL) {
            if (angleDelta == 0) {
                wheelWrapper.setImageResource(R.drawable.left_green);
                weight.setBackground(ContextCompat.getDrawable(App.getContext(), R.drawable.right_item_green));
                bob.setImageResource(R.drawable.plummet_on_wheel_green);
            } else {
                wheelWrapper.setImageResource(R.drawable.left_grey);
                weight.setBackground(ContextCompat.getDrawable(App.getContext(), R.drawable.right_item_grey));
                bob.setImageResource(R.drawable.plummet_on_wheel_red);
            }
            rotateWheel(wheel, wheelangle, wheelangleMachine);
            rotateBob(bob, wheelangle - wheelangle0, wheelangleMachine - wheelangleMachine0);

            if (angleDelta == 0) {
                arrowUp.setImageResource(R.drawable.master_arrow_up_grey);
                arrowDown.setImageResource(R.drawable.master_arrow_down_grey);
                arrowUp.setVisibility(View.VISIBLE);
                arrowDown.setVisibility(View.VISIBLE);
            } else {
                if (angleDelta > 360) {
                    arrowUp.setVisibility(View.VISIBLE);
                    arrowUp.setImageResource(R.drawable.master_arrow_up_green);
                    arrowDown.setVisibility(View.INVISIBLE);
                } else {
                    arrowDown.setImageResource(R.drawable.master_arrow_down_green);
                    arrowDown.setVisibility(View.VISIBLE);
                    arrowUp.setVisibility(View.INVISIBLE);
                }
            }
        }
    }
}