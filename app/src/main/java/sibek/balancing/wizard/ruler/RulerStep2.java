package sibek.balancing.wizard.ruler;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import sibek.balancing.R;
import sibek.balancing.wizard.MainStep;

public class RulerStep2 extends MainStep {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.ruler_step_2, container, false);
        return v;
    }
}
