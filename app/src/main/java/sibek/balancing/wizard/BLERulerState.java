package sibek.balancing.wizard;

/**
 * Created by dracula on 25.10.2016.
 */

public enum BLERulerState {
    RULER_IDLE,             /* Ruler is idle */
    RULER_MEASURE,          /* Active ruler measurement state */
    RULER_WAIT,              /* Waiting for returning in place */
    RULER_MEASURE_L,       /* Active ruler measurement L in layouts 3, 4 */
    RULER_DONTSHOW,  /* Don't show diam (when it's unstable) */
    RULER_SHOW_L1,  /*Show sticker place for left weight */
    RULER_SHOW_L2,  /*Show sticker place for right weight */
    RULER_SHOW_L3,  /*Show sticker place for right 2nd weight */
    RULER_CAL_1,
    RULER_CAL_2,
    RULER_CAL_3
}