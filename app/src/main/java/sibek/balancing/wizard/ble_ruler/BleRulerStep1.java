package sibek.balancing.wizard.ble_ruler;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.otto.Subscribe;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import sibek.balancing.App;
import sibek.balancing.R;
import sibek.balancing.wizard.MainStep;

/**
 * Created by dracula on 25.10.2016.
 */

public class BleRulerStep1 extends MainStep {
    @BindView(R.id.ruler)
    ImageView ruler;

//    private int i = 0;
    private float from = 0;
    private float to = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.ble_ruler_step_2, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    /*@OnClick(R.id.test_rotate)
    public void testRotate() {
        from = i;
        i += 10;
        to = i;

        Log.d("BleRulerStep1", "Math.toDegrees() = " + String.valueOf(Math.toDegrees(6.28)));

        rotateBleRuler(ruler, from, to);
    }*/

    @Subscribe
    public void setParams(HashMap<String, Float> params) {
        to = (float) Math.toDegrees(App.getFloatParam("angle"));
        rotateBleRuler(ruler, from, to);
        from = to;
    }
}