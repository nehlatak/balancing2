package sibek.balancing.router.routes;

import sibek.balancing.router.machine_vars.MachineVarsProvider;
import sibek.balancing.router.machine_vars.VarsFromJSONProvider;
import sibek.balancing.router.machine_vars.VarsParams;

/**
 * Created by dracula on 03.11.2016.
 */

public class Diagnostic extends Route {
    private static final String command = "vval";

    @Override
    public String getCommand() {
        return command;
    }

    @Override
    public VarsParams getVars() {
        return varParams;
    }

    protected void buildVarsParams() {
        //@TODO: строка параметров должна браться командой vlist от платы
        String varsJson = "[ { \"title\":\"Napruga\", \"values\": [ \"+3V3S\", \"+5VL\", \"+24VL\", \"V1V\", \"VUSBL\", \"VBUSL\", \"VRF\" ] }, { \"title\":\"Toki\", \"values\": [\"V1C\", \"V2C\", \"V5C\", \"V7C\" ] }, { \"title\":\"Levizna\", \"values\": [\"3V3EL\", \"RG1F\", \"RG2F\", \"RG3F\", \"V5V\", \"V7V\" ] } ]";

        MachineVarsProvider fromJSONProvider = new VarsFromJSONProvider(varsJson);
        VarsParams varParams = new VarsParams()
                .setCommand(command)
                .setFloatParams(new Integer[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16})
                .setParams(fromJSONProvider.getVarsNames());
        this.varParams = varParams;
    }
}