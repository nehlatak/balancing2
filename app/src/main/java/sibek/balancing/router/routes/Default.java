package sibek.balancing.router.routes;

import sibek.balancing.router.machine_vars.MachineVarsProvider;
import sibek.balancing.router.machine_vars.VarsFromStringProvider;
import sibek.balancing.router.machine_vars.VarsParams;

/**
 * Created by dracula on 03.11.2016.
 */

public class Default extends Route {
    private static final String command = "state";

    @Override
    public String getCommand() {
        return command;
    }

    @Override
    public VarsParams getVars() {
        return varParams;
    }

    protected void buildVarsParams() {
        String commands = "version state substate testmode spiinput drvcurr drvvolt wheelangle wheelspeed rofs rdiam rwidth weight0 angle0 rndweight0 wheelangle0 weight1 angle1 rndweight1 wheelangle1 weight2 angle2 rndweight2 wheelangle2 user msg1 width diam offset split numsp mode layout stick roundmode minweight startmode covermode pedalmode automode clockwise truemode autoalu maxrot diamepsilon drvfreq minfreq accthld angleepsilon rulerhorz rulervert rulerrad rulerofs rulerdiam calweight deccurr decfreq weightdist v0 v1 v2 v3 v4 v5 va0 va1 va2 va3 va4 va5 va6 va7 w0 w1 w2 w3 freqcoeff rstick c0 c1 c2 c3 c4 c5 r0 r1 errors0 errors1 errors2 wheeldist autoaluflag realstick result totaltime idletime balancetime worktime muxval keycal0 cheatepsilon rulercal0 rulercal1 rulercal2 rulercal3 rulercalf cal0 cal1 cal2 cal3 testdrv loaddef loadref saveref passwd start stop enter osc rotate c-meter rrulercal0 rrulercal1 rrulercal2 rrulercal3 rrulercal4 rrulercal5";

        MachineVarsProvider machineVarsProvider = new VarsFromStringProvider(commands);
        VarsParams varParams = new VarsParams()
                .setCommand(command)
                .setFloatParams(new Integer[]{10, 11, 12, 13, 16, 17, 20, 21, 26, 27, 45, 46, 53, 76, 78, 79, 80, 81, 82, 83})
                .setParams(machineVarsProvider.getVarsNames());
        this.varParams = varParams;
    }
}
