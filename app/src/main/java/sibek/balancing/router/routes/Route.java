package sibek.balancing.router.routes;

import sibek.balancing.router.machine_vars.VarsParams;

/**
 * Created by dracula on 03.11.2016.
 */

public abstract class Route {
    protected VarsParams varParams;

    public Route() {
        buildVarsParams();
    }

    public abstract String getCommand();

    public abstract VarsParams getVars();

    protected abstract void buildVarsParams();
}