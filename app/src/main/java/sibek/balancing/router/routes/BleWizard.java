package sibek.balancing.router.routes;

import sibek.balancing.router.machine_vars.MachineVarsProvider;
import sibek.balancing.router.machine_vars.VarsFromStringProvider;
import sibek.balancing.router.machine_vars.VarsParams;

/**
 * Created by dracula on 07.11.2016.
 */

public class BleWizard extends Route {
    private static final String command = "state_rcal";

    @Override
    public String getCommand() {
        return command;
    }

    @Override
    public VarsParams getVars() {
        return varParams;
    }

    @Override
    protected void buildVarsParams() {
        //0 1 6.28 35 0
        String commands = "rulernumber state angle var1 var2";

        MachineVarsProvider machineVarsProvider = new VarsFromStringProvider(commands);
        VarsParams varParams = new VarsParams()
                .setCommand(command)
                .setFloatParams(new Integer[]{2})
                .setParams(machineVarsProvider.getVarsNames());
        this.varParams = varParams;
    }
}
