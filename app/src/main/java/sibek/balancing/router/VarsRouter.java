package sibek.balancing.router;

import android.util.Log;

import sibek.balancing.router.machine_vars.VarsParams;
import sibek.balancing.router.routes.Route;

/**
 * Created by dracula on 13.09.2016.
 */
public class VarsRouter {
    private VarsRouter() {
    }

    private Route route;
    private VarsParams routeVarsParams;
    private static volatile VarsRouter instance;

    public static VarsRouter getInstance() {
        VarsRouter localInstance = instance;
        if (localInstance == null) {
            synchronized (VarsRouter.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new VarsRouter();
                }
            }
        }
        return localInstance;
    }

    public synchronized void setRoute(Route route) {
        Log.d("VarsRouter", "Set new route = " + String.valueOf(route));
        this.route = route;
        routeVarsParams = route.getVars();
    }

    public synchronized Route getRoute() {
        return this.route;
    }

    public synchronized VarsParams getRouteVars() {
        return routeVarsParams;
    }
}