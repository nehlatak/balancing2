package sibek.balancing.router.machine_vars;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by dracula on 13.09.2016.
 */
public class VarsFromStringProvider implements MachineVarsProvider {
    public VarsFromStringProvider() {
    }

    public VarsFromStringProvider(String varsStr) {
        this.varsStr = varsStr.trim();
    }

    private String varsStr;

    public void setVarsStr(String varsStr) {
        this.varsStr = varsStr;
    }

    /*@Override
    public List<String> getVarsNames() {
        List<String> list = new ArrayList<String>();
        int pos = 0, end;
        while ((end = varsStr.indexOf(' ', pos)) >= 0) {
            list.add(varsStr.substring(pos, end));
            pos = end + 1;
        }
        return list;
    }*/

    @Override
    public List<String> getVarsNames() {
        String[] vars = this.varsStr.split(" ");
        List<String> list = new ArrayList<String>(Arrays.asList(vars));
        return list;
    }
}