package sibek.balancing.router.machine_vars;

import java.util.List;

/**
 * Created by dracula on 12.09.2016.
 */
public interface MachineVarsProvider {
    public List<String> getVarsNames();
}