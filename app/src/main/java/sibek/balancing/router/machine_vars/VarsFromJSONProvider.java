package sibek.balancing.router.machine_vars;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dracula on 13.09.2016.
 */
public class VarsFromJSONProvider implements MachineVarsProvider {
    public VarsFromJSONProvider(String varsStr) {
        this.varsStr = varsStr;
    }

    private String varsStr;

    @Override
    public List<String> getVarsNames() {
        try {
            List<String> list = new ArrayList<String>();

            JSONArray json = new JSONArray(varsStr);
            for (int i = 0; i < json.length(); i++) {
                JSONObject tab = json.getJSONObject(i);
                String tabName = tab.getString("title");
                JSONArray jsonArray = tab.getJSONArray("values");

                for (int j = 0; j < jsonArray.length(); j++) {
                    String varName = jsonArray.getString(j);
                    list.add(varName);
                }
            }
            return list;
        } catch (Exception e) {
            return null;
        }
    }
}