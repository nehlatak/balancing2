package sibek.balancing.router.machine_vars;

import java.util.List;

/**
 * Created by dracula on 13.09.2016.
 */
public class VarsParams {
    private String command;
    private List<String> params;
    private Integer[] floatParams;

    public VarsParams setCommand(String command) {
        this.command = command;
        return this;
    }

    public VarsParams setParams(List<String> params) {
        this.params = params;
        return this;
    }

    public VarsParams setFloatParams(Integer[] floatParams) {
        this.floatParams = floatParams;
        return this;
    }

    public Integer[] getFloatParams() {
        return floatParams;
    }

    public String getCommand() {
        return command;
    }

    public List<String> getParams() {
        return params;
    }
}