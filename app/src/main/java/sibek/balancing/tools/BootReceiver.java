package sibek.balancing.tools;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import sibek.balancing.activities.MainActivity;
import sibek.balancing.network.ConnectionService;

public class BootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent startServiceIntent = new Intent(context, ConnectionService.class);
        context.startService(startServiceIntent);

        Intent startActivityIntent = new Intent(context, MainActivity.class);
        startActivityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(startActivityIntent);
    }
}