package sibek.balancing.tools;

public enum TypesParams {
    INT,
    FLOAT,
    LOCAL_PREFERENCE,
    STRING;
}