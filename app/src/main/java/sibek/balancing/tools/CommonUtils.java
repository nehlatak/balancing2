package sibek.balancing.tools;

import android.util.Log;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by dracula on 12.09.2016.
 */
public class CommonUtils {
    public static List<String> getValuesList(String str) {
        List<String> list = new ArrayList<String>();
        int pos = 0, end;
        while ((end = str.indexOf(' ', pos)) >= 0) {
            list.add(str.substring(pos, end));
            pos = end + 1;
        }
        return list;
    }

    public static void printMap(Map mp) {
        Iterator it = mp.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            Log.d("Utils", pair.getKey() + " = " + pair.getValue());
            it.remove(); // avoids a ConcurrentModificationException
        }
    }

    public static float[] arrayConcat(float[] a, float[] b) {
        int aLen = a.length;
        int bLen = b.length;
        float[] c = new float[aLen + bLen];
        System.arraycopy(a, 0, c, 0, aLen);
        System.arraycopy(b, 0, c, aLen, bLen);
        return c;
    }

    public static String byteArrayToHex(byte[] a) {
        StringBuilder sb = new StringBuilder(a.length * 2);
        for (byte b : a)
            sb.append(String.format("%02x ", b & 0xff));
        return sb.toString();
    }
}