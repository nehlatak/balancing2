package sibek.balancing.tools;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import sibek.balancing.App;

public class ShutdownReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        App.setProfileBooleanParam("password", false);
    }
}