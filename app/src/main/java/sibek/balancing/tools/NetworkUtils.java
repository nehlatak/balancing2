package sibek.balancing.tools;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import sibek.balancing.App;

/**
 * Created by dracula on 28.09.2016.
 */

public class NetworkUtils {
    public static boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) App.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = cm.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    public static boolean isWifiConnected() {
        if (isNetworkAvailable()) {
            ConnectivityManager cm
                    = (ConnectivityManager) App.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            return (cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_WIFI);
        }
        return false;
    }

    public static boolean isEthernetConnected() {
        if (isNetworkAvailable()) {
            ConnectivityManager cm
                    = (ConnectivityManager) App.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            return (cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_ETHERNET);
        }
        return false;
    }
}
