package sibek.balancing.tools;

public class BalanceParams {
    private BalanceParams() {
    }

    //    Balance states
    public static final int STATE_IDLE = 0;
    public static final int STATE_BALANCE = 1;
    public static final int STATE_BALANCE_CAL0 = 2;
    public static final int STATE_BALANCE_CAL1 = 3;
    public static final int STATE_BALANCE_CAL2 = 4;
    public static final int STATE_BALANCE_CAL3 = 5;
    public static final int STATE_BALANCE_CAL4 = 6;
    public static final int STATE_RULER = 7;
    public static final int STATE_RULER_CAL0 = 8;
    public static final int STATE_RULER_CAL1 = 9;
    public static final int STATE_RULER_CAL2 = 10;
    public static final int STATE_RULER_CAL3 = 11;
    public static final int STATE_RULER_CAL_FAST = 12;
    public static final int STATE_RULER2 = 13;
    public static final int STATE_PROGRAMMING = 14;
    public static final int STATE_RRULER_3P_CAL0 = 15;
    public static final int STATE_RRULER_3P_CAL1 = 16;
    public static final int STATE_RRULER_3P_CAL2 = 17;
    public static final int STATE_RRULER_D_CAL0 = 18;
    public static final int STATE_RRULER_D_CAL1 = 19;
    public static final int STATE_RRULER_D_CAL2 = 20;
    public static final int STATE_BALANCE_1W_CAL_SAV = 21;
    public static final int STATE_BALANCE_1W_CAL_LD = 22;

    // Balance substates
    public static final int BALANCE_IDLE = 0;
    public static final int BALANCE_WAIT_COVER = 1;
    public static final int BALANCE_START = 2;
    public static final int BALANCE_MIN_SPEED = 3;
    public static final int BALANCE_STABLE_SPEED = 4;
    public static final int BALANCE_MEASURE = 5;
    public static final int BALANCE_WAIT_RESULTS = 6;
    public static final int BALANCE_DECEL = 7;
    public static final int BALANCE_FORWARD = 8;
    public static final int BALANCE_AUTO_ROTATION = 9;
    public static final int BALANCE_STOP = 10;
    public static final int BALANCE_BREAKER = 11;

    // Balance results
    public static final int RESULT_IDLE = 0;
    public static final int RESULT_SUCCESS = 1;
    public static final int RESULT_STOP_CMD = 2;
    public static final int RESULT_STOP_COVER = 3;
    public static final int RESULT_STOP_PEDAL = 4;
    public static final int RESULT_STOP_ERROR = 5;

    // Ruler substates
    public static final int RULER_IDLE = 0;
    public static final int RULER_MEASURE = 1;
    public static final int RULER_WAIT = 2;
    public static final int RULER_MEASURE_L = 3;
    public static final int RULER_DONTSHOW = 4;
    public static final int RULER_SHOW_L1 = 5;
    public static final int RULER_SHOW_L2 = 6;
    public static final int RULER_SHOW_L3 = 7;

    // Disk modes
    public static final int MODE_ALU = 0;
    public static final int MODE_STEEL = 1;
    public static final int MODE_TRUCK = 2;
    public static final int MODE_ABS = 3;
    public static final int MODE_STAT = 4;

    // Weight layouts
    public static final int LAYOUT_1_5 = 0;
    public static final int LAYOUT_2_5 = 1;
    public static final int LAYOUT_1_4 = 2;
    public static final int LAYOUT_1_3 = 3;
    public static final int LAYOUT_2_3 = 4;
    public static final int LAYOUT_2_4 = 5;

    // Static layouts
    public static final int LAYOUT_1 = 0;
    public static final int LAYOUT_2 = 1;
    public static final int LAYOUT_3 = 2;
    public static final int LAYOUT_4 = 3;
    public static final int LAYOUT_5 = 4;

    public static final int BOB_YELLOW = 0;
    public static final int BOB_RED = 1;
    public static final int BOB_GREEN = 2;

    public static final int DIRECTION_LEFT = 0;
    public static final int DIRECTION_RIGHT = 1;
    public static final int DIRECTION_UP = 2;

    public static final int SOUND_IMPORTANT = 1;
    public static final int SOUND_NORMAL = 2;
}
