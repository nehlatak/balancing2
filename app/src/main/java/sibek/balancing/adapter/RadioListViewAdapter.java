package sibek.balancing.adapter;

import android.content.Context;
import android.support.v4.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.ArrayList;

import sibek.balancing.R;
import sibek.balancing.menu.MenuItem;
import sibek.balancing.tools.TypesParams;

public class RadioListViewAdapter extends BaseAdapter {
    private ArrayList<MenuItem> elements;
    private Context context;
    private LayoutInflater mInflater;
    private int selectedIndex = -1;

    public RadioListViewAdapter(Context context, ArrayList<MenuItem> elements) {
        this.context = context;
        this.elements = elements;
        mInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return elements.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    static class ViewHolder {
        TextView title;
        TextView description;
        ImageView icon;
        RadioButton radio;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.radio_listview_row, null);

            viewHolder = new ViewHolder();
            viewHolder.title = (TextView) convertView.findViewById(R.id.list_item_title);
            viewHolder.description = (TextView) convertView.findViewById(R.id.list_item_description);
            viewHolder.icon = (ImageView) convertView.findViewById(R.id.list_item_image);
            viewHolder.radio = (RadioButton) convertView.findViewById(R.id.list_radio);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Pair<TypesParams, Object> elementValue = elements.get(position).getValue();
        MenuItem element = elements.get(position);

        viewHolder.title.setText(element.title);
//        viewHolder.value.setText(String.valueOf(elementValue.second));
        viewHolder.description.setText(element.description);
        viewHolder.icon.setImageResource(element.getIcon());

        if (selectedIndex == position) {
            viewHolder.radio.setChecked(true);
        } else {
            viewHolder.radio.setChecked(false);
        }
        return convertView;
    }

    public void setSelectedIndex(int index) {
        selectedIndex = index;
        notifyDataSetChanged();
    }
}
