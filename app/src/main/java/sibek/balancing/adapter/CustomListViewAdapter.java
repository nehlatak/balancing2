package sibek.balancing.adapter;

import android.content.Context;
import android.support.v4.util.Pair;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import sibek.balancing.App;
import sibek.balancing.R;
import sibek.balancing.menu.MenuItem;
import sibek.balancing.tools.TypesParams;


public class CustomListViewAdapter extends BaseAdapter {
    private ArrayList<MenuItem> elements;
    private Context context;
    private LayoutInflater mInflater;
    private ArrayList<String> voltages = new ArrayList<>();

    public CustomListViewAdapter(Context context, ArrayList<MenuItem> elements) {
        this.context = context;
        this.elements = elements;
        mInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        voltages.add("v0");
        voltages.add("v1");
        voltages.add("v2");
        voltages.add("v4");
        voltages.add("v5");
        voltages.add("va0");
        voltages.add("va1");
        voltages.add("va2");
        voltages.add("va3");
        voltages.add("va4");
        voltages.add("va5");
        voltages.add("va6");
        voltages.add("va7");
    }

    @Override
    public int getCount() {
        return elements.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    static class ViewHolder {
        TextView title;
        TextView description;
        TextView value;
        ImageView icon;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.listview_row, null);

            viewHolder = new ViewHolder();
            viewHolder.title = (TextView) convertView.findViewById(R.id.list_item_title);
            viewHolder.description = (TextView) convertView.findViewById(R.id.list_item_description);
            viewHolder.icon = (ImageView) convertView.findViewById(R.id.list_item_image);
            viewHolder.value = (TextView) convertView.findViewById(R.id.list_item_value);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        MenuItem element = elements.get(position);

//        element.getMachineParam()

        viewHolder.title.setText(element.title);

        if (element.getSubItems().size() == 0) {
            Pair<TypesParams, Object> elementValue = element.getValue();
            if (elementValue != null) {
                if (voltages.contains(element.getMachineParam())) {
                    viewHolder.value.setText(showVoltage(element.getMachineParam()));
                } else {
                    viewHolder.value.setText(String.valueOf(elementValue.second));
                }
            } else {
                if (voltages.contains(element.getMachineParam())) {
                    viewHolder.value.setText(showVoltage(element.getMachineParam()));
                } else {
                    viewHolder.value.setText(App.getParamString(element.getMachineParam()));
                }
            }
        }

        viewHolder.description.setText(element.description);
        viewHolder.icon.setImageResource(element.getIcon());

        ArrayList<MenuItem> subItems = element.getSubItems();

        if (subItems.size() > 0) {
            String machineParam = element.getMachineParam();
            if (App.isMachineParam(machineParam)) {
                int value = App.getIntParam(machineParam);
                MenuItem selectedValue = subItems.get(value);
                element.setIcon(selectedValue.getIcon());
            } else {
                try {
                    int value = Integer.parseInt(App.getProfileStringParam(machineParam));
                    MenuItem selectedValue = subItems.get(value);
                    element.setIcon(selectedValue.getIcon());
                } catch (Exception e) {
                    Log.d("DEBUG", "" + String.valueOf("cant get profile int param!"));
                }
            }
        }

        return convertView;
    }

    public ArrayList<MenuItem> getElements() {
        return elements;
    }

    private String showVoltage(String voltageParam) {
        String value = App.getParamString(voltageParam);
        String result = "";
        if (!value.isEmpty() && value.length() > 2) {
            if (voltageParam.equals("va1")) {
                result += "-";
            } else {
                result += "+";
            }
            value = value.substring(0, value.length() - 2) + "." + value.substring(value.length() - 2, value.length());
            result += value;
            result += "V";
        } else {
            result = "0";
        }
        return result;
    }
}