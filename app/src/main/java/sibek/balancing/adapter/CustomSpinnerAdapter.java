package sibek.balancing.adapter;

import android.content.Context;
import android.support.v4.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import sibek.balancing.R;
import sibek.balancing.menu.MenuItem;
import sibek.balancing.tools.TypesParams;

public class CustomSpinnerAdapter extends BaseAdapter {
    private ArrayList<MenuItem> elements;
    private Context context;
    private LayoutInflater mInflater;

    public CustomSpinnerAdapter(Context context, ArrayList<MenuItem> elements) {
        this.context = context;
        this.elements = elements;
        mInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return elements.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    static class ViewHolder {
        TextView title;
        TextView value;
        TextView description;
        ImageView icon;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.listview_row, null);

            viewHolder = new ViewHolder();
            viewHolder.title = (TextView) convertView.findViewById(R.id.list_item_title);
            viewHolder.value = (TextView) convertView.findViewById(R.id.list_item_value);
            viewHolder.description = (TextView) convertView.findViewById(R.id.list_item_description);
            viewHolder.icon = (ImageView) convertView.findViewById(R.id.list_item_image);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Pair<TypesParams, Object> elementValue = elements.get(position).getValue();

        viewHolder.title.setText(elements.get(position).title);
        viewHolder.value.setText(String.valueOf(elementValue.second));
        viewHolder.description.setText(elements.get(position).description);
        viewHolder.icon.setImageResource(elements.get(position).getIcon());

        return convertView;
    }
}
