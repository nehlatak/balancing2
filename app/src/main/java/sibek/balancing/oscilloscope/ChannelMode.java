package sibek.balancing.oscilloscope;

public final class ChannelMode {
    private ChannelMode() {
    }

    public static final int OSC_NONE = 0;
    public static final int OSC_QEP = 1;
    public static final int OSC_RAW_1DIFF = 2;
    public static final int OSC_IIR_1 = 3;
    public static final int OSC_FIR_1 = 4;
    public static final int OSC_RAW_2DIFF = 5;
    public static final int OSC_IIR_2 = 6;
    public static final int OSC_FIR_2 = 7;
    public static final int OSC_FFT = 8;

    public static final int OSC_RAW1 = 8;
    public static final int OSC_RAW2 = 9;
    public static final int OSC_RAW3 = 10;
    public static final int OSC_RAW4 = 11;
}