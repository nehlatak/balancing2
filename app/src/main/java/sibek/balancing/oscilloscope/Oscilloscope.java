package sibek.balancing.oscilloscope;

import android.util.Log;

import java.util.concurrent.atomic.AtomicBoolean;

import sibek.balancing.App;
import sibek.balancing.network.UdpClient;
import sibek.balancing.network.response_handlers.OSCSamplesHandler;
import sibek.balancing.network.response_handlers.QEPSamplesHandler;

/**
 * Created by dracula on 05.10.2016.
 */
public class Oscilloscope {
    private UdpClient udpConnection;
    private String telnetCommand;
    private static Oscilloscope ourInstance = new Oscilloscope();
    public static volatile OscMode oscMode = OscMode.SAMPLES;
    public static AtomicBoolean isOscActivated = new AtomicBoolean(false); //флаг активности осцила
    public static AtomicBoolean responseFlag = new AtomicBoolean(true);//флаг - принимаем ответы или ждем(при включении режима истории)
    public static final Object sync = new Object();

    public static Oscilloscope getInstance() {
        return ourInstance;
    }

    private Oscilloscope() {
    }

    public static void setMode(OscMode oscMode) {
        Oscilloscope.oscMode = oscMode;
    }

    public static OscMode getOscMode() {
        return oscMode;
    }

    public void disconnect() {
        if (udpConnection != null) {
            setMode(OscMode.SAMPLES);
            Oscilloscope.responseFlag.set(false);
            udpConnection.interrupt();
            try {
                udpConnection.join();
                udpConnection = null;
                Log.d("DEBUG", String.valueOf("UDP thread is stopped"));
                App.sendCommand("osc");
            } catch (InterruptedException e) {
                Log.d("DEBUG", e.toString());
            }
        }
        isOscActivated.set(false);
    }

    private void sendCommand(String command) {
        switch (command) {
            case OscCommands.STOP: {
                telnetCommand = command;
                App.sendCommand(telnetCommand);
            }
            break;
            case OscCommands.CLOCKWISE_ROTATION:
            case OscCommands.CLOCKWISE_ROTATION_O: {
                telnetCommand = command;
                App.sendCommand(telnetCommand);
                telnetCommand = OscCommands.TEST_DRV;
                App.sendCommand(telnetCommand);
            }
            break;
            case OscCommands.ANTICLOCKWISE_ROTATION:
            case OscCommands.ANTICLOCKWISE_ROTATION_O: {
                telnetCommand = command;
                App.sendCommand(telnetCommand);
                telnetCommand = OscCommands.TEST_DRV;
                App.sendCommand(telnetCommand);
            }
            break;
        }
    }

    public void setQEPmode() {
        setMode(OscMode.QEP);
        udpConnection.setResponseHandler(new QEPSamplesHandler());
    }

    public void setOSCmode() {
        setMode(OscMode.SAMPLES);
        udpConnection.setResponseHandler(new OSCSamplesHandler());
    }

    public void connect() {
        udpConnection = new UdpClient();
        udpConnection.setResponseHandler(new OSCSamplesHandler());
        App.sendCommand("osc 2 2");
        udpConnection.start();
    }

    public void rotateClockwise() {
        if (App.oldMachineType) {
            sendCommand(OscCommands.CLOCKWISE_ROTATION_O);
        } else {
            sendCommand(OscCommands.CLOCKWISE_ROTATION);
        }
    }

    public void rotateAntiClockwise() {
        if (App.oldMachineType) {
            sendCommand(OscCommands.ANTICLOCKWISE_ROTATION_O);
        } else {
            sendCommand(OscCommands.ANTICLOCKWISE_ROTATION);
        }
    }

    public void stop() {
        sendCommand(OscCommands.STOP);
    }

    public boolean setChannels(int ch1, int ch2) {
        boolean result = setOscChannels(ch1, ch2);
        if (result) {
            udpConnection.sendStartPacket();
        }
        return result;
    }

    private boolean setOscChannels(int ch1, int ch2) {
        App.sendCommand("osc");
        String telnetCommand = "osc " + ch1 + " " + ch2;
        return (App.sendCommand(telnetCommand));
    }

    public void pause() {
        Oscilloscope.responseFlag.set(false);
    }

    public void play() {
        Oscilloscope.responseFlag.set(true);
    }
}