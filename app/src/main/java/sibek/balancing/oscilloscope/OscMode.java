package sibek.balancing.oscilloscope;

public enum OscMode {
    SAMPLES,
    QEP,
    FFT;
}