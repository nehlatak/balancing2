package sibek.balancing.oscilloscope;

/**
 * Created by dracula on 03.10.2016.
 */

public class Samples {
    private float[] vertices = new float[3];

    public void setVertices(float[] vertices) {
        this.vertices = vertices;
    }

    public float[] getVertices() {
        return vertices;
    }
}