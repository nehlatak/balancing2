package sibek.balancing.oscilloscope.graphic;

import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import sibek.balancing.oscilloscope.FPSCounter;
import sibek.balancing.oscilloscope.Oscilloscope;
import sibek.balancing.view.CustomGLSurfaceView;

public class GLRenderer implements GLSurfaceView.Renderer {
    private CustomGLSurfaceView glView;
    private float scaleFactor = 1;

    // mMVPMatrix is an abbreviation for "Model View Projection Matrix"
    private final float[] mMVPMatrix = new float[16];
    private final float[] mProjectionMatrix = new float[16];
    private final float[] mViewMatrix = new float[16];

    private float translateFactorX = 0;
    private float translateFactorY = 0;

    private int linesCount;

    private Square background;
    private ArrayList<Line> lines = new ArrayList<>(3);
    private ArrayList<LineVertices> linesVertices = new ArrayList<>(3);

    private int layoutWidth;
    private int layoutHeight;

    private FPSCounter counter;

    public GLRenderer(CustomGLSurfaceView glView) {
        this.glView = glView;
    }

    public void setWidthHeight(int width, int height) {
        this.layoutWidth = width;
        this.layoutHeight = height;
    }

    @Override
    public void onSurfaceCreated(GL10 unused, EGLConfig config) {
        GLES20.glDisable(GLES20.GL_DITHER);
        GLES20.glDisable(GLES20.GL_DEPTH_TEST);
        GLES20.glDisable(GLES20.GL_BLEND);
        // Set the background frame color
//        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

        // Set the camera position (View matrix)
        float eyeX = 0.0f;
        float eyeY = 0.0f;
        float eyeZ = 4.0f;
        float centerX = 0.0f;
        float centerY = 0.0f;
        float centerZ = 0.0f;
        float upX = 0.0f;
        float upY = 1.0f;
        float upZ = 0.0f;

        Matrix.setLookAtM(mViewMatrix, 0, eyeX, eyeY, eyeZ, centerX, centerY, centerZ, upX, upY, upZ);

        background = new Square();
        background.setupImage();

        for (int i = 0; i < 3; i++) {
            lines.add(new Line());
            linesVertices.add(new LineVertices());
        }

        for (int i = 0; i < linesVertices.size(); i++) {
            switch (i) {
                case 0:
                    lines.get(i).setColor(1.0f, 0.0f, 0.0f, 0.0f);
                    break;
                case 1:
                    lines.get(i).setColor(0.0f, 1.0f, 0.0f, 0.0f);
                    break;
                case 2:
                    lines.get(i).setColor(0.0f, 0.0f, 1.0f, 0.0f);
                    break;
                default:
                    lines.get(i).setColor(1.0f, 0.0f, 0.0f, 0.0f);
                    break;
            }
        }
        recalcOffsets();
        counter = new FPSCounter();
    }

    public void recalcOffsets() {
        switch (Oscilloscope.getOscMode()) {
            case SAMPLES:
                linesCount = 2;
                break;
            case FFT:
            case QEP:
                linesCount = 3;
                break;
        }

        int halfHeight = layoutHeight / 2;
        int linesStep = halfHeight / (linesCount + 1) * 2;
        for (int i = 0; i < linesVertices.size(); i++) {
            int offset = layoutHeight / 2 - linesStep * (i + 1);
            linesVertices.get(i).setVerticesOffset(offset);
        }
    }

    @Override
    public void onDrawFrame(GL10 unused) {
//        Log.d("GLRenderer", "onDrawFrame");
        // Draw background color
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);

//        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

        // Calculate the projection and view transformation
        Matrix.multiplyMM(mMVPMatrix, 0, mProjectionMatrix, 0, mViewMatrix, 0);

        background.draw(mMVPMatrix);

        if (Oscilloscope.responseFlag.get()) {
            for (int i = 0; i < linesCount; i++) {
                Line line = lines.get(i);
                line.setVertices(linesVertices.get(i).getVertices())
                        .setTranslate(translateFactorX, translateFactorY)
                        .setScale(scaleFactor)
                        .draw(mMVPMatrix);
            }
        } else {
            for (int i = 0; i < linesCount; i++) {
                Line line = lines.get(i);
                line.setVertices(linesVertices.get(i).buildHistory().getHistoryVertices())
                        .setTranslate(translateFactorX, translateFactorY)
                        .setScale(scaleFactor)
                        .draw(mMVPMatrix);
            }
        }

//        counter.logFrame();
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height) {
        // Adjust the viewport based on geometry changes,
        // such as screen rotation
        GLES20.glViewport(0, 0, width, height);

        // this projection matrix is applied to object coordinates
        // in the onDrawFrame() method
//        Matrix.frustumM(mProjectionMatrix, 0, 0, width, -height / 2, height / 2, 0, 100);

        Matrix.orthoM(mProjectionMatrix, 0, 0, width, -height / 2, height / 2, 0, 100);
    }

    public void addVertices(float[] vertices) {
        if (linesVertices.size() == 3) {
            switch (Oscilloscope.getOscMode()) {
                case SAMPLES:
                    if (vertices.length == 2) {
                        for (int i = 0; i < 2; i++) {
                            linesVertices.get(i).addVertex(vertices[i]);
                        }
                    }
                    break;
                case FFT:
                case QEP:
                    if (vertices.length == 3) {
                        for (int i = 0; i < 3; i++) {
                            linesVertices.get(i).addVertex(vertices[i]);
                        }
                    }
                    break;
            }
        }
    }

    public void setTranslateFactor(float translateFactorX, float translateFactorY) {
        this.translateFactorX += translateFactorX / scaleFactor;
        this.translateFactorY -= translateFactorY / scaleFactor;
    }

    public void setScaleFactor(float scaleFactor) {
        this.scaleFactor = scaleFactor;
    }
}