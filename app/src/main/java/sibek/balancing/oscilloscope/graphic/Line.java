package sibek.balancing.oscilloscope.graphic;

import android.opengl.GLES20;
import android.opengl.Matrix;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.Arrays;

import sibek.balancing.oscilloscope.OscMode;
import sibek.balancing.oscilloscope.Oscilloscope;

public class Line {
    private float translateX = 0;
    private float translateY = 0;

    private float scaleFactor = 1;

    private final float[] scaleMatrix = new float[16];
    private final float[] translateMatrix = new float[16];
    private float[] mvpMatrix = new float[16];
    private FloatBuffer VertexBuffer;

    protected int GlProgram;
    protected int PositionHandle;
    protected int ColorHandle;
    protected int MVPMatrixHandle;

    // number of coordinates per vertex in this array
    static final int COORDS_PER_VERTEX = 3;

    //    private int VertexCount = LineCoords.length / COORDS_PER_VERTEX;
    private int VertexCount;
    private int vertexArrayLength;
    private final int VertexStride = COORDS_PER_VERTEX * 4; // 4 bytes per vertex

    // Set color with red, green, blue and alpha (opacity) values
    float color[] = {0.0f, 0.0f, 0.0f, 1.0f};

    private final String vertexShaderCode =
            // This matrix member variable provides a hook to manipulate
            // the coordinates of the objects that use this vertex shader
            "uniform mat4 uMVPMatrix;" +
                    "attribute vec4 vPosition;" +
                    "void main() {" +
                    // the matrix must be included as a modifier of gl_Position
                    // Note that the uMVPMatrix factor *must be first* in order
                    // for the matrix multiplication product to be correct.
                    "  gl_Position = uMVPMatrix * vPosition;" +
                    "}";

    private final String fragmentShaderCode =
            "precision mediump float;" +
                    "uniform vec4 vColor;" +
                    "void main() {" +
                    "  gl_FragColor = vColor;" +
                    "}";

    public Line() {
        int vertexShader = GLUtils.loadShader(GLES20.GL_VERTEX_SHADER, vertexShaderCode);
        int fragmentShader = GLUtils.loadShader(GLES20.GL_FRAGMENT_SHADER, fragmentShaderCode);
        GlProgram = GLES20.glCreateProgram();             // create empty OpenGL ES Program
        GLES20.glAttachShader(GlProgram, vertexShader);   // add the vertex shader to program
        GLES20.glAttachShader(GlProgram, fragmentShader); // add the fragment shader to program
        GLES20.glLinkProgram(GlProgram);                  // creates OpenGL ES program executables
    }

    public Line setVertices(float[] vertices) {
        vertexArrayLength = vertices.length;
        VertexCount = vertexArrayLength / COORDS_PER_VERTEX;

        // initialize vertex byte buffer for shape coordinates
        ByteBuffer bb = ByteBuffer.allocateDirect(
                // (number of coordinate values * 4 bytes per float)
                vertexArrayLength * 4);
        // use the device hardware's native byte order
        bb.order(ByteOrder.nativeOrder());

        // create a floating point buffer from the ByteBuffer
        VertexBuffer = bb.asFloatBuffer();
        VertexBuffer.clear();
        VertexBuffer.put(vertices);
        VertexBuffer.position(0);
        return this;
    }

    public Line setColor(float red, float green, float blue, float alpha) {
        color[0] = red;
        color[1] = green;
        color[2] = blue;
        color[3] = alpha;
        return this;
    }

    public Line draw(float[] mvMatrix) {
        mvpMatrix = Arrays.copyOf(mvMatrix, mvMatrix.length);
        Matrix.setIdentityM(scaleMatrix, 0);
        if (Oscilloscope.getOscMode() == OscMode.QEP) {
            Matrix.scaleM(scaleMatrix, 0, scaleFactor, 1, 0.0f);
        } else {
            Matrix.scaleM(scaleMatrix, 0, scaleFactor, scaleFactor, 0.0f);
        }
        Matrix.multiplyMM(mvpMatrix, 0, mvpMatrix, 0, scaleMatrix, 0);

        Matrix.setIdentityM(translateMatrix, 0);

        if (Oscilloscope.getOscMode() == OscMode.QEP) {
            Matrix.translateM(translateMatrix, 0, translateX, 0.0f, 0.0f);
        } else {
            Matrix.translateM(translateMatrix, 0, translateX, translateY, 0.0f);
        }

        Matrix.multiplyMM(mvpMatrix, 0, mvpMatrix, 0, translateMatrix, 0);

        // Add program to OpenGL ES environment
        GLES20.glUseProgram(GlProgram);

        // get handle to vertex shader's vPosition member
        PositionHandle = GLES20.glGetAttribLocation(GlProgram, "vPosition");

        // Enable a handle to the triangle vertices
        GLES20.glEnableVertexAttribArray(PositionHandle);

        // Prepare the triangle coordinate data
        GLES20.glVertexAttribPointer(PositionHandle, COORDS_PER_VERTEX,
                GLES20.GL_FLOAT, false,
                VertexStride, VertexBuffer);

        // get handle to fragment shader's vColor member
        ColorHandle = GLES20.glGetUniformLocation(GlProgram, "vColor");

        // Set color for drawing the triangle
        GLES20.glUniform4fv(ColorHandle, 1, color, 0);

        // get handle to shape's transformation matrix
        MVPMatrixHandle = GLES20.glGetUniformLocation(GlProgram, "uMVPMatrix");
        GLUtils.checkGlError("glGetUniformLocation");

        // Apply the projection and view transformation
        GLES20.glUniformMatrix4fv(MVPMatrixHandle, 1, false, mvpMatrix, 0);
        GLUtils.checkGlError("glUniformMatrix4fv");

        GLES20.glLineWidth(5);
        // Draw the triangle
        GLES20.glDrawArrays(GLES20.GL_LINE_STRIP, 0, VertexCount);

        // Disable vertex array
        GLES20.glDisableVertexAttribArray(PositionHandle);
        return this;
    }

    public Line setTranslate(float translateX, float translateY) {
        this.translateX = translateX;
        this.translateY = translateY;
        return this;
    }

    public Line setScale(float scaleFactor) {
        this.scaleFactor = scaleFactor;
        return this;
    }
}