package sibek.balancing.oscilloscope.graphic;

import java.util.ArrayDeque;
import java.util.ArrayList;

import sibek.balancing.oscilloscope.Oscilloscope;

public class LineVertices {
    private ArrayList<Float> queueY = new ArrayList<>();
    private ArrayDeque<Float> history = new ArrayDeque<>();
    private float[] vertices = new float[3];
    private int lastChangedItem = 0;
    private int queueSize = 2048;
    private int historySize;
    private float[] historyVertices;
    private float incrementX = 1;
    private int divideFactor = 10;

    public LineVertices() {
        switch (Oscilloscope.getOscMode()) {
            case SAMPLES:
                historySize = queueSize * 5;
                historyVertices = new float[historySize * 3];
                break;
            case FFT:
            case QEP:
                historySize = queueSize * 5;
                historyVertices = new float[historySize * 3];
                break;
            default:
                historySize = queueSize * 5;
                historyVertices = new float[historySize * 3];
                break;
        }
    }

    public void setVerticesOffset(int verticesOffset) {
        this.verticesOffset = verticesOffset;
    }

    private int verticesOffset = 0;

    private synchronized void buildVertices() {
        float x = 0;
        float z = 0.0f;
        int i = 0;

        if (lastChangedItem == queueSize - 1) {
            vertices[lastChangedItem * 3 + 1] = queueY.get(lastChangedItem);
        } else {
            vertices = new float[queueY.size() * 3];
            for (Float y : queueY) {
                vertices[i] = x;
                vertices[i + 1] = y;
                vertices[i + 2] = z;
                x += incrementX;
                i += 3;
            }
        }
    }

    public float[] getVertices() {
        buildVertices();
        return vertices;
    }

    long startTime = System.nanoTime();
    long endTime = System.nanoTime();
    long duration = 0;

    public synchronized void addVertex(float vertex) {
//        Log.d("LineVertices", "addVertex()");
        vertex = vertex / divideFactor;
        vertex += verticesOffset;

        /*if (lastChangedItem == 0) {
            endTime = System.nanoTime();
            duration = (endTime - startTime);
            Log.d("LineVertices", "duration = " + String.valueOf(duration/1000000));
            startTime = System.nanoTime();
        }*/

        if (queueY.size() == queueSize) {
            queueY.set(lastChangedItem, vertex);
            lastChangedItem++;
            if (lastChangedItem == queueSize) {
                lastChangedItem = 0;
            }
        } else {
            queueY.add(vertex);
        }

        if (history.size() == historySize) {
            history.pollFirst();
            history.add(vertex);
        } else {
            history.add(vertex);
        }
    }

    public synchronized LineVertices buildHistory() {
        float x = 0;
        float z = 0.0f;
        int i = 0;

        for (Float y : history) {
            historyVertices[i] = x;
            historyVertices[i + 1] = y;
            historyVertices[i + 2] = z;
            x += incrementX;
            i += 3;
        }
        return this;
    }

    public float[] getHistoryVertices() {
        return historyVertices;
    }
}
