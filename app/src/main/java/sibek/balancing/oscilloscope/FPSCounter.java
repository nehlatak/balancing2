package sibek.balancing.oscilloscope;

import android.util.Log;

public class FPSCounter {
    static long startTime = System.nanoTime();
    static int frames = 0;

    public void logFrame() {
        frames++;
        if (System.nanoTime() - startTime >= 1000000000) {
            Log.d("FPS", "fps: " + frames);
            frames = 0;
            startTime = System.nanoTime();
        }
    }
}