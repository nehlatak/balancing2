package sibek.balancing.oscilloscope;

import sibek.balancing.App;

public class OscCommands {
    private OscCommands() {
    }

    public static final String START = "start";
    public static final String STOP = "stop";
    public static final String EMPTY_CALIBRATION = "keycal0";

    public static final String CLOCKWISE_ROTATION_O = "clockwise 1"; //TODO: убрать после полного отказа от старой платы
    public static final String CLOCKWISE_ROTATION = "testdrv";
    public static final String ANTICLOCKWISE_ROTATION_O = "clockwise 0"; //TODO: убрать после полного отказа от старой платы
    public static final String ANTICLOCKWISE_ROTATION = "testdrvc";

    public static final String OSC_STOP = "osc 0 0";
    public static final String TEST_DRV = "testdrv";
}