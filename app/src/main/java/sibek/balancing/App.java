package sibek.balancing;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.util.Pair;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.orm.SugarApp;
import com.orm.query.Condition;
import com.orm.query.Select;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import com.squareup.otto.ThreadEnforcer;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.zip.CRC32;
import java.util.zip.Checksum;

import sibek.balancing.fragments.KeyboardFragment;
import sibek.balancing.fragments.MessageFragment;
import sibek.balancing.model.BalancingError;
import sibek.balancing.model.ResultStatistic;
import sibek.balancing.model.TimeStatistic;
import sibek.balancing.network.ConnectionService;
import sibek.balancing.network.MachineCommands;
import sibek.balancing.network.response_handlers.CurrentMachineStateHandler;
import sibek.balancing.router.VarsRouter;
import sibek.balancing.router.routes.Default;
import sibek.balancing.router.routes.Route;
import sibek.balancing.tools.AppEvents;
import sibek.balancing.tools.BalanceParams;
import sibek.balancing.tools.TypesParams;

public class App extends SugarApp {
    public static volatile boolean connectFlag = false; //принимаем сэмплы от осцила или выводим историю
    public static Bus bus;
    /*public static final String[] params = {
            "version", "state", "substate", "testmode", "spiinput", "drvcurr", "drvvolt", "wheelangle", "wheelspeed", "rofs",
            "rdiam", "rwidth", "weight0", "angle0", "rndweight0", "wheelangle0", "weight1", "angle1", "rndweight1", "wheelangle1",
            "weight2", "angle2", "rndweight2", "wheelangle2", "user", "msg1", "width", "diam", "offset", "split",
            "numsp", "mode", "layout", "stick", "roundmode", "minweight", "startmode", "covermode", "pedalmode", "automode",
            "clockwise", "truemode", "autoalu", "maxrot", "diamepsilon", "drvfreq", "minfreq", "accthld", "angleepsilon", "rulerhorz",
            "rulervert", "rulerrad", "rulerofs", "rulerdiam", "calweight", "deccurr", "decfreq", "weightdist", "v0", "v1",
            "v2", "v3", "v4", "v5", "va0", "va1", "va2", "va3", "va4", "va5",
            "va6", "va7", "w0", "w1", "w2", "w3", "freqcoeff", "rstick", "c0", "c1",
            "c2", "c3", "c4", "c5", "r0", "r1", "errors0", "errors1", "errors2", "wheeldist",
            "autoaluflag", "realstick", "result", "totaltime", "idletime", "balancetime", "worktime", "muxval", "keycal0", "cheatepsilon",
            "rulercal0", "rulercal1", "rulercal2", "rulercal3", "rulercalf", "cal0", "cal1", "cal2", "cal3", "testdrv",
            "loaddef", "loadref", "saveref", "passwd", "start", "stop", "enter", "osc", "rotate", "c-meter",
            "rrulercal0", "rrulercal1", "rrulercal2", "rrulercal3", "rrulercal4", "rrulercal5"
    };*/
    public static boolean popupFlag = false;
    public static float balanceErrors0 = 0;
    public static float balanceErrors1 = 0;
    public static float balanceErrors2 = 0;
    public static boolean zeroFlag = true;
    public static boolean errorFlag = false;
    public static int offsetValue = 0;
    public static float widthValue = 0;
    public static float diamValue = 0;
    public static int balanceState = BalanceParams.STATE_IDLE;
    public static int balanceSubstate = BalanceParams.BALANCE_IDLE;
    public static final int OVERLOAD_WEIGHT = 300;
    public static boolean oldMachineType = false; //TODO: хак - условие обозначающее старый тип платы. Влияет на обработку ответов от станка

    private static VarsRouter router = VarsRouter.getInstance();
    private static boolean errorFlag0 = false;
    private static boolean errorFlag1 = false;
    private static boolean errorFlag2 = false;
    private int newBalanceErrors0 = 0;
    private int newBalanceErrors1 = 0;
    private int newBalanceErrors2 = 0;
    private static App singleton;
    private static HashMap<String, Pair> paramsMap = new HashMap<>();
    private static Context context;
    private static ConnectionService connectionService;
    private final ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            connectionService = ((ConnectionService.LocalBinder) service).getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            connectionService = null;
        }
    };
    private static MessageFragment messageFragment = new MessageFragment();
    private static KeyboardFragment keyboard = new KeyboardFragment();
    private static int numErrors = 0;
    private int balanceResult = 1;
    private static MediaPlayer player = new MediaPlayer();
    private int statsTime = 0;
    private static int soundCount = 0;
    private Route defRoute = new Default();

    @Subscribe
    public void setParams(HashMap<String, Pair> params) {
        Log.d("App", "CurRoute = " + VarsRouter.getInstance().getRoute());
        String curCommand = router.getRoute().getCommand();

        paramsMap = params;

        if (curCommand.equals("state")) {
            balanceState = App.getIntParam("state");
            balanceSubstate = App.getIntParam("substate");
            numErrors = 0;

            newBalanceErrors0 = App.getIntParam("errors0");
            newBalanceErrors1 = App.getIntParam("errors1");
            newBalanceErrors2 = App.getIntParam("errors2");

            if (balanceErrors0 != newBalanceErrors0) {
                Log.d("DEBUG", "newBalanceErrors0 = " + String.valueOf(newBalanceErrors0));
            }
            if (balanceErrors1 != newBalanceErrors1) {
                Log.d("DEBUG", "newBalanceErrors1 = " + String.valueOf(newBalanceErrors1));
            }
            if (balanceErrors2 != newBalanceErrors2) {
                Log.d("DEBUG", "newBalanceErrors2 = " + String.valueOf(newBalanceErrors2));
            }

            errorFlag0 = processErrors(newBalanceErrors0, balanceErrors0, 1);
            errorFlag1 = processErrors(newBalanceErrors1, balanceErrors1, 33);
            errorFlag2 = processErrors(newBalanceErrors2, balanceErrors2, 65);
            errorFlag = (errorFlag0 || errorFlag1 || errorFlag2);

            balanceErrors0 = newBalanceErrors0;
            balanceErrors1 = newBalanceErrors1;
            balanceErrors2 = newBalanceErrors2;

            offsetValue = App.getIntParam("offset");
            widthValue = App.getFloatParam("width");
            diamValue = App.getFloatParam("diam");
            zeroFlag = (offsetValue == 0 || widthValue == 0 || diamValue == 0);

            int newBalanceResult = App.getIntParam("result");
            if (newBalanceResult != BalanceParams.RESULT_IDLE && balanceResult == BalanceParams.RESULT_IDLE) {
                (new Thread() {
                    public void run() {
                        int time = (int) (System.currentTimeMillis() / 1000L);
                        int user = App.getIntParam("user");
                        int mode = App.getIntParam("mode");
                        int layout = unpackLayout(App.getIntParam("layout"), mode);
                        float width = App.getFloatParam("width");
                        float diam = App.getFloatParam("diam");
                        int offset = App.getIntParam("offset");
                        int splitMode = App.getIntParam("split");
                        int numsp = App.getIntParam("numsp");
                        float weight0 = App.getFloatParam("weight0");
                        float weight1 = App.getFloatParam("weight1");
                        float weight2 = App.getFloatParam("weight2");
                        float angle0 = App.getFloatParam("angle0");
                        float angle1 = App.getFloatParam("angle1");
                        float angle2 = App.getFloatParam("angle2");
                        int result = App.getIntParam("result");

                        ResultStatistic statisticRow = new ResultStatistic(time, user, mode, layout, width, diam, offset, splitMode, numsp, weight0, weight1, weight2, angle0, angle1, angle2, result);
                        statisticRow.save();
                    }
                }).start();
            }
            balanceResult = newBalanceResult;

            statsTime += 100;
            if (statsTime >= 120000) {
                statsTime = 0;

                (new Thread() {
                    public void run() {
                        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
                        Date date = new Date();
                        String today = dateFormat.format(date);

                        int totaltime = (int) TimeUnit.MILLISECONDS.toSeconds(App.getIntParam("totaltime"));
                        int idletime = (int) TimeUnit.MILLISECONDS.toSeconds(App.getIntParam("idletime"));
                        int balancetime = (int) TimeUnit.MILLISECONDS.toSeconds(App.getIntParam("balancetime"));
                        int worktime = (int) TimeUnit.MILLISECONDS.toSeconds(App.getIntParam("worktime"));

                        List<TimeStatistic> result = Select.from(TimeStatistic.class).where(Condition.prop("date").eq(today)).list();
                        if (result.size() > 0) {
                            TimeStatistic row = result.get(0);
                            row.totaltime = totaltime;
                            row.idletime = idletime;
                            row.balancetime = balancetime;
                            row.worktime = worktime;
                            row.save();
                        } else {
                            TimeStatistic row = new TimeStatistic(today, totaltime, idletime, balancetime, worktime);
                            row.save();
                        }
                        Log.d("DEBUG", String.valueOf("time statistic updated!"));
                    }
                }).start();
            }
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        router.setRoute(defRoute);

        List<String> params = router.getRouteVars().getParams();
        for (String param : params) {
            Pair<TypesParams, Integer> parameter = new Pair<>(TypesParams.INT, 0);
            App.paramsMap.put(param, parameter);
        }

        bus = new Bus(ThreadEnforcer.ANY);
        bus.register(this);
        singleton = this;
        context = getSugarContext();

        String indicationLength = App.getProfileStringParam("indication_length");
        if (indicationLength.isEmpty()) {
            App.setProfileStringParam("indication_length", String.valueOf(10));
        }

        String sound = App.getProfileStringParam("sound_level");
        if (sound.isEmpty()) {
            App.setProfileStringParam("sound_level", String.valueOf(0));
        }

        App.setProfileBooleanParam("password", false);

        startConnectionServiceAndBind();
    }

    private void startConnectionServiceAndBind() {
        //start connectivity service
        Intent connectionIntent = new Intent(this, ConnectionService.class);
        startService(connectionIntent);
        bindService(connectionIntent, mServiceConnection, BIND_AUTO_CREATE);
    }

    public static void showMessage(Object message) {
        Toast.makeText(App.getContext(), message.toString(), Toast.LENGTH_LONG).show();
    }

    public static void showMessageFromResource(int resource, int duration) {
        try {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.toast_message, null);
            TextView text = (TextView) layout.findViewById(R.id.message);
            text.setText(resource);
            Toast toast = new Toast(context);
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.setDuration(duration);
            toast.setView(layout);
            toast.show();
        } catch (Exception e) {
            Log.d("KEYBOARD", String.valueOf("toast message exception!"));
        }
    }

    public static void showPopupMessage(FragmentManager fm, String message, int drawable) {
        if (!popupFlag) {
            if (!messageFragment.isAdded()) {
                messageFragment.setMessageText(message);
                messageFragment.setBackground(drawable);
                messageFragment.show(fm, "messageDialog");
                messageFragment.setRetainInstance(true);
                messageFragment.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
            }
            popupFlag = true;
        }
    }

    public static void openKeyboard(String param, String paramValue, TypesParams valueType, FragmentManager fragmentManager) {
        keyboard.valueParam = param;
        keyboard.machineValue = paramValue;
        keyboard.valueType = valueType;
        keyboard.setRetainInstance(true);
        keyboard.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        keyboard.show(fragmentManager, "keyboard");
    }

    public static void openKeyboard(String param, TypesParams valueType, FragmentManager fragmentManager) {
        keyboard.valueParam = param;
        keyboard.valueType = valueType;
        keyboard.setRetainInstance(true);
        keyboard.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        keyboard.show(fragmentManager, "keyboard");
    }

    public static void openKeyboard(String param, FragmentManager fragmentManager) {
        keyboard.valueParam = param;
        keyboard.valueType = TypesParams.INT;
        keyboard.machineValue = App.getParamString(param);
        keyboard.setRetainInstance(true);
        keyboard.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        keyboard.show(fragmentManager, "keyboard");
    }

    public static void closePopupMessage() {
        if (popupFlag) {
            messageFragment.dismiss();
            popupFlag = false;
        }
    }

    public static boolean sendCommand(String telnetCommand) {
        if (connectionService != null) {
            return connectionService.sendCommand(telnetCommand);
        } else {
            Log.d("App", "Can't send command = \"" + telnetCommand + "\". connectionService is null!");
            return false;
        }
    }

    public static Pair getParam(String name) {
        return App.paramsMap.get(name);
    }

    public static int getIntParam(String name) {
        try {
            Pair param = App.paramsMap.get(name);
            if (param != null) {
                return Integer.parseInt(param.second.toString());
            } else {
                return 0;
            }
        } catch (NumberFormatException ex) {
            Log.w("App", "CANT parse int param with name \"" + String.valueOf(name) + "\"");
            return 0;
        }
    }

    public static float getFloatParam(String name) {
        try {
            Pair param = App.paramsMap.get(name);
            if (param != null) {
                return Float.parseFloat(App.paramsMap.get(name).second.toString());
            } else {
                return 0f;
            }
        } catch (NumberFormatException ex) {
            Log.w("App", "CANT parse int param with name \"" + String.valueOf(name) + "\"");
            return 0;
        }
    }

    public static String getFloatParamString(String name) {
        float value = App.round(App.getFloatParam(name), 2);
        return String.valueOf(value);
    }

    public static String getParamString(String name) {
        if (App.paramsMap.get(name) != null) {
            String result = String.valueOf(App.paramsMap.get(name).second);
            if (result.length() > 3) {
                char dot = result.toCharArray()[3];

                if (dot == '.' && result.length() == 5) {
                    result = result.substring(0, result.length() - 1);
                    result = result.substring(0, result.length() - 1);
                }
            }

            return result;
        } else {
            return "";
        }
    }

    public static Context getContext() {
        return context;
    }

    public static void setIntParam(String name, int value) {
        String command = name + " " + value;
        App.sendCommand(command);
    }

    public static void setFloatParam(String name, float value) {
        String command = name + " " + value;
        App.sendCommand(command);
    }

    public static String getStringFromResources(String key) {
        try {
            return context.getResources().getString(context.getResources().getIdentifier(key, "string", context.getPackageName()));
        } catch (Exception e) {
            return "";
        }
    }

    public static String getStringFromResources(int id) {
        return context.getString(id);
    }

    public static <T> boolean isExists(final T[] array, final T object) {
        return Arrays.asList(array).contains(object);
    }

    public static HashMap<String, Pair> getParamsMap() {
        return paramsMap;
    }

    public static void pxToDp(int px) {
        DisplayMetrics displayMetrics = App.getContext().getResources().getDisplayMetrics();
        int result = Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        Log.d("DIMEN", String.valueOf(result));
        App.showMessage(String.valueOf(result));
    }

    public static void pxToSp(float px) {
        float scaledDensity = App.getContext().getResources().getDisplayMetrics().scaledDensity;
        float result = px / scaledDensity;
        Log.d("DIMEN", String.valueOf(result));
        App.showMessage(String.valueOf(result));
    }

    public static int dpToPx(int dp) {
        Resources r = getContext().getResources();
        int px = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dp,
                r.getDisplayMetrics()
        );
        return px;
    }

    public static float dpToPx(float dp) {
        Resources r = getContext().getResources();
        float px = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dp,
                r.getDisplayMetrics()
        );
        return px;
    }

    public static long calcCrc32(byte[] bytes) {
        Checksum checksum = new CRC32();
        checksum.update(bytes, 0, bytes.length);
        return checksum.getValue();
    }

    public static void playSound(int fileSoundLvl, String filePath) {
        int userSoundLvl = Integer.parseInt(App.getProfileStringParam("sound_level"));
        switch (userSoundLvl) {
            case BalanceParams.SOUND_NORMAL:
                playSound(filePath);
                break;
            case BalanceParams.SOUND_IMPORTANT:
                if (fileSoundLvl == BalanceParams.SOUND_IMPORTANT) {
                    playSound(filePath);
                }
                break;
        }
    }

    public static void playSound(String filePath) {
        try {
            if (!player.isPlaying()) {
                AssetFileDescriptor file = null;
                file = singleton.getAssets().openFd(filePath);
                player.reset();
                player.setDataSource(file.getFileDescriptor(), file.getStartOffset(), file.getLength());
                player.prepare();
                player.start();
            }
        } catch (Exception e) {
            Log.d("DEBUG", e.toString());
        }
    }

    public static void playSound(int fileSoundLvl, final String[] filePaths) {
        int userSoundLvl = Integer.parseInt(App.getProfileStringParam("sound_level"));
        switch (userSoundLvl) {
            case BalanceParams.SOUND_NORMAL:
                playSound(filePaths);
                break;
            case BalanceParams.SOUND_IMPORTANT:
                if (fileSoundLvl == BalanceParams.SOUND_IMPORTANT) {
                    playSound(filePaths);
                }
                break;
        }
    }

    public static void playSound(final String[] filePaths) {
        final int filesCount = filePaths.length;
        try {
            if (!player.isPlaying()) {
                Log.d("DEBUG", String.valueOf("play filePaths sounds"));
                AssetFileDescriptor file;
                file = singleton.getAssets().openFd(filePaths[soundCount]);
                player.reset();
                player.setDataSource(file.getFileDescriptor(), file.getStartOffset(), file.getLength());
                player.prepare();
                player.start();
                soundCount++;
                player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        if (soundCount < filesCount) {
                            playSound(filePaths);
                        } else {
                            soundCount = 0;
                            player.setOnCompletionListener(null);
                        }
                    }
                });
            }
        } catch (Exception e) {
            Log.d("DEBUG", e.toString());
        }
    }

    // Clamps the number
    public static double clamp(double value, double minValue, double maxValue) {
        return Math.max(minValue, Math.min(maxValue, value));
    }

    public static float round(float d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    }

    public static int packLayout(int mask, int layout, int mode) {
        double mul = Math.pow(2, (mode * 3));
        double div = Math.pow(2, ((mode + 1) * 3));
        return (int) (Math.floor(mask / div) * div + layout * mul + mask % mul);
    }

    public static int unpackLayout(int mask, int mode) {
        return (int) Math.floor(mask / Math.pow(2, (mode * 3))) % 8;
    }

    public static boolean isNumeric(String str) {
        return str.matches("-?\\d+(\\.\\d+)?");
    }

    public static void openMenuPopup(View view, Context context) {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setView(view);

        final Dialog dialog = alert.create();
        view.setTag(R.id.dialog, dialog);
        dialog.show();

        //Grab the window of the dialog, and change the width
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    public static boolean isMachineParam(String str) {
        List<String> params = router.getRouteVars().getParams();

        for (String parameter : params) {
            if (parameter.equals(str)) {
                return true;
            }
        }
        return false;
    }

    public static int getProfileIntParam(String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(App.getContext());
        return prefs.getInt(key, 0);
    }

    public static void setProfileIntParam(String key, int value) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(App.getContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public static boolean getProfileBooleanParam(String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(App.getContext());
        return prefs.getBoolean(key, false);
    }

    public static void setProfileBooleanParam(String key, boolean value) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(App.getContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(key, value);
        editor.apply();
        App.bus.post(AppEvents.PREFERENCE_CHANGE);
    }

    public static float getProfileFloatParam(String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(App.getContext());
        return prefs.getFloat(key, 0);
    }

    public static void setProfileFloatParam(String key, float value) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(App.getContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.putFloat(key, value);
        editor.apply();
    }

    public static String getProfileStringParam(String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(App.getContext());
        return prefs.getString(key, "");
    }

    public static void setProfileStringParam(String key, String value) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(App.getContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.apply();
        App.bus.post(AppEvents.PREFERENCE_CHANGE);
    }

    // Processes balance errors
    public static boolean processErrors(int newErrors, float oldErrors, int offset) {
        int code = getErrorCode(newErrors, offset);
        if (newErrors != oldErrors && code != 0) {
            addErrorToJournal(code);
        }
        return (code != 0);
    }

    private static int getErrorCode(int newErrors, int offset) {
        for (int i = 0; i < 32; i++) {
            if ((newErrors & 0b1) == 0b1) {
                int code = (i + offset);
                if ((code < 5 || code > 7) && (code != 19)) {
                    return code;
                }
            }
            newErrors >>= 1;
        }
        return 0;
    }

    private static void addErrorToJournal(final int code) {
        (new Thread() {
            public void run() {
                int unixTime = (int) (System.currentTimeMillis() / 1000L);
                BalancingError error = new BalancingError(unixTime, code);
                error.save();
            }
        }).start();
    }

    public static void startSaveStatistic() {
        connectionService.addResponseHandler(getStringFromResources(R.string.statisticHandler), new CurrentMachineStateHandler());
        Log.d("DEBUG", "startSaveStatistic");
        try {
//            Thread.sleep(100);
            ArrayList<String> commands = MachineCommands.getCommands();
            for (String command : commands) {
                App.sendCommand(command);
                Log.d("DEBUG", "send command = " + command);
                Thread.sleep(100);
            }
        } catch (InterruptedException e) {
            Log.d("DEBUG", e.toString());
        }
    }

    public static void stopSaveStatistic() {
        Log.d("DEBUG", "remove stat handler");
        connectionService.removeHandler(getStringFromResources(R.string.statisticHandler));
    }

    public static String getCachePath() {
        return App.getContext().getCacheDir().getPath();
    }

    public static void getCacheFilesList() {
        String[] names;
        if (App.getContext().getCacheDir().isDirectory()) {
            names = App.getContext().getCacheDir().list();
            Log.d("DEBUG", Arrays.toString(names));
        }
    }

    public static String getStatisticFilePath() {
        return App.getCachePath() + "/" + App.getStringFromResources(R.string.statisticFileName);
    }
}