package sibek.balancing.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.otto.Subscribe;

import java.util.HashMap;

import sibek.balancing.App;
import sibek.balancing.R;
import sibek.balancing.tools.BalanceParams;

public class WizardProgressFragment extends ProgressFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_wizard_progress, null);

        progress = (ProgressBar) view.findViewById(R.id.balacing_progress);
        arrow = (ImageView) view.findViewById(R.id.arrow);
        wheelRevs = (TextView) view.findViewById(R.id.speed);
        stopButton = (Button) view.findViewById(R.id.stop_button);

        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopPressed = true;
                App.sendCommand("stop");
            }
        });

        return view;
    }

    @Override
    @Subscribe
    public void setParams(HashMap<String, Float> params) {
        balanceSubstate = App.getIntParam("substate");
        splitMode = App.getIntParam("split");

        double mspeed = Math.max(Math.floor(currFreq * 60.0), 0);

        progress.setProgress(calcBalanceProgress());

        if (speed != mspeed) {
            rotateArrow(arrow, (float) speed, (float) mspeed);
            wheelRevs.setText(String.valueOf((int) mspeed));
            speed = mspeed;
        }

        if (stopPressed || balanceProgress == 1) {
            if (speed < dismissSpeed) {
                this.dismiss();
            }
        }

        if (stopPressed && App.balanceSubstate == BalanceParams.BALANCE_IDLE) {
            App.sendCommand("stop");
            getActivity().finish();
        }
    }

    @Override
    public void onResume() {
        parentOnResume();
        App.bus.register(this);
        balanceFreq = App.getFloatParam("drvfreq") / App.getFloatParam("freqcoeff");
        measureTime = App.getIntParam("maxrot") / balanceFreq;
        balanceTime = 0;
        accelTime = 0;
        balanceProgress = 0;
        stopPressed = false;
        progress.setProgress(0);
    }
}