package sibek.balancing.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.otto.Subscribe;

import java.util.HashMap;

import sibek.balancing.App;
import sibek.balancing.R;
import sibek.balancing.tools.BalanceParams;

public class ProgressFragment extends DialogFragment {
    protected int balanceSubstate = BalanceParams.BALANCE_IDLE;
    protected float balanceFreq = 0;
    protected float measureTime = 0;
    protected double currFreq = 0;
    protected double speed = 0;
    protected double balanceProgress = 0;
    protected double accelTime = 0;
    protected double balanceTime = 0;
    protected boolean stopPressed = false;
    protected final int dismissSpeed = 0;
    protected int splitMode = 1;

    protected ProgressBar progress;
    protected ImageView arrow;
    protected TextView wheelRevs;
    protected Button stopButton;
    private Button leftWeight;
    private Button rightWeight;
    private Button rightWeight2;
    private boolean shown = false;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_progress, null);

        progress = (ProgressBar) view.findViewById(R.id.balacing_progress);
        arrow = (ImageView) view.findViewById(R.id.arrow);
        wheelRevs = (TextView) view.findViewById(R.id.speed);
        stopButton = (Button) view.findViewById(R.id.stop_button);
        leftWeight = (Button) view.findViewById(R.id.left_weight_p);
        rightWeight = (Button) view.findViewById(R.id.right_weight_p);
        rightWeight2 = (Button) view.findViewById(R.id.right_weight2_p);

        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopPressed = true;
                App.sendCommand("stop");
            }
        });

        return view;
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        if (shown) return;

        super.show(manager, tag);
        shown = true;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        shown = false;
        super.onDismiss(dialog);
    }

    public boolean isShown() {
        return shown;
    }

    @Override
    public void dismiss() {
        if (this.isShown()) {
            super.dismiss();
        }
    }

    @Subscribe
    public void setParams(HashMap<String, Float> params) {
        balanceSubstate = App.getIntParam("substate");
        splitMode = App.getIntParam("split");

        if (balanceSubstate == BalanceParams.BALANCE_DECEL) {
            float rndweight0 = App.getFloatParam("rndweight0");
            float rndweight1 = App.getFloatParam("rndweight1");
            float rndweight2 = App.getFloatParam("rndweight2");

            if (rndweight0 > 999) {
                leftWeight.setText(String.valueOf(999));
            } else {
                leftWeight.setText(App.getParamString("rndweight0"));
            }

            if (rndweight1 > 999) {
                rightWeight.setText(String.valueOf(999));
            } else {
                rightWeight.setText(App.getParamString("rndweight1"));
            }

            if (rndweight2 > 999) {
                rightWeight2.setText(String.valueOf(999));
            } else {
                rightWeight2.setText(App.getParamString("rndweight2"));
            }

            leftWeight.setVisibility(View.VISIBLE);
            rightWeight.setVisibility(View.VISIBLE);
            if (splitMode == 1) {
                rightWeight2.setVisibility(View.VISIBLE);
            } else {
                rightWeight2.setVisibility(View.INVISIBLE);
            }
        }

        double mspeed = Math.max(Math.floor(currFreq * 60.0), 0);

        progress.setProgress(calcBalanceProgress());

        if (speed != mspeed) {
            rotateArrow(arrow, (float) speed, (float) mspeed);
            wheelRevs.setText(String.valueOf((int) mspeed));
            speed = mspeed;
        }

        if (stopPressed || balanceProgress == 1) {
            if (speed < dismissSpeed) {
                this.dismiss();
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
    }

    //для наследников этого класса
    protected void parentOnResume() {
        super.onResume();
    }

    @Override
    public void onResume() {
        parentOnResume();
        App.bus.register(this);
        balanceFreq = App.getFloatParam("drvfreq") / App.getFloatParam("freqcoeff");
        measureTime = App.getIntParam("maxrot") / balanceFreq;
        balanceTime = 0;
        accelTime = 0;
        balanceProgress = 0;
        stopPressed = false;
        progress.setProgress(0);

        leftWeight.setVisibility(View.INVISIBLE);
        rightWeight.setVisibility(View.INVISIBLE);
        rightWeight2.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onPause() {
        super.onPause();
        App.bus.unregister(this);
    }

    protected int calcBalanceProgress() {
        // update the balance progress
        currFreq = App.getIntParam("wheelspeed") * 10.0 / 720;
        if (App.getIntParam("testmode") == 0) {
            if (stopPressed) {
                balanceProgress = 0;
            } else if (balanceSubstate >= BalanceParams.BALANCE_START && balanceSubstate < BalanceParams.BALANCE_DECEL) {
                //increment the elapsed time
                balanceTime = balanceTime + 0.09;

                // estimate the acceleration time
                if (balanceSubstate < BalanceParams.BALANCE_MEASURE && currFreq > 0.01) {
                    accelTime = balanceTime * balanceFreq / currFreq;
                }

                // estimate the balance progress
                double progress = balanceTime / (accelTime + measureTime);
                balanceProgress = App.clamp(progress, balanceProgress, 1.0);

//                Log.d("PROGRESS", "progress = " + String.valueOf(progress));

            } else if (balanceSubstate >= BalanceParams.BALANCE_DECEL) {
                balanceProgress = 1;
            }
        }

        float resultProgress = (float) balanceProgress * 100;

        return Math.round(resultProgress);
    }

    protected void rotateArrow(ImageView view, float from, float to) {
        from = from * 0.885f;
        to = to * 0.885f;
        RotateAnimation rAnim = new RotateAnimation(from, to, Animation.RELATIVE_TO_SELF, 1.0f, Animation.RELATIVE_TO_SELF, 0.0f);
        rAnim.setFillEnabled(true);
        rAnim.setFillAfter(true);
        rAnim.setInterpolator(new LinearInterpolator());
        rAnim.setDuration(0);
        view.startAnimation(rAnim);
    }
}