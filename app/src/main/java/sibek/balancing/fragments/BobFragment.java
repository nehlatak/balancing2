package sibek.balancing.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableRow;

import com.squareup.otto.Subscribe;

import java.util.HashMap;

import sibek.balancing.App;
import sibek.balancing.R;
import sibek.balancing.tools.BalanceParams;
import sibek.balancing.view.TableRadioGroup;

public class BobFragment extends BaseFragment {
    private int lastMode;
    private HashMap<Integer, Integer> statDies = new HashMap<>();
    private HashMap<Integer, Integer> dynDies = new HashMap<>();
    private int mode = 666;
    private int layout = 666;

    private RadioGroup bobMode;
    private TableRadioGroup dynLayout;
    private TableRadioGroup statLayout;
    private Button backButton;
    private int bobPosition;
    private int dynDiesPosition;
    private int statDiesPosition;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        lastMode = BalanceParams.MODE_ALU;

        dynDies.put(BalanceParams.LAYOUT_1_5, R.id.radio_bob_1_5);
        dynDies.put(BalanceParams.LAYOUT_2_5, R.id.radio_bob_2_5);
        dynDies.put(BalanceParams.LAYOUT_1_4, R.id.radio_bob_1_4);
        dynDies.put(BalanceParams.LAYOUT_1_3, R.id.radio_bob_1_3);
        dynDies.put(BalanceParams.LAYOUT_2_3, R.id.radio_bob_2_3);
        dynDies.put(BalanceParams.LAYOUT_2_4, R.id.radio_bob_2_4);

        statDies.put(BalanceParams.LAYOUT_1, R.id.radio_bob_1);
        statDies.put(BalanceParams.LAYOUT_5, R.id.radio_bob_5);
        statDies.put(BalanceParams.LAYOUT_2, R.id.radio_bob_2);
        statDies.put(BalanceParams.LAYOUT_3, R.id.radio_bob_3);
        statDies.put(BalanceParams.LAYOUT_4, R.id.radio_bob_4);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bob_menu, null);

        bobMode = (RadioGroup) view.findViewById(R.id.bob_mode);
        dynLayout = (TableRadioGroup) view.findViewById(R.id.bob_dyn_layout);
        statLayout = (TableRadioGroup) view.findViewById(R.id.bob_stat_layout);
        backButton = (Button) view.findViewById(R.id.back_button);

        if (bobPosition != 0) {
            if (bobPosition == bobMode.getChildAt(0).getId()) {
                statLayout.setVisibility(View.GONE);
                dynLayout.setVisibility(View.VISIBLE);
            }
            if (bobPosition == bobMode.getChildAt(1).getId()) {
                statLayout.setVisibility(View.VISIBLE);
                dynLayout.setVisibility(View.GONE);
            }
        } else {
            statLayout.setVisibility(View.GONE);
            dynLayout.setVisibility(View.VISIBLE);
        }

        bobMode.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (bobPosition != checkedId) {
                    switch (checkedId) {
                        case R.id.bob_1:
                            statLayout.setVisibility(View.VISIBLE);
                            dynLayout.setVisibility(View.GONE);
                            if (mode != BalanceParams.MODE_STAT) {
                                lastMode = mode;
                                mode = BalanceParams.MODE_STAT;
                                App.setIntParam("mode", mode);
                                App.playSound(BalanceParams.SOUND_NORMAL, "sounds/ru/static_balance.wav");
                            }
                            break;
                        case R.id.bob_2:
                            if (mode == BalanceParams.MODE_STAT) {
                                mode = lastMode;
                                App.setIntParam("mode", mode);
                                App.playSound(BalanceParams.SOUND_NORMAL, "sounds/ru/dynamic_balance.wav");
                            }
                            statLayout.setVisibility(View.GONE);
                            dynLayout.setVisibility(View.VISIBLE);
                            break;
                    }
                    bobPosition = checkedId;
                }
            }
        });

        statLayout.setOnCheckedChangeListener(new TableRadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(TableRadioGroup group, int checkedId) {
                if (statDiesPosition != checkedId) {
                    switch (checkedId) {
                        case R.id.radio_bob_1:
                            App.setIntParam("layout", App.packLayout(App.getIntParam("layout"), BalanceParams.LAYOUT_1, mode));
                            break;
                        case R.id.radio_bob_5:
                            App.setIntParam("layout", App.packLayout(App.getIntParam("layout"), BalanceParams.LAYOUT_5, mode));
                            break;
                        case R.id.radio_bob_2:
                            App.setIntParam("layout", App.packLayout(App.getIntParam("layout"), BalanceParams.LAYOUT_2, mode));
                            break;
                        case R.id.radio_bob_3:
                            App.setIntParam("layout", App.packLayout(App.getIntParam("layout"), BalanceParams.LAYOUT_3, mode));
                            break;
                        case R.id.radio_bob_4:
                            App.setIntParam("layout", App.packLayout(App.getIntParam("layout"), BalanceParams.LAYOUT_4, mode));
                            break;
                    }
                    statDiesPosition = checkedId;
                    App.playSound(BalanceParams.SOUND_NORMAL, "sounds/ru/balance_1_weight.wav");
                }
            }
        });

        setBackActionOnClick(statLayout);

        dynLayout.setOnCheckedChangeListener(new TableRadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(TableRadioGroup group, int checkedId) {
                if (dynDiesPosition != checkedId) {
                    switch (checkedId) {
                        case R.id.radio_bob_1_5:
                            App.setIntParam("layout", App.packLayout(App.getIntParam("layout"), BalanceParams.LAYOUT_1_5, mode));
                            break;
                        case R.id.radio_bob_2_5:
                            App.setIntParam("layout", App.packLayout(App.getIntParam("layout"), BalanceParams.LAYOUT_2_5, mode));
                            break;
                        case R.id.radio_bob_1_4:
                            App.setIntParam("layout", App.packLayout(App.getIntParam("layout"), BalanceParams.LAYOUT_1_4, mode));
                            break;
                        case R.id.radio_bob_1_3:
                            App.setIntParam("layout", App.packLayout(App.getIntParam("layout"), BalanceParams.LAYOUT_1_3, mode));
                            break;
                        case R.id.radio_bob_2_3:
                            App.setIntParam("layout", App.packLayout(App.getIntParam("layout"), BalanceParams.LAYOUT_2_3, mode));
                            break;
                        case R.id.radio_bob_2_4:
                            App.setIntParam("layout", App.packLayout(App.getIntParam("layout"), BalanceParams.LAYOUT_2_4, mode));
                            break;
                    }
                    dynDiesPosition = checkedId;
                    App.playSound(BalanceParams.SOUND_NORMAL, "sounds/ru/balance_2_weight.wav");
                }
            }
        });

        setBackActionOnClick(dynLayout);

        backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        return view;
    }

    private void setBackActionOnClick(TableRadioGroup tableGroup) {
        int groupCount = tableGroup.getChildCount();
        for (int i = 0; i < groupCount; i++) {
            TableRow row = (TableRow) tableGroup.getChildAt(i);
            int rowItemsCount = row.getChildCount();
            for (int j = 0; j < rowItemsCount; j++) {
                RadioButton rb = (RadioButton) row.getChildAt(j);
                rb.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        backButton.performClick();
                    }
                });
            }
        }
    }

    @Subscribe
    public void setParams(HashMap<String, Float> params) {
        int machineMode = App.getIntParam("mode");
        int machineLayout = App.unpackLayout(App.getIntParam("layout"), machineMode);

        if (machineMode != mode || machineLayout != layout) {
            mode = machineMode;
            layout = machineLayout;
            if (mode == BalanceParams.MODE_STAT) {
                bobMode.check(R.id.bob_1);
                statLayout.check(statDies.get(layout));
            } else {
                bobMode.check(R.id.bob_2);
                dynLayout.check(dynDies.get(layout));
            }
        }
    }
}