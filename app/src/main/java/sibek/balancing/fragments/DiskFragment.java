package sibek.balancing.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableRow;

import com.squareup.otto.Subscribe;

import java.util.HashMap;
import java.util.Map;

import sibek.balancing.App;
import sibek.balancing.R;
import sibek.balancing.tools.BalanceParams;
import sibek.balancing.view.TableRadioGroup;

public class DiskFragment extends BaseFragment {
    private HashMap<Integer, Integer> spokes = new HashMap<>();
    private HashMap<Integer, Integer> diskModes = new HashMap<>();

    private RadioGroup diskTypes;
    private TableRadioGroup spokesGroup;
    private Button backButton;
    private int modeValue = 666;
    private int spokeValue = 666;
    private int diskRadiosCheckedId;
    private int spokesGroupCheckedId;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_disk_menu, null);

        spokes.put(0, R.id.non_spokes);
        spokes.put(3, R.id.spokes_3);
        spokes.put(4, R.id.spokes_4);
        spokes.put(5, R.id.spokes_5);
        spokes.put(6, R.id.spokes_6);
        spokes.put(7, R.id.spokes_7);
        spokes.put(8, R.id.spokes_8);
        spokes.put(9, R.id.spokes_9);
        spokes.put(10, R.id.spokes_10);
        spokes.put(11, R.id.spokes_11);
        spokes.put(12, R.id.spokes_12);
        spokes.put(13, R.id.spokes_13);
//        spokes.put(14, R.id.spokes_14);

        diskModes.put(BalanceParams.MODE_ALU, R.id.radio_alu);
        diskModes.put(BalanceParams.MODE_STEEL, R.id.radio_steel);
        diskModes.put(BalanceParams.MODE_TRUCK, R.id.radio_truck);
        diskModes.put(BalanceParams.MODE_ABS, R.id.radio_abs);

        diskTypes = (RadioGroup) view.findViewById(R.id.diskGroup);
        spokesGroup = (TableRadioGroup) view.findViewById(R.id.spokesGroup);
        backButton = (Button) view.findViewById(R.id.back_button);

        diskTypes.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (diskRadiosCheckedId != checkedId) {
                    switch (checkedId) {
                        case R.id.radio_alu:
                            App.setIntParam("mode", BalanceParams.MODE_ALU);
                            App.playSound(BalanceParams.SOUND_NORMAL, "sounds/ru/alu_disk_selected.wav");
                            break;
                        case R.id.radio_steel:
                            App.setIntParam("mode", BalanceParams.MODE_STEEL);
                            App.setIntParam("split", 0);
                            App.playSound(BalanceParams.SOUND_NORMAL, "sounds/ru/steel_disk_selected.wav");
                            break;
                        case R.id.radio_truck:
                            App.setIntParam("mode", BalanceParams.MODE_TRUCK);
                            App.setIntParam("split", 0);
                            App.playSound(BalanceParams.SOUND_NORMAL, "sounds/ru/truck_disk_selected.wav");
                            break;
                        case R.id.radio_abs:
                            App.setIntParam("mode", BalanceParams.MODE_ABS);
                            App.setIntParam("split", 0);
                            break;
                    }
                    diskRadiosCheckedId = checkedId;
                }
            }
        });

        setBackActionOnClick(diskTypes);

        spokesGroup.setOnCheckedChangeListener(new TableRadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(TableRadioGroup group, int checkedId) {
                if (spokesGroupCheckedId != checkedId) {
                    int numsp = getSpokeValue(checkedId);
                    if (numsp == 0) {
                        App.setIntParam("numsp", 0);
                        App.setIntParam("split", 0);
                        App.playSound(BalanceParams.SOUND_NORMAL, "sounds/ru/alu_disk_selected.wav");
                    } else {
                        App.setIntParam("split", 1);
                        App.setIntParam("numsp", numsp);
                        App.playSound(BalanceParams.SOUND_NORMAL, "sounds/ru/num_spikes_selected.wav");
                    }
                    spokesGroupCheckedId = checkedId;
                }
            }
        });

        setBackActionOnClick(spokesGroup);

        backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        return view;
    }

    private void setBackActionOnClick(TableRadioGroup tableGroup) {
        int groupCount = tableGroup.getChildCount();
        for (int i = 0; i < groupCount; i++) {
            TableRow row = (TableRow) tableGroup.getChildAt(i);
            int rowItemsCount = row.getChildCount();
            for (int j = 0; j < rowItemsCount; j++) {
                RadioButton rb = (RadioButton) row.getChildAt(j);
                rb.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        backButton.performClick();
                    }
                });
            }
        }
    }

    private void setBackActionOnClick(RadioGroup group) {
        int groupCount = group.getChildCount();
        for (int i = 0; i < groupCount; i++) {
            RadioButton rb = (RadioButton) group.getChildAt(i);
            if (i != 0) {
                rb.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        backButton.performClick();
                    }
                });
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        spokesVisibility();
    }

    @Subscribe
    public void setParams(HashMap<String, Float> params) {
        selectMode();
    }

    public int getSpokeValue(int checkedId) {
        for (Map.Entry<Integer, Integer> entry : spokes.entrySet()) {
            int key = entry.getKey();
            int value = entry.getValue();
            if (value == checkedId) {
                return key;
            }
        }
        return 0;
    }

    private void selectMode() {
        int mode = App.getIntParam("mode");
        if (modeValue != mode && mode != BalanceParams.MODE_STAT) {
            modeValue = mode;
            diskTypes.check(diskModes.get(modeValue));
            spokesVisibility();
        }
        if (mode != BalanceParams.MODE_STAT) {
            selectSpoke();
        }
    }

    private void selectSpoke() {
        int numsp = App.getIntParam("numsp");
        int split = App.getIntParam("split");

        if (spokeValue != numsp) {
            spokeValue = numsp;
            if (split == 0) {
                spokesGroup.check(spokes.get(0));
            } else {
                spokesGroup.check(spokes.get(numsp));
            }
        }
    }

    private void spokesVisibility() {
        if (modeValue == 0) {
            spokesGroup.setVisibility(View.VISIBLE);
        } else {
            spokeValue = 666;
            spokesGroup.setVisibility(View.INVISIBLE);
        }
    }
}