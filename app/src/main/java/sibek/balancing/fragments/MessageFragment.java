package sibek.balancing.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import sibek.balancing.App;
import sibek.balancing.R;

public class MessageFragment extends DialogFragment {
    private TextView message;
    private FrameLayout messageContainer;
    private Button stopButton;
    private String messageText;
    private int drawable;
    private boolean shown = false;

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        if (shown) return;

        super.show(manager, tag);
        shown = true;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        shown = false;
        super.onDismiss(dialog);
    }

    public boolean isShown() {
        return shown;
    }

    @Override
    public void dismiss() {
        if (this.isShown()) {
            super.dismiss();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_popup, null);

        message = (TextView) view.findViewById(R.id.message);
        messageContainer = (FrameLayout) view.findViewById(R.id.container);
        message.setText(messageText);
        messageContainer.setBackground(ContextCompat.getDrawable(App.getContext(), drawable));
        stopButton = (Button) view.findViewById(R.id.stop_button);
        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                App.sendCommand("stop");
            }
        });

        return view;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public void setBackground(int drawable) {
        this.drawable = drawable;
    }
}