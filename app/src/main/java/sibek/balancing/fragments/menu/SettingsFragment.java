package sibek.balancing.fragments.menu;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.util.Pair;
import android.util.Log;

import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.HashMap;

import sibek.balancing.App;
import sibek.balancing.R;
import sibek.balancing.activities.ErrorsActivity;
import sibek.balancing.activities.MainActivity;
import sibek.balancing.activities.OscilloscopeActivity;
import sibek.balancing.activities.StatisticActivity;
import sibek.balancing.activities.TelnetActivity;
import sibek.balancing.fragments.MainScreenFragment;
import sibek.balancing.menu.MenuItem;
import sibek.balancing.tools.AppEvents;
import sibek.balancing.tools.TypesParams;

public class SettingsFragment extends BaseMenuFragment {
    public SettingsFragment() {
        tabsTitles.add(App.getStringFromResources(R.string.settings_tab_1));
        tabsTitles.add(App.getStringFromResources(R.string.settings_tab_2));
        tabsTitles.add(App.getStringFromResources(R.string.settings_tab_3));
        tabsTitles.add(App.getStringFromResources(R.string.settings_tab_4));

        ArrayList<MenuItem> tab1 = new ArrayList<>();
        ArrayList<MenuItem> tab2 = new ArrayList<>();
        ArrayList<MenuItem> tab3 = new ArrayList<>();
        ArrayList<MenuItem> tab4 = new ArrayList<>();

        ArrayList<MenuItem> scoringItems = new ArrayList<>();
        scoringItems.add(new MenuItem(R.string.sound_level_0_header, R.string.sound_level_0_text, new OnMenuClick() {
            @Override
            public void onClick() {
                App.setProfileStringParam("sound_level", String.valueOf(0));
            }
        }).setIcon(R.drawable.main_icon_net_zvuka));
        scoringItems.add(new MenuItem(R.string.sound_level_1_header, R.string.sound_level_1_text, new OnMenuClick() {
            @Override
            public void onClick() {
                App.setProfileStringParam("sound_level", String.valueOf(1));
            }
        }).setIcon(R.drawable.main_icon_vazhnye_sobytia));
        scoringItems.add(new MenuItem(R.string.sound_level_2_header, R.string.sound_level_2_text, new OnMenuClick() {
            @Override
            public void onClick() {
                App.setProfileStringParam("sound_level", String.valueOf(2));
            }
        }).setIcon(R.drawable.main_icon_vse_sobytia));
        MenuItem scoring = new MenuItem(R.string.sound_level_header, R.string.sound_level_text, "sound_level");
        scoring.setSubItems(scoringItems);
        scoring.setIcon(R.drawable.main_icon_ozvuchivanie);
        tab1.add(scoring);

        MenuItem theme = new MenuItem(R.string.theme_header, R.string.theme_text);
        theme.setIcon(R.drawable.main_icon_vibor_tem);
        tab1.add(theme);

        MenuItem wifiSearch = new MenuItem(R.string.wifi_search_header, R.string.wifi_search_text)
                .setIcon(R.drawable.main_icon_wi_fi)
                .setOnClickListener(new OnMenuClick() {
                    @Override
                    public void onClick() {
                        startActivity(new Intent(android.provider.Settings.ACTION_WIFI_SETTINGS));
                    }
                });
        tab2.add(wifiSearch);

        WifiManager wifiManager = (WifiManager) App.getContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        MenuItem ssid = new MenuItem("ssid", wifiInfo.getSSID());
        ssid.setIcon(R.drawable.main_icon_ssid);
        tab2.add(ssid);

        MenuItem network = new MenuItem(R.string.network_header, R.string.network_text);
        network.setIcon(R.drawable.main_icon_set);
        tab2.add(network);

        MenuItem update = new MenuItem(R.string.hardware_search_header, R.string.hardware_search_text);
        update.setIcon(R.drawable.main_icon_obnovlenie);
        tab2.add(update);

        MenuItem remoteControl = new MenuItem(R.string.remote_control_header, R.string.remote_control_text);
        remoteControl.setIcon(R.drawable.main_icon_udalennoe_upravlenie);
        tab2.add(remoteControl);

        MenuItem serverAddress = new MenuItem(R.string.server_addr_header, R.string.server_addr_text, "server_addr", new Pair<TypesParams, Object>(TypesParams.LOCAL_PREFERENCE, App.getProfileStringParam("server_addr")), new OnMenuClick() {
            @Override
            public void onClick() {
                App.openKeyboard("server_addr", App.getProfileStringParam("server_addr"), TypesParams.LOCAL_PREFERENCE, getFragmentManager());
            }
        });
        serverAddress.password = true;
        serverAddress.setIcon(R.drawable.main_icon_set);
        tab2.add(serverAddress);

        MenuItem telnetClient = new MenuItem();
        telnetClient
                .setTitle(R.string.start_telnet_client)
                .setDescription(R.string.start_telnet_client_descr)
                .setIcon(R.drawable.telnet_icon)
                .setOnClickListener(new OnMenuClick() {
                    @Override
                    public void onClick() {
                        Intent telnet = new Intent(context, TelnetActivity.class);
                        startActivity(telnet);
                    }
                });
        tab3.add(telnetClient);

        MenuItem writeSettings = new MenuItem(R.string.save_settings_header, R.string.save_settings_text, new OnMenuClick() {
            @Override
            public void onClick() {
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                App.sendCommand("saveref");
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                Log.d("DEBUG", String.valueOf("NO"));
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage(R.string.save_settings_confirm).setPositiveButton(R.string.yes, dialogClickListener)
                        .setNegativeButton(R.string.no, dialogClickListener).show();
            }
        });
        writeSettings.password = true;
        writeSettings.setIcon(R.drawable.main_icon_zapis_nastroek);
        tab3.add(writeSettings);

        MenuItem restoreSettings = new MenuItem(R.string.factory_settings_header, R.string.factory_settings_text, new OnMenuClick() {
            @Override
            public void onClick() {
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                App.sendCommand("loadref");
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                Log.d("DEBUG", String.valueOf("NO"));
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage(R.string.factory_settings_confirm).setPositiveButton(R.string.yes, dialogClickListener)
                        .setNegativeButton(R.string.no, dialogClickListener).show();
            }
        });
        restoreSettings.password = true;
        restoreSettings.setIcon(R.drawable.main_icon_vosstanovlenie);
        tab3.add(restoreSettings);

        MenuItem errors = new MenuItem(R.string.errors_header, R.string.errors_text, new OnMenuClick() {
            @Override
            public void onClick() {
                Intent err = new Intent(context, ErrorsActivity.class);
                startActivity(err);
            }
        });
        errors.setIcon(R.drawable.main_icon_error);
        tab3.add(errors);

        MenuItem statisticByDay = new MenuItem(R.string.stats_disks_header, R.string.stats_disks_text, new OnMenuClick() {
            @Override
            public void onClick() {
                Intent stat = new Intent(context, StatisticActivity.class);
                stat.putExtra("type", "byDays");
                startActivity(stat);
            }
        });
        statisticByDay.setIcon(R.drawable.main_icon_stat_day);
        tab3.add(statisticByDay);

        MenuItem statisticByDiam = new MenuItem(R.string.stats_inches_header, R.string.stats_inches_text, new OnMenuClick() {
            @Override
            public void onClick() {
                Intent stat = new Intent(context, StatisticActivity.class);
                stat.putExtra("type", "byInches");
                startActivity(stat);
            }
        });
        statisticByDiam.setIcon(R.drawable.main_icon_stat_diametr);
        tab3.add(statisticByDiam);

        MenuItem statisticByBob = new MenuItem(R.string.stats_weights_header, R.string.stats_weights_text, new OnMenuClick() {
            @Override
            public void onClick() {
                Intent stat = new Intent(context, StatisticActivity.class);
                stat.putExtra("type", "byPlummet");
                startActivity(stat);
            }
        });
        statisticByBob.setIcon(R.drawable.main_icon_stat_bob);
        tab3.add(statisticByBob);

        MenuItem statisticByTime = new MenuItem(R.string.stats_time_header, R.string.stats_time_text, new OnMenuClick() {
            @Override
            public void onClick() {
                Intent stat = new Intent(context, StatisticActivity.class);
                stat.putExtra("type", "byTime");
                startActivity(stat);
            }
        });
        statisticByTime.setIcon(R.drawable.main_icon_stat_time);
        tab3.add(statisticByTime);

        MenuItem qepAngle = new MenuItem(R.string.wheel_qep_header, R.string.wheel_qep_text, "wheelangle");
        qepAngle.setIcon(R.drawable.main_icon_datchik_ugla);
        tab4.add(qepAngle);

        MenuItem testDrv = new MenuItem(R.string.driver_test_header, R.string.driver_test_text, new OnMenuClick() {
            @Override
            public void onClick() {
                Activity activity = getActivity();
                if (activity instanceof MainActivity) {
                    App.sendCommand("testdrv");
                    ((MainActivity) activity).displayView(0);
                }
            }
        });
        testDrv.setIcon(R.drawable.main_icon_test_privoda);
        tab4.add(testDrv);

        MenuItem oscilloscope = new MenuItem(R.string.oscilloscope_header, R.string.oscilloscope_text, new OnMenuClick() {
            @Override
            public void onClick() {
                Intent osc = new Intent(context, OscilloscopeActivity.class);
                startActivity(osc);
            }
        });
        oscilloscope.setIcon(R.drawable.main_icon_ostsylograf);
        tab4.add(oscilloscope);

        ArrayList<MenuItem> multiplexorItems = new ArrayList<>();
        multiplexorItems.add(new MenuItem(R.string.mux_0_header, R.string.mux_0_text, "muxval", new Pair<TypesParams, Object>(TypesParams.INT, 0)).setIcon(R.drawable.main_icon_multiplexor));
        multiplexorItems.add(new MenuItem(R.string.mux_1_header, R.string.mux_1_text, "muxval", new Pair<TypesParams, Object>(TypesParams.INT, 1)).setIcon(R.drawable.main_icon_multiplexor));
        multiplexorItems.add(new MenuItem(R.string.mux_2_header, R.string.mux_2_text, "muxval", new Pair<TypesParams, Object>(TypesParams.INT, 2)).setIcon(R.drawable.main_icon_multiplexor));
        multiplexorItems.add(new MenuItem(R.string.mux_3_header, R.string.mux_3_text, "muxval", new Pair<TypesParams, Object>(TypesParams.INT, 3)).setIcon(R.drawable.main_icon_multiplexor));
        MenuItem multiplexor = new MenuItem(R.string.mux_header, R.string.mux_text, "muxval").setIcon(R.drawable.main_icon_multiplexor);
        multiplexor.setSubItems(multiplexorItems);
        tab4.add(multiplexor);

        tabs.add(tab1);
        tabs.add(tab2);
        tabs.add(tab3);
        tabs.add(tab4);
    }

    @Subscribe
    public void updateParams(HashMap<String, Float> params) {
        super.updateParams(params);
    }

    @Subscribe
    public void updateParams(AppEvents action) {
        super.updateParams(action);
    }
}