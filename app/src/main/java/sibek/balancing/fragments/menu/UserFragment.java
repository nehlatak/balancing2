package sibek.balancing.fragments.menu;

import android.support.v4.util.Pair;

import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.HashMap;

import sibek.balancing.App;
import sibek.balancing.R;
import sibek.balancing.menu.MenuItem;
import sibek.balancing.tools.AppEvents;
import sibek.balancing.tools.BalanceParams;
import sibek.balancing.tools.TypesParams;

public class UserFragment extends BaseMenuFragment {
    public UserFragment() {
        tabsTitles.add(App.getStringFromResources(R.string.user_tab_1));
        tabsTitles.add(App.getStringFromResources(R.string.user_tab_2));

        ArrayList<MenuItem> tab1 = new ArrayList<>();
        ArrayList<MenuItem> tab2 = new ArrayList<>();

//        MenuItem roundMode = new MenuItem(R.string.round_header, R.string.round_text, "roundmode", new Pair<TypesParams, Object>(TypesParams.INT, 0));
        MenuItem roundMode = new MenuItem(R.string.round_header, R.string.round_text, "roundmode", new OnMenuClick() {
            @Override
            public void onClick() {
                App.openKeyboard("roundmode", getFragmentManager());
            }
        });
        roundMode.setIcon(R.drawable.main_icon_rezhym_okruglenie);

        tab1.add(roundMode);

//        MenuItem minWeight = new MenuItem(R.string.min_weight_header, R.string.min_weight_text, "minweight", new Pair<TypesParams, Object>(TypesParams.INT, 0));
        MenuItem minWeight = new MenuItem(R.string.min_weight_header, R.string.min_weight_text, "minweight", new OnMenuClick() {
            @Override
            public void onClick() {
                App.openKeyboard("minweight", getFragmentManager());
            }
        });
        minWeight.setIcon(R.drawable.main_icon_minim_ves);
        tab1.add(minWeight);

        ArrayList<MenuItem> autoAluModeItems = new ArrayList<>();
        autoAluModeItems.add(new MenuItem(R.string.auto_alu_0_header, R.string.auto_alu_0_text, "autoalu", new Pair<TypesParams, Object>(TypesParams.INT, 0)).setIcon(R.drawable.main_icon_raspozn_otkl));
        autoAluModeItems.add(new MenuItem(R.string.auto_alu_1_header, R.string.auto_alu_1_text, "autoalu", new Pair<TypesParams, Object>(TypesParams.INT, 1)).setIcon(R.drawable.main_icon_raspozn_vkl));
        MenuItem autoAluMode = new MenuItem(R.string.auto_alu_header, R.string.auto_alu_text, "autoalu").setIcon(R.drawable.main_icon_umnoe_raspoznavanie);
        autoAluMode.setSubItems(autoAluModeItems);
        tab1.add(autoAluMode);

        ArrayList<MenuItem> trueModeItems = new ArrayList<>();
        trueModeItems.add(new MenuItem(R.string.true_mode_0_header, R.string.true_mode_0_text, "truemode", new Pair<TypesParams, Object>(TypesParams.INT, 0)).setIcon(R.drawable.main_icon_12_chasov));
        trueModeItems.add(new MenuItem(R.string.true_mode_1_header, R.string.true_mode_1_text, "truemode", new Pair<TypesParams, Object>(TypesParams.INT, 1)).setIcon(R.drawable.main_icon_rasschetnoe));
        MenuItem trueMode = new MenuItem(R.string.true_mode_header, R.string.true_mode_text, "truemode").setIcon(R.drawable.main_icon_smeschenie_ugla);
        trueMode.setSubItems(trueModeItems);
        tab1.add(trueMode);

        MenuItem indicationLength = new MenuItem(R.string.indication_length_header, R.string.indication_length_text, "indication_length", new Pair<TypesParams, Object>(TypesParams.LOCAL_PREFERENCE, App.getProfileStringParam("indication_length")), new OnMenuClick() {
            @Override
            public void onClick() {
                App.openKeyboard("indication_length", String.valueOf(App.getProfileStringParam("indication_length")), TypesParams.LOCAL_PREFERENCE, getFragmentManager());
            }
        });
        indicationLength.setIcon(R.drawable.main_icon_l);
        tab1.add(indicationLength);

        ArrayList<MenuItem> startModeItems = new ArrayList<>();
        startModeItems.add(new MenuItem(R.string.start_mode_0_header, R.string.start_mode_0_text, "startmode", new Pair<TypesParams, Object>(TypesParams.INT, 0)).setIcon(R.drawable.main_icon_kryshka_opuschena));
        startModeItems.add(new MenuItem(R.string.start_mode_1_header, R.string.start_mode_1_text, "startmode", new Pair<TypesParams, Object>(TypesParams.INT, 1)).setIcon(R.drawable.main_icon_bezuslovny_zapusk));
        startModeItems.add(new MenuItem(R.string.start_mode_2_header, R.string.start_mode_2_text, "startmode", new Pair<TypesParams, Object>(TypesParams.INT, 2)).setIcon(R.drawable.main_icon_avto_zapusk));
        MenuItem startMode = new MenuItem(R.string.start_mode_header, R.string.start_mode_text, "startmode").setIcon(R.drawable.main_icon_rezhym_zapuska);
        startMode.setSubItems(startModeItems);
        tab2.add(startMode);

        ArrayList<MenuItem> rotationModeItems = new ArrayList<>();
        rotationModeItems.add(new MenuItem(R.string.rotation_mode_0_header, R.string.rotation_mode_0_text, "automode", new Pair<TypesParams, Object>(TypesParams.INT, 0)).setIcon(R.drawable.main_icon_ne_dovodit).setOnClickListener(new OnMenuClick() {
            @Override
            public void onClick() {
                App.playSound(BalanceParams.SOUND_NORMAL, "sounds/ru/auto_rotation_off.wav");
            }
        }));
        rotationModeItems.add(new MenuItem(R.string.rotation_mode_1_header, R.string.rotation_mode_1_text, "automode", new Pair<TypesParams, Object>(TypesParams.INT, 1)).setIcon(R.drawable.main_icon_po_levoy).setOnClickListener(new OnMenuClick() {
            @Override
            public void onClick() {
                App.playSound(BalanceParams.SOUND_NORMAL, "sounds/ru/auto_rotation_on.wav");
            }
        }));
        rotationModeItems.add(new MenuItem(R.string.rotation_mode_2_header, R.string.rotation_mode_2_text, "automode", new Pair<TypesParams, Object>(TypesParams.INT, 2)).setIcon(R.drawable.main_icon_po_pravoy).setOnClickListener(new OnMenuClick() {
            @Override
            public void onClick() {
                App.playSound(BalanceParams.SOUND_NORMAL, "sounds/ru/auto_rotation_on.wav");
            }
        }));
        rotationModeItems.add(new MenuItem(R.string.rotation_mode_3_header, R.string.rotation_mode_3_text, "automode", new Pair<TypesParams, Object>(TypesParams.INT, 3)).setIcon(R.drawable.main_icon_do_blizhayshego).setOnClickListener(new OnMenuClick() {
            @Override
            public void onClick() {
                App.playSound(BalanceParams.SOUND_NORMAL, "sounds/ru/auto_rotation_on.wav");
            }
        }));
        MenuItem rotationMode = new MenuItem(R.string.rotation_mode_header, R.string.rotation_mode_text, "automode").setIcon(R.drawable.main_icon_rezhym_dovod_kolesa);
        rotationMode.setSubItems(rotationModeItems);
        tab2.add(rotationMode);

        ArrayList<MenuItem> pedalModeItems = new ArrayList<>();
        pedalModeItems.add(new MenuItem(R.string.pedal_mode_0_header, R.string.pedal_mode_0_text, "pedalmode", new Pair<TypesParams, Object>(TypesParams.INT, 0)).setIcon(R.drawable.main_icon_pedal_vruchnuyu));
        pedalModeItems.add(new MenuItem(R.string.pedal_mode_1_header, R.string.pedal_mode_1_text, "pedalmode", new Pair<TypesParams, Object>(TypesParams.INT, 1)).setIcon(R.drawable.main_icon_pedal_avto));
        MenuItem pedalMode = new MenuItem(R.string.pedal_mode_header, R.string.pedal_mode_text, "pedalmode").setIcon(R.drawable.main_icon_rezhym_pedali);
        pedalMode.setSubItems(pedalModeItems);
        tab2.add(pedalMode);

        ArrayList<MenuItem> directionModeItems = new ArrayList<>();
        directionModeItems.add(new MenuItem(R.string.direction_0_header, R.string.direction_0_text, "clockwise", new Pair<TypesParams, Object>(TypesParams.INT, 0)).setIcon(R.drawable.main_icon_protiv_chasovoy));
        directionModeItems.add(new MenuItem(R.string.direction_1_header, R.string.direction_1_text, "clockwise", new Pair<TypesParams, Object>(TypesParams.INT, 1)).setIcon(R.drawable.main_icon_po_chasovoy));
        MenuItem directionMode = new MenuItem(R.string.direction_header, R.string.direction_text, "clockwise").setIcon(R.drawable.main_icon_rezhym_naprav_vraschenia);
        directionMode.setSubItems(directionModeItems);
        tab2.add(directionMode);

        tabs.add(tab1);
        tabs.add(tab2);
    }

    @Subscribe
    public void updateParams(HashMap<String, Float> params) {
        super.updateParams(params);
    }

    @Subscribe
    public void updateParams(AppEvents action) {
        super.updateParams(action);
    }
}