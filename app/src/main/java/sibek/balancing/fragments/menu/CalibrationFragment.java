package sibek.balancing.fragments.menu;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.widget.Toast;

import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.HashMap;

import sibek.balancing.App;
import sibek.balancing.R;
import sibek.balancing.activities.BleRulerWizardActivity;
import sibek.balancing.activities.MainActivity;
import sibek.balancing.activities.WizardActivity;
import sibek.balancing.fragments.MainScreenFragment;
import sibek.balancing.menu.MenuItem;
import sibek.balancing.tools.AppEvents;
import sibek.balancing.tools.BalanceParams;
import sibek.balancing.tools.TypesParams;

public class CalibrationFragment extends BaseMenuFragment {
    public CalibrationFragment() {
        tabsTitles.add(App.getStringFromResources(R.string.calibraion_tab_2));
        tabsTitles.add(App.getStringFromResources(R.string.calibraion_tab_3));
        tabsTitles.add(App.getStringFromResources(R.string.calibraion_tab_4));

        ArrayList<MenuItem> tab1 = new ArrayList<>();
        ArrayList<MenuItem> tab2 = new ArrayList<>();
        ArrayList<MenuItem> tab3 = new ArrayList<>();

        MenuItem resetShaft = new MenuItem(R.string.balance_fast_cal_header, R.string.balance_fast_cal_text, new OnMenuClick() {
            @Override
            public void onClick() {
                Activity activity = getActivity();
                if (activity instanceof MainActivity) {
                    App.sendCommand("keycal0");
                    ((MainActivity) activity).displayView(0);
                }
            }
        });
        resetShaft.setIcon(R.drawable.main_icon_obnulenie_vala);
        tab1.add(resetShaft);

        MenuItem calibration = new MenuItem(R.string.calibration_master_header, R.string.calibration_master_text, new OnMenuClick() {
            @Override
            public void onClick() {
                int machineMode = App.getIntParam("mode");
                int machineLayout = App.unpackLayout(App.getIntParam("layout"), machineMode);

                if (machineLayout != BalanceParams.LAYOUT_1_5) {
                    App.setIntParam("layout", App.packLayout(App.getIntParam("layout"), BalanceParams.LAYOUT_1_5, machineMode));
                    App.showMessageFromResource(R.string.force_set_layout_1_5, Toast.LENGTH_SHORT);
                }

                App.sendCommand("cal0");
                Intent intent = new Intent(App.getContext(), WizardActivity.class);
                intent.putExtra("wizard", "balance");
                startActivity(intent);
            }
        });
        calibration.password = true;
        calibration.setIcon(R.drawable.main_icon_master_datchikov);
        tab1.add(calibration);

        MenuItem calibration2 = new MenuItem("Мастер калибровки BLE линейки", "Мастер производит калибровку BLE линейки")
                .setOnClickListener(new OnMenuClick() {
                    @Override
                    public void onClick() {
                        Intent intent = new Intent(App.getContext(), BleRulerWizardActivity.class);
                        startActivity(intent);
                    }
                })
                .setIcon(R.drawable.main_icon_master_datchikov);
        tab1.add(calibration2);

        /*MenuItem shaftCalibration = new MenuItem(R.string.balance_cal_0_header, R.string.balance_cal_0_text, new OnMenuClick() {
            @Override
            public void onClick() {
                //вызов мастера калибровки вала
            }
        });
        shaftCalibration.setIcon(R.drawable.main_icon_kalibrovka_vala);
        tab1.add(shaftCalibration);

        MenuItem wheelCalibration = new MenuItem(R.string.balance_cal_1_header, R.string.balance_cal_1_text, new OnMenuClick() {
            @Override
            public void onClick() {
                //вызов мастера калибровки колеса
            }
        });
        wheelCalibration.setIcon(R.drawable.main_icon_kalibrovka_kolesa);
        tab1.add(wheelCalibration);

        MenuItem rightCalibration = new MenuItem(R.string.balance_cal_2_header, R.string.balance_cal_2_text, new OnMenuClick() {
            @Override
            public void onClick() {
                //вызов мастера калибровки справа
            }
        });
        rightCalibration.setIcon(R.drawable.main_icon_kalibrovka_sprava);
        tab1.add(rightCalibration);

        MenuItem leftCalibration = new MenuItem(R.string.balance_cal_3_header, R.string.balance_cal_3_text, new OnMenuClick() {
            @Override
            public void onClick() {
                //вызов мастера калибровки слева
            }
        });
        leftCalibration.setIcon(R.drawable.main_icon_kalibrovka_sleva);
        tab1.add(leftCalibration);*/

        MenuItem plummetWeight = new MenuItem(R.string.balance_cal_weight_header, R.string.balance_cal_weight_text, "calweight", new OnMenuClick() {
            @Override
            public void onClick() {
                App.openKeyboard("calweight", getFragmentManager());
            }
        });
        plummetWeight.setIcon(R.drawable.main_icon_massa_gruzika);
        tab1.add(plummetWeight);

        MenuItem rulerCalibration = new MenuItem(R.string.rruler_cal_0_header, R.string.rruler_cal_0_text, new OnMenuClick() {
            @Override
            public void onClick() {
                App.sendCommand("rrulercal0");
                Intent intent = new Intent(App.getContext(), WizardActivity.class);
                intent.putExtra("wizard", "ruler");
                startActivity(intent);
            }
        });
        rulerCalibration.password = true;
        rulerCalibration.setIcon(R.drawable.main_icon_master_lineyki);
        tab2.add(rulerCalibration);

        MenuItem diamCalibration = new MenuItem(R.string.rruler_cal_3_header, R.string.rruler_cal_3_text, new OnMenuClick() {
            @Override
            public void onClick() {
                App.sendCommand("rrulercal3");
                Intent intent = new Intent(App.getContext(), WizardActivity.class);
                intent.putExtra("wizard", "diam");
                startActivity(intent);
            }
        });
        diamCalibration.password = true;
        diamCalibration.setIcon(R.drawable.main_icon_kalibrovka_diametra);
        tab2.add(diamCalibration);

        MenuItem diameter = new MenuItem(R.string.ruler_cal_diam_header, R.string.ruler_cal_diam_text, "rulerdiam", new OnMenuClick() {
            @Override
            public void onClick() {
                App.openKeyboard("rulerdiam", TypesParams.FLOAT, getFragmentManager());
            }
        });
        diameter.password = true;
        diameter.setIcon(R.drawable.main_icon_diametr);
        tab2.add(diameter);

        MenuItem horizontal = new MenuItem(R.string.ruler_horz_header, R.string.ruler_horz_text, "rulerhorz", new OnMenuClick() {
            @Override
            public void onClick() {
                App.openKeyboard("rulerhorz", getFragmentManager());
            }
        });
        horizontal.password = true;
        horizontal.setIcon(R.drawable.main_icon_gorizont);
        tab3.add(horizontal);

        MenuItem vertical = new MenuItem(R.string.ruler_vert_header, R.string.ruler_vert_text, "rulervert", new OnMenuClick() {
            @Override
            public void onClick() {
                App.openKeyboard("rulervert", getFragmentManager());
            }
        });
        vertical.password = true;
        vertical.setIcon(R.drawable.main_icon_vertikal);
        tab3.add(vertical);

        MenuItem radius = new MenuItem(R.string.ruler_radius_header, R.string.ruler_radius_text, "rulerrad", new OnMenuClick() {
            @Override
            public void onClick() {
                App.openKeyboard("rulerrad", getFragmentManager());
            }
        });
        radius.password = true;
        radius.setIcon(R.drawable.main_icon_znachenie_radiusa);
        tab3.add(radius);

        MenuItem distance = new MenuItem(R.string.wheel_distance_header, R.string.wheel_distance_text, "wheeldist", new OnMenuClick() {
            @Override
            public void onClick() {
                App.openKeyboard("wheeldist", getFragmentManager());
            }
        });
        distance.password = true;
        distance.setIcon(R.drawable.main_icon_razmer_vyleta);
        tab3.add(distance);

        tabs.add(tab1);
        tabs.add(tab2);
        tabs.add(tab3);
    }

    @Subscribe
    public void updateParams(HashMap<String, Float> params) {
        super.updateParams(params);
    }

    @Subscribe
    public void updateParams(AppEvents action) {
        super.updateParams(action);
    }
}