package sibek.balancing.fragments.menu;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.util.Pair;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

import sibek.balancing.App;
import sibek.balancing.R;
import sibek.balancing.adapter.CustomListViewAdapter;
import sibek.balancing.adapter.RadioListViewAdapter;
import sibek.balancing.fragments.BaseFragment;
import sibek.balancing.menu.MenuItem;
import sibek.balancing.tools.AppEvents;
import sibek.balancing.tools.TypesParams;
import sibek.balancing.view.SlidingTabLayout;

public abstract class BaseMenuFragment extends BaseFragment {
    protected ArrayList<String> tabsTitles = new ArrayList<>();
    protected ArrayList<ArrayList<MenuItem>> tabs = new ArrayList<>();
    protected Context context;
    protected HashMap<Integer, CustomListViewAdapter> tabsAdapters = new HashMap<>();
    protected RadioListViewAdapter radioListViewAdapter;
    protected int currentTabPosition;

    public BaseMenuFragment() {
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // Get the ViewPager and set it's PagerAdapter so that it can display items
        ViewPager mViewPager = (ViewPager) view.findViewById(R.id.viewpager);
        mViewPager.setAdapter(new SamplePagerAdapter());

        // Give the SlidingTabLayout the ViewPager, this must be
        // done AFTER the ViewPager has had it's PagerAdapter set.
        SlidingTabLayout tabsView = (SlidingTabLayout) view.findViewById(R.id.tabs);
        tabsView.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width

        // Setting Custom Color for the Scroll bar indicator of the Tab View
        tabsView.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.tabsScrollColor);
            }
        });

        ViewPager.SimpleOnPageChangeListener pageChangeListener = new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                currentTabPosition = position;
            }
        };
        tabsView.setOnPageChangeListener(pageChangeListener);

        tabsView.setViewPager(mViewPager);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_tabs, container, false);
        context = getActivity();
        return rootView;
    }

    class SamplePagerAdapter extends PagerAdapter {

        /**
         * Return the number of pages to display
         */
        @Override
        public int getCount() {
            return tabsTitles.size();
        }

        /**
         * Return true if the value returned from is the same object as the View
         * added to the ViewPager.
         */
        @Override
        public boolean isViewFromObject(View view, Object o) {
            return o == view;
        }

        /**
         * Return the title of the item at position. This is important as what
         * this method returns is what is displayed in the SlidingTabLayout.
         */
        @Override
        public CharSequence getPageTitle(int position) {
            return tabsTitles.get(position);
        }

        /**
         * Instantiate the View which should be displayed at position. Here we
         * inflate a layout from the apps resources and then change the text
         * view to signify the position.
         */
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View tab = tabBuilder(container, position);
            container.addView(tab);
            return tab;
        }

        /**
         * Destroy the item from the ViewPager. In our case this is simply
         * removing the View.
         */
        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }

    public View tabBuilder(ViewGroup container, int position) {
        // Inflate a new layout from our resources
        View tab = getActivity().getLayoutInflater().inflate(R.layout.tab_layout, container, false);
        ListView listView = (ListView) tab.findViewById(R.id.items);

        CustomListViewAdapter customAdapter = new CustomListViewAdapter(context, tabs.get(position));
        listView.setAdapter(customAdapter);
        tabsAdapters.put(position, customAdapter);
        setItemsDialog(listView, position);
        return tab;
    }

    public void setItemsDialog(ListView items, int tabPostion) {
        final int tabPos = tabPostion;
        items.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                final MenuItem element = tabs.get(tabPos).get(position);

                if (element.getSubItems().size() > 0) {
                    //build popup with radio list
                    radioListViewAdapter = new RadioListViewAdapter(App.getContext(), element.getSubItems());
                    final ListView list = new ListView(App.getContext());
                    list.setAdapter(radioListViewAdapter);
                    list.setDivider(new ColorDrawable(Color.parseColor("#d9dada")));
                    list.setDividerHeight(2);

                    final String machineParam = element.getMachineParam();
                    int machineParamValue = 0;
                    if (App.isMachineParam(machineParam)) {
                        machineParamValue = (int) App.getParam(element.getMachineParam()).second;
                    } else {
                        try {
                            machineParamValue = Integer.parseInt(App.getProfileStringParam(element.getMachineParam()));
                        } catch (NumberFormatException e) {
                            machineParamValue = 0;
                        }
                    }
                    radioListViewAdapter.setSelectedIndex(machineParamValue);

                    list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            MenuItem subElement = element.getSubItems().get(position);
                            if (subElement.method != null) {
                                subElement.click();
                            }
                            final Dialog dialog = (Dialog) list.getTag(R.id.dialog);
                            if (dialog != null) {
                                dialog.dismiss();
                            }
                            App.setIntParam(machineParam, position);
                            radioListViewAdapter.setSelectedIndex(position);
                        }
                    });

                    App.openMenuPopup(list, getActivity());
                } else {
                    if (element.password) {
                        if (App.getProfileBooleanParam("password")) {
                            element.click();
                        } else {
                            checkUserAccess();
                        }
                    } else {
                        element.click();
                    }
                }
            }
        });
    }

    public void updateParams(HashMap<String, Float> params) {
        CustomListViewAdapter tab = tabsAdapters.get(currentTabPosition);
        if (tab != null) {
            ArrayList<MenuItem> elements = tab.getElements();
            if (elements != null) {
                for (MenuItem tabItem : elements) {
                    String machineParam = tabItem.getMachineParam();
                    if (machineParam != null) {
                        if (App.isMachineParam(machineParam)) {
                            Pair<TypesParams, Object> machineValue = App.getParam(machineParam);
                            Pair<TypesParams, Object> tabItemValue = tabItem.getValue();
                            if (tabItemValue == null || !tabItemValue.equals(machineValue)) {
                                tabItem.setValue(machineValue);
                                tab.notifyDataSetChanged();
                            }
                        }
                    }
                }
            }
        }
    }

    public void updateParams(AppEvents action) {
        if (action == AppEvents.PREFERENCE_CHANGE || action == AppEvents.IP_CHANGED) {
            CustomListViewAdapter tab = tabsAdapters.get(currentTabPosition);
            if (tab != null) {
                ArrayList<MenuItem> elements = tab.getElements();
                if (elements != null) {
                    for (MenuItem tabItem : elements) {
                        String valueParam = tabItem.getMachineParam();
                        if (valueParam != null) {
                            if (!App.isMachineParam(valueParam)) {
                                Pair<TypesParams, Object> machineValue = new Pair<TypesParams, Object>(TypesParams.LOCAL_PREFERENCE, App.getProfileStringParam(valueParam));
                                Pair<TypesParams, Object> tabItemValue = tabItem.getValue();
                                if (tabItemValue == null || !tabItemValue.equals(machineValue)) {
                                    tabItem.setValue(machineValue);
                                    tab.notifyDataSetChanged();
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    protected void checkUserAccess() {
        App.openKeyboard("password", TypesParams.INT, getFragmentManager());
        App.showMessageFromResource(R.string.enter_password, Toast.LENGTH_SHORT);
    }
}