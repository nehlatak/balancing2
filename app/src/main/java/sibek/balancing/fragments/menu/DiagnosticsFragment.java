package sibek.balancing.fragments.menu;

import android.util.Log;

import com.squareup.otto.Subscribe;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import sibek.balancing.App;
import sibek.balancing.R;
import sibek.balancing.menu.MenuItem;
import sibek.balancing.router.VarsRouter;
import sibek.balancing.router.routes.Default;
import sibek.balancing.router.routes.Diagnostic;
import sibek.balancing.router.routes.Route;
import sibek.balancing.tools.AppEvents;

public class DiagnosticsFragment extends BaseMenuFragment {
    private static VarsRouter router = VarsRouter.getInstance();
    private Route defRoute = new Default();
    private Route diagRoute = new Diagnostic();

    public DiagnosticsFragment() {
        if (App.oldMachineType) {
            tabsTitles.add(App.getStringFromResources(R.string.arm_voltages_header));
            tabsTitles.add(App.getStringFromResources(R.string.dsp_voltages_header));
            tabsTitles.add(App.getStringFromResources(R.string.capacitance_header));
            tabsTitles.add(App.getStringFromResources(R.string.resistance_header));

            ArrayList<MenuItem> tab1 = new ArrayList<>();
            ArrayList<MenuItem> tab2 = new ArrayList<>();
            ArrayList<MenuItem> tab3 = new ArrayList<>();
            ArrayList<MenuItem> tab4 = new ArrayList<>();

            MenuItem voltage0 = new MenuItem(R.string.v0_header, R.string.v0_text, "v0");
            voltage0.setIcon(R.drawable.main_icon_uprav_protsessor);
            tab1.add(voltage0);

            MenuItem voltage1 = new MenuItem(R.string.v1_header, R.string.v1_text, "v1");
            voltage1.setIcon(R.drawable.main_icon_uprav_protsessor);
            tab1.add(voltage1);

            MenuItem voltage2 = new MenuItem(R.string.v2_header, R.string.v2_text, "v2");
            voltage2.setIcon(R.drawable.main_icon_uprav_protsessor);
            tab1.add(voltage2);

            MenuItem voltage4 = new MenuItem(R.string.v4_header, R.string.v4_text, "v4");
            voltage4.setIcon(R.drawable.main_icon_uprav_protsessor);
            tab1.add(voltage4);

            MenuItem voltage5 = new MenuItem(R.string.v5_header, R.string.v5_text, "v5");
            voltage5.setIcon(R.drawable.main_icon_uprav_protsessor);
            tab1.add(voltage5);

            MenuItem voltage0va = new MenuItem(R.string.va0_header, R.string.va0_text, "va0");
            voltage0va.setIcon(R.drawable.main_icon_signal_protsessor);
            tab2.add(voltage0va);

            MenuItem voltage1va = new MenuItem(R.string.va1_header, R.string.va1_text, "va1");
            voltage1va.setIcon(R.drawable.main_icon_signal_protsessor);
            tab2.add(voltage1va);

            MenuItem voltage2va = new MenuItem(R.string.va2_header, R.string.va2_text, "va2");
            voltage2va.setIcon(R.drawable.main_icon_signal_protsessor);
            tab2.add(voltage2va);

            MenuItem voltage3va = new MenuItem(R.string.va3_header, R.string.va3_text, "va3");
            voltage3va.setIcon(R.drawable.main_icon_signal_protsessor);
            tab2.add(voltage3va);

            MenuItem voltage4va = new MenuItem(R.string.va4_header, R.string.va4_text, "va4");
            voltage4va.setIcon(R.drawable.main_icon_signal_protsessor);
            tab2.add(voltage4va);

            MenuItem voltage5va = new MenuItem(R.string.va5_header, R.string.va5_text, "va5");
            voltage5va.setIcon(R.drawable.main_icon_signal_protsessor);
            tab2.add(voltage5va);

            MenuItem voltage6va = new MenuItem(R.string.va6_header, R.string.va6_text, "va6");
            voltage6va.setIcon(R.drawable.main_icon_signal_protsessor);
            tab2.add(voltage6va);

            MenuItem voltage7va = new MenuItem(R.string.va7_header, R.string.va7_text, "va7");
            voltage7va.setIcon(R.drawable.main_icon_signal_protsessor);
            tab2.add(voltage7va);

            MenuItem capacitance0 = new MenuItem(R.string.capacitance_0_header, R.string.capacitance_0_text, "c0");
            capacitance0.setIcon(R.drawable.main_icon_yomkost);
            capacitance0.setOnClickListener(new OnMenuClick() {
                @Override
                public void onClick() {
                    App.sendCommand("c-meter");
                }
            });
            tab3.add(capacitance0);

            MenuItem capacitance1 = new MenuItem(R.string.capacitance_1_header, R.string.capacitance_1_text, "c1");
            capacitance1.setIcon(R.drawable.main_icon_yomkost);
            capacitance1.setOnClickListener(new OnMenuClick() {
                @Override
                public void onClick() {
                    App.sendCommand("c-meter");
                }
            });
            tab3.add(capacitance1);

            MenuItem capacitance2 = new MenuItem(R.string.capacitance_2_header, R.string.capacitance_2_text, "c2");
            capacitance2.setIcon(R.drawable.main_icon_yomkost);
            capacitance2.setOnClickListener(new OnMenuClick() {
                @Override
                public void onClick() {
                    App.sendCommand("c-meter");
                }
            });
            tab3.add(capacitance2);

            MenuItem capacitance3 = new MenuItem(R.string.capacitance_3_header, R.string.capacitance_3_text, "c3");
            capacitance3.setIcon(R.drawable.main_icon_yomkost);
            capacitance3.setOnClickListener(new OnMenuClick() {
                @Override
                public void onClick() {
                    App.sendCommand("c-meter");
                }
            });
            tab3.add(capacitance3);

            MenuItem resistance0 = new MenuItem(R.string.resistance_0_header, R.string.resistance_0_text, "r0");
            resistance0.setIcon(R.drawable.main_icon_lineyka_vylet);
            tab4.add(resistance0);

            MenuItem resistance1 = new MenuItem(R.string.resistance_1_header, R.string.resistance_1_text, "r1");
            resistance1.setIcon(R.drawable.main_icon_lineyka_vylet);
            tab4.add(resistance1);

            tabs.add(tab1);
            tabs.add(tab2);
            tabs.add(tab3);
            tabs.add(tab4);
        } else {
            String str2 = "[ { \"title\":\"Napruga\", \"values\": [ \"+3V3S\", \"+5VL\", \"+24VL\", \"V1V\", \"VUSBL\", \"VBUSL\", \"VRF\" ] }, { \"title\":\"Toki\", \"values\": [\"V1C\", \"V2C\", \"V5C\", \"V7C\" ] }, { \"title\":\"Levizna\", \"values\": [\"3V3EL\", \"RG1F\", \"RG2F\", \"RG3F\", \"V5V\", \"V7V\" ] } ]";
            //TODO: параметры должны браться из vlist
            try {
                JSONArray json = new JSONArray(str2);
                for (int i = 0; i < json.length(); i++) {
                    JSONObject jsonTab = json.getJSONObject(i);
                    String tabName = jsonTab.getString("title");

                    tabsTitles.add(tabName);
                    ArrayList<MenuItem> tab = new ArrayList<>();

                    JSONArray jsonArray = jsonTab.getJSONArray("values");

                    for (int j = 0; j < jsonArray.length(); j++) {
                        String varName = jsonArray.getString(j);

                        MenuItem menuItem = new MenuItem()
                                .setTitle(varName)
                                .setDescription("Описание элемента")
                                .setIcon(R.drawable.main_icon_uprav_protsessor)
                                .setMachineParam(varName);
                        tab.add(menuItem);
                    }
                    tabs.add(tab);
                }
            } catch (Exception e) {
                Log.d("DiagnosticsFragment", "Error with parse diagnostic menu!");
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (App.oldMachineType) {
            router.setRoute(defRoute);
        } else {
            router.setRoute(diagRoute);
        }
    }

    @Subscribe
    public void updateParams(HashMap<String, Float> params) {
        super.updateParams(params);
    }

    @Subscribe
    public void updateParams(AppEvents action) {
        super.updateParams(action);
    }
}