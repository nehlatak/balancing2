package sibek.balancing.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import sibek.balancing.App;
import sibek.balancing.R;
import sibek.balancing.tools.AppEvents;
import sibek.balancing.tools.BalanceParams;
import sibek.balancing.tools.TypesParams;

public class KeyboardFragment extends DialogFragment {
    public String valueParam = "";
    public String machineValue;
    public TypesParams valueType = TypesParams.INT;
    private boolean deleteFlag = false;
    private boolean clearFlag = false;
    private Pattern pattern;
    private Matcher matcher;

    private static final String IPADDRESS_PATTERN = "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
    private static final String FLOAT_PATTERN = "^(\\d{1,3})(\\.{0,1})(\\d{0,2})$";
    private static final String INT_PATTERN = "^(\\d{1,3})$";
    private static final int PASSWORD = 679;

    private TextView inputText;
    private HashMap<Integer, Button> buttons = new HashMap<>();
    private Button delete;
    private Button buttonC;
    private Button buttonComma;
    private Button enter;
    private ImageView imageParam;
    private Dialog curDialog;
    private boolean shown = false;

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        }
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        if (shown) return;

        super.show(manager, tag);
        shown = true;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        shown = false;
        super.onDismiss(dialog);
    }

    public boolean isShown() {
        return shown;
    }

    @Override
    public void dismiss() {
        if (this.isShown()) {
            super.dismiss();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        switch (valueParam) {
            case "offset":
                imageParam.setBackground(ContextCompat.getDrawable(App.getContext(), R.drawable.keyboard_arrow_right));
                break;
            case "stick":
            case "width":
                imageParam.setBackground(ContextCompat.getDrawable(App.getContext(), R.drawable.keyboard_arrow_left_right));
                break;
            case "diam":
                imageParam.setBackground(ContextCompat.getDrawable(App.getContext(), R.drawable.keyboard_arrow_up));
                break;
            default:
                break;
        }

        inputText.setText(machineValue);
        deleteFlag = false;
        clearFlag = false;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.keyboard, null);

        curDialog = getDialog();

        inputText = (TextView) view.findViewById(R.id.input_text);
        delete = (Button) view.findViewById(R.id.button_delete);
        buttonC = (Button) view.findViewById(R.id.buttonC);
        buttonComma = (Button) view.findViewById(R.id.button_comma);
        enter = (Button) view.findViewById(R.id.button_enter);
        imageParam = (ImageView) view.findViewById(R.id.param_image);

        buttons.put(0, (Button) view.findViewById(R.id.button0));
        buttons.put(1, (Button) view.findViewById(R.id.button1));
        buttons.put(2, (Button) view.findViewById(R.id.button2));
        buttons.put(3, (Button) view.findViewById(R.id.button3));
        buttons.put(4, (Button) view.findViewById(R.id.button4));
        buttons.put(5, (Button) view.findViewById(R.id.button5));
        buttons.put(6, (Button) view.findViewById(R.id.button6));
        buttons.put(7, (Button) view.findViewById(R.id.button7));
        buttons.put(8, (Button) view.findViewById(R.id.button8));
        buttons.put(9, (Button) view.findViewById(R.id.button9));

        Iterator it = buttons.entrySet().iterator();
        while (it.hasNext()) {
            final Map.Entry pair = (Map.Entry) it.next();
            ((Button) pair.getValue()).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String text = inputText.getText().toString();
                    if (App.isMachineParam(valueParam)) {
                        if (deleteFlag) {
                            if (text.length() < 3 || (text.contains(".") && text.length() < 4)) {
                                inputText.append(pair.getKey().toString());
                            }
                        } else {
                            if (clearFlag) {
                                if (text.length() < 3 || (text.contains(".") && text.length() < 4)) {
                                    inputText.append(pair.getKey().toString());
                                }
                            } else {
                                clearFlag = true;
                                inputText.setText(pair.getKey().toString());
                            }
                        }
                    } else {
                        if (text.length() < 15) {
                            inputText.append(pair.getKey().toString());
                        }
                    }
                }
            });
            it.remove();
        }

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteFlag = true;
                int length = inputText.getText().length();
                if (length > 0) {
                    String text = inputText.getText().toString();
                    inputText.setText(text.substring(0, text.length() - 1));
                }
            }
        });

        buttonC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputText.setText("");
            }
        });

        buttonComma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!valueParam.equals("indication_length")) {
                    String text = inputText.getText().toString();
                    Log.d("DEBUG", String.valueOf(valueType));
                    if (valueType == TypesParams.FLOAT) {
                        if (text.length() != 3) {
                            if (!text.contains(".")) {
                                inputText.append(".");
                            }
                        }
                    }
                    if (valueType == TypesParams.LOCAL_PREFERENCE) {
                        inputText.append(".");
                    }
                }
            }
        });

        enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String result = inputText.getText().toString();
                if (!result.isEmpty()) {
                    switch (valueType) {
                        case FLOAT:
                            if (validateFloat(result)) {
                                try {
                                    float valueFloat = Float.parseFloat(result);
                                    if (App.isMachineParam(valueParam)) {
                                        App.setFloatParam(valueParam, valueFloat);
                                        playKeyboardSound(valueFloat, valueParam, TypesParams.FLOAT);
                                    } else {
                                        App.setProfileStringParam(valueParam, String.valueOf(valueFloat));
                                    }
                                } catch (Exception e) {
                                    Log.d("KEYBOARD", String.valueOf("wrong float value!"));
                                }
                            }
                            break;
                        case INT:
                            if (validateInt(result)) {
                                try {
                                    int valueInt = Integer.parseInt(result);
                                    if (App.isMachineParam(valueParam)) {
                                        App.setIntParam(valueParam, valueInt);
                                        playKeyboardSound(valueInt, valueParam, TypesParams.INT);
                                    } else {
                                        if (valueParam.equals("password")) {
                                            if (valueInt == PASSWORD) {
                                                App.setProfileBooleanParam("password", true);
                                            } else {
                                                App.showMessageFromResource(R.string.invalid_password, Toast.LENGTH_LONG);
                                            }
                                        }
                                        if (!valueParam.equals("password")) {
                                            App.setProfileStringParam(valueParam, String.valueOf(valueInt));
                                        }
                                    }
                                } catch (Exception e) {
                                    Log.d("KEYBOARD", String.valueOf("wrong int value!"));
                                }
                            }
                            break;
                        case LOCAL_PREFERENCE:
                            switch (valueParam) {
                                case "server_addr":
                                    if (validateIp(result)) {
                                        App.setProfileStringParam(valueParam, result);
                                        App.bus.post(AppEvents.IP_CHANGED);
                                    } else {
                                        Log.d("KEYBOARD", String.valueOf("wrong IP value!"));
                                    }
                                    break;
                                case "indication_length":
                                    int length = Integer.parseInt(result);
                                    if (length > 0 && length < 50) {
                                        App.setProfileStringParam(valueParam, result);
                                    }
                                    break;
                                default:
                                    App.setProfileStringParam(valueParam, result);
                                    break;
                            }
                            App.bus.post(AppEvents.PREFERENCE_CHANGE);
                            break;
                        default:
                            break;
                    }
                }
                curDialog.dismiss();
            }
        });

        return view;
    }

    private void playKeyboardSound(float enteredValue, String param, TypesParams type) {
        if (param.equals("width") || param.equals("stick") || param.equals("diam") || param.equals("offset")) {
            switch (type) {
                case FLOAT:
                    float currentIntValue = App.getFloatParam(param);
                    if (currentIntValue != (int) enteredValue) {
                        App.playSound(BalanceParams.SOUND_NORMAL, "sounds/ru/recalc.wav");
                    }
                    break;
                case INT:
                    int currentFloatValue = App.getIntParam(param);
                    if (currentFloatValue != enteredValue) {
                        App.playSound(BalanceParams.SOUND_NORMAL, "sounds/ru/recalc.wav");
                    }
                    break;
            }
        }
    }

    private boolean validateIp(final String ip) {
        pattern = Patterns.IP_ADDRESS;
        matcher = pattern.matcher(ip);
        return matcher.matches();
    }

    private boolean validateFloat(final String floatValue) {
        return isDouble(floatValue);
    }

    private boolean validateInt(final String intValue) {
        return isInteger(intValue);
    }

    public static boolean isInteger(String s) {
        return isInteger(s,10);
    }

    public static boolean isInteger(String s, int radix) {
        if(s.isEmpty()) return false;
        for(int i = 0; i < s.length(); i++) {
            if(i == 0 && s.charAt(i) == '-') {
                if(s.length() == 1) return false;
                else continue;
            }
            if(Character.digit(s.charAt(i),radix) < 0) return false;
        }
        return true;
    }

    public static boolean isDouble(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}