package sibek.balancing.fragments;

import android.support.v4.app.Fragment;

import sibek.balancing.App;

public class BaseFragment extends Fragment {

    @Override
    public void onStart() {
        super.onStart();
        App.bus.register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        App.bus.unregister(this);
    }
}