package sibek.balancing.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.squareup.otto.Subscribe;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.HashMap;

import sibek.balancing.App;
import sibek.balancing.R;
import sibek.balancing.activities.ErrorsActivity;
import sibek.balancing.tools.AppEvents;
import sibek.balancing.tools.BalanceParams;
import sibek.balancing.tools.TypesParams;
import sibek.balancing.view.PathView;

public class MainScreenFragment extends BaseFragment {
    private boolean weightFlag = true;
    private boolean widthBlinkFlag = false;
    private boolean diamBlinkFlag = false;
    private boolean offsetBlinkFlag = false;
    private int spokeValue = 0;
    private int mode = 0;
    private int layout = 0;
    private HashMap<Integer, Integer> diskSpokesImages = new HashMap<>();
    private HashMap<Integer, Integer> diskImages = new HashMap<>();
    private HashMap<Integer, Integer> staticModeImages = new HashMap<>();
    private HashMap<Integer, Integer> dynamicModeImages = new HashMap<>();
    private int splitMode = 1;
    private int wheelangle = 0;
    private int wheelangle0 = 0;
    private int wheelangle1 = 0;
    private int wheelangle2 = 0;
    private int angleDelta0 = 0;
    private int angleDelta1 = 0;
    private int angleDelta2 = 0;
    //    private int angleepsilon = 4;
    private int weight1 = 0;
    private int weight2 = 0;
    private int weight3 = 0;
    private int user = 0;
    private DecimalFormat df = new DecimalFormat("#.#");
    private DecimalFormat df2 = new DecimalFormat("#.#");
    private DecimalFormatSymbols customSymbol = new DecimalFormatSymbols();
    private int indicationAngleLength;
    private double allowableAngle;
    private int leftEdge0;
    private int rightEdge0;
    private int leftEdge1;
    private int rightEdge1;
    private int leftEdge2;
    private int rightEdge2;
    private boolean bob0Allowed = false;
    private boolean bob1Allowed = false;
    private boolean bob2Allowed = false;
    private int balanceState = BalanceParams.STATE_IDLE;
    private int balanceSubstate = BalanceParams.BALANCE_IDLE;
    private int newBalanceState;
    private int newBalanceSubstate;
    private int balanceResult;
    private int newBalanceResult;
    private int newBalanceErrors0;
    private int newBalanceErrors1;
    private int newBalanceErrors2;
    private int balanceErrors0;
    private int balanceErrors1;
    private int balanceErrors2;

    private Vibrator vibrator;
    private Button startButton;
    private Button stopButton;
    private Button diskButton;
    private Button bobButton;
    private ToggleButton userButton;
    private TextView offset;
    private TextView width;
    private TextView diam;
    private ImageView diskImage;
    private ImageView bobImage;
    private ImageView wheelLeft;
    private ImageView wheelLeftWrapper;
    private ImageView wheelRight;
    private ImageView wheelRightWrapper;
    private ImageView leftBob;
    private ImageView rightBob1;
    private ImageView rightBob2;
    private ImageView bob1;
    private ImageView bob2;
    private ImageView bob3;
    private ImageView bob4;
    private ImageView bob5;
    private ImageView twelveOClock;
    private ImageView calculatedOffset;
    private ImageView intelligence;
    private ImageView connectionError;
    private ImageView error;
    private ImageView rulerMiddle;
    private ImageView rulerSetDirection;
    private Button leftWeight;
    private Button rightWeight;
    private Button rightWeight2;
    private FragmentManager fragmentManager;
    private DiskFragment diskFragment;
    private BobFragment bobFragment;
    private RelativeLayout leftControls;
    private RelativeLayout rightControls;
    private LinearLayout ruler;
    private LinearLayout leftWeightContainer;
    private LinearLayout rightWeightContainer;
    private TextView rulerDirectionValue;
    private FrameLayout offsetContainer;
    private FrameLayout diamContainer;
    private FrameLayout widthContainer;
    private LinearLayout rulerDirectionContainer;
    private RelativeLayout.LayoutParams lp;
    private ProgressFragment progressDialog = new ProgressFragment();
    private MessageFragment coverMessage = new MessageFragment();
    private MessageFragment wheelMessage = new MessageFragment();
    private PathView weight0TopArrow;
    private PathView weight0BottomArrow;
    private PathView weight1TopArrow;
    private PathView weight1BottomArrow;
    private PathView weight2TopArrow;
    private PathView weight2BottomArrow;

    public MainScreenFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        customSymbol.setDecimalSeparator('.');
        df.setDecimalFormatSymbols(customSymbol);
        df2.setDecimalFormatSymbols(customSymbol);

        diskFragment = new DiskFragment();
        bobFragment = new BobFragment();

        diskSpokesImages.put(3, R.drawable.spoke_03);
        diskSpokesImages.put(4, R.drawable.spoke_04);
        diskSpokesImages.put(5, R.drawable.spoke_05);
        diskSpokesImages.put(6, R.drawable.spoke_06);
        diskSpokesImages.put(7, R.drawable.spoke_07);
        diskSpokesImages.put(8, R.drawable.spoke_08);
        diskSpokesImages.put(9, R.drawable.spoke_09);
        diskSpokesImages.put(10, R.drawable.spoke_10);
        diskSpokesImages.put(11, R.drawable.spoke_11);
        diskSpokesImages.put(12, R.drawable.spoke_12);
        diskSpokesImages.put(13, R.drawable.spoke_13);

        diskImages.put(0, R.drawable.alu);
        diskImages.put(1, R.drawable.steel);
        diskImages.put(2, R.drawable.truck);
        diskImages.put(3, R.drawable.abs);
        diskImages.put(4, R.drawable.static_disk);

        dynamicModeImages.put(BalanceParams.LAYOUT_1_5, R.drawable.bob_1_5_default);
        dynamicModeImages.put(BalanceParams.LAYOUT_2_5, R.drawable.bob_2_5_default);
        dynamicModeImages.put(BalanceParams.LAYOUT_1_4, R.drawable.bob_1_4_default);
        dynamicModeImages.put(BalanceParams.LAYOUT_1_3, R.drawable.bob_1_3_default);
        dynamicModeImages.put(BalanceParams.LAYOUT_2_3, R.drawable.bob_2_3_default);
        dynamicModeImages.put(BalanceParams.LAYOUT_2_4, R.drawable.bob_2_4_default);

        staticModeImages.put(BalanceParams.LAYOUT_1, R.drawable.bob_1_default);
        staticModeImages.put(BalanceParams.LAYOUT_5, R.drawable.bob_5_default);
        staticModeImages.put(BalanceParams.LAYOUT_2, R.drawable.bob_2_default);
        staticModeImages.put(BalanceParams.LAYOUT_3, R.drawable.bob_3_default);
        staticModeImages.put(BalanceParams.LAYOUT_4, R.drawable.bob_4_default);

        coverMessage.setMessageText(App.getContext().getString(R.string.push_cover));
        coverMessage.setBackground(R.drawable.lower_cover);
        wheelMessage.setMessageText(App.getContext().getString(R.string.rotate_wheel));
        wheelMessage.setBackground(R.drawable.scroll_wheel);
        indicationAngleLength = Integer.parseInt(App.getProfileStringParam("indication_length"));
    }

    @Override
    public void onResume() {
        super.onResume();

        if (this.fragmentManager == null) {
            this.fragmentManager = getFragmentManager();
        }

        if (
                weight0TopArrow != null &&
                        weight0BottomArrow != null &&
                        weight1TopArrow != null &&
                        weight1BottomArrow != null &&
                        weight2TopArrow != null &&
                        weight2BottomArrow != null
                ) {
            weight0TopArrow.setGrey();
            weight0BottomArrow.setGrey();
            weight1TopArrow.setGrey();
            weight1BottomArrow.setGrey();
            weight2TopArrow.setGrey();
            weight2BottomArrow.setGrey();
        }
    }

    public void setFragmentManager(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main_screen, container, false);
        vibrator = (Vibrator) App.getContext().getSystemService(Context.VIBRATOR_SERVICE);

        startButton = (Button) rootView.findViewById(R.id.start_button);
        stopButton = (Button) rootView.findViewById(R.id.stop_button);
        leftWeight = (Button) rootView.findViewById(R.id.left_weight);
        rightWeight = (Button) rootView.findViewById(R.id.right_weight);
        rightWeight2 = (Button) rootView.findViewById(R.id.right_weight2);
        userButton = (ToggleButton) rootView.findViewById(R.id.user_switch);
        offset = (TextView) rootView.findViewById(R.id.offset);
        width = (TextView) rootView.findViewById(R.id.width);
        diam = (TextView) rootView.findViewById(R.id.diam);
        diskButton = (Button) rootView.findViewById(R.id.disk_button);
        bobButton = (Button) rootView.findViewById(R.id.bob_button);
        diskImage = (ImageView) rootView.findViewById(R.id.disk_value_image);
        bobImage = (ImageView) rootView.findViewById(R.id.bob_value_image);
        wheelLeft = (ImageView) rootView.findViewById(R.id.wheelLeft);
        wheelRight = (ImageView) rootView.findViewById(R.id.wheelRight);
        wheelRightWrapper = (ImageView) rootView.findViewById(R.id.right_wheel_wrapper);
        wheelLeftWrapper = (ImageView) rootView.findViewById(R.id.left_wheel_wrapper);
        leftBob = (ImageView) rootView.findViewById(R.id.left_bob);
        rightBob1 = (ImageView) rootView.findViewById(R.id.right_bob_1);
        rightBob2 = (ImageView) rootView.findViewById(R.id.right_bob_2);
        bob1 = (ImageView) rootView.findViewById(R.id.bob_1);
        bob2 = (ImageView) rootView.findViewById(R.id.bob_2);
        bob3 = (ImageView) rootView.findViewById(R.id.bob_3);
        bob4 = (ImageView) rootView.findViewById(R.id.bob_4);
        bob5 = (ImageView) rootView.findViewById(R.id.bob_5);
        twelveOClock = (ImageView) rootView.findViewById(R.id.twelve_o_clock_offset);
        calculatedOffset = (ImageView) rootView.findViewById(R.id.calculated_offset);
        intelligence = (ImageView) rootView.findViewById(R.id.intelligence);
        connectionError = (ImageView) rootView.findViewById(R.id.connection_error);
        error = (ImageView) rootView.findViewById(R.id.error);
        rulerMiddle = (ImageView) rootView.findViewById(R.id.middle_part);
        rulerSetDirection = (ImageView) rootView.findViewById(R.id.ruler_direction_set);
        leftControls = (RelativeLayout) rootView.findViewById(R.id.left_controls);
        rightControls = (RelativeLayout) rootView.findViewById(R.id.right_controls);
        ruler = (LinearLayout) rootView.findViewById(R.id.ruler);
        leftWeightContainer = (LinearLayout) rootView.findViewById(R.id.left_weight_container);
        rightWeightContainer = (LinearLayout) rootView.findViewById(R.id.right_weight_container);
        rulerDirectionValue = (TextView) rootView.findViewById(R.id.ruler_direction_value);
        offsetContainer = (FrameLayout) rootView.findViewById(R.id.offset_container);
        diamContainer = (FrameLayout) rootView.findViewById(R.id.diam_container);
        widthContainer = (FrameLayout) rootView.findViewById(R.id.width_container);
        rulerDirectionContainer = (LinearLayout) rootView.findViewById(R.id.ruler_direction_container);
        weight0TopArrow = (PathView) rootView.findViewById(R.id.weight0_top_arrow);
        weight0BottomArrow = (PathView) rootView.findViewById(R.id.weight0_bot_arrow);
        weight1TopArrow = (PathView) rootView.findViewById(R.id.weight1_top_arrow);
        weight1BottomArrow = (PathView) rootView.findViewById(R.id.weight1_bot_arrow);
        weight2TopArrow = (PathView) rootView.findViewById(R.id.weight2_top_arrow);
        weight2BottomArrow = (PathView) rootView.findViewById(R.id.weight2_bot_arrow);

        lp = (RelativeLayout.LayoutParams) ruler.getLayoutParams();

        userButton.setChecked(false);

        userButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userButton.isChecked()) {
                    App.setIntParam("user", 1);
                    App.playSound(BalanceParams.SOUND_NORMAL, "sounds/ru/user_2_selected.wav");
                } else {
                    App.setIntParam("user", 0);
                    App.playSound(BalanceParams.SOUND_NORMAL, "sounds/ru/user_1_selected.wav");
                }
            }
        });

        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (App.balanceErrors0 == 0) {
                    App.sendCommand("start");
                }
//                debugInfo();
            }
        });

        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                App.sendCommand("stop");
            }
        });

        stopButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        weightFlag = false;
                        if (balanceState == BalanceParams.STATE_IDLE && (weight1 != 0 || weight2 != 0 || weight3 != 0)) {
                            App.playSound(BalanceParams.SOUND_NORMAL, "sounds/ru/round_off.wav");
                        }
                        return false;
                    case MotionEvent.ACTION_UP:
                        weightFlag = true;
                        if (balanceState == BalanceParams.STATE_IDLE && (weight1 != 0 || weight2 != 0 || weight3 != 0)) {
                            App.playSound(BalanceParams.SOUND_NORMAL, "sounds/ru/round_on.wav");
                        }
                        return false;
                }
                return false;
            }
        });

        leftWeight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                App.setIntParam("rotate", 0);
                App.playSound(BalanceParams.SOUND_NORMAL, "sounds/ru/auto_rotation_left.wav");
            }
        });

        rightWeight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                App.setIntParam("rotate", 1);
                App.playSound(BalanceParams.SOUND_NORMAL, "sounds/ru/auto_rotation_right.wav");
            }
        });

        rightWeight2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                App.setIntParam("rotate", 2);
                App.playSound(BalanceParams.SOUND_NORMAL, "sounds/ru/auto_rotation_right.wav");
            }
        });

        offset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                App.openKeyboard("offset", offset.getText().toString(), TypesParams.INT, fragmentManager);
            }
        });

        width.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mode == BalanceParams.MODE_ALU && (layout == BalanceParams.LAYOUT_1_3 || layout == BalanceParams.LAYOUT_2_3)) {
                    App.openKeyboard("stick", width.getText().toString(), TypesParams.FLOAT, fragmentManager);
                } else {
                    App.openKeyboard("width", width.getText().toString(), TypesParams.FLOAT, fragmentManager);
                }
            }
        });

        diam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                App.openKeyboard("diam", diam.getText().toString(), TypesParams.FLOAT, fragmentManager);
            }
        });

        diskButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction fTrans = fragmentManager.beginTransaction();
//                fTrans.setCustomAnimations(R.animator.slide_in_left, R.animator.slide_out_right, 0, 0);
                fTrans.replace(R.id.container_body, diskFragment);
                fTrans.addToBackStack(null);
                fTrans.commit();
            }
        });

        bobButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction fTrans = fragmentManager.beginTransaction();
//                fTrans.setCustomAnimations(R.animator.slide_in_left, R.animator.slide_out_right, 0, 0);
                fTrans.replace(R.id.container_body, bobFragment);
                fTrans.addToBackStack(null);
                fTrans.commit();
            }
        });

        error.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                error.setVisibility(View.GONE);
                Intent intent = new Intent(App.getContext(), ErrorsActivity.class);
                startActivity(intent);
            }
        });

        triggerSplitMode();
        return rootView;
    }

    @Subscribe
    public void setParams(HashMap<String, Float> params) {
        weight1 = App.getIntParam("rndweight0");
        weight2 = App.getIntParam("rndweight1");
        weight3 = App.getIntParam("rndweight2");
        splitMode = App.getIntParam("split");
//        angleepsilon = App.getIntParam("angleepsilon");
        spokeValue = App.getIntParam("numsp");
        user = App.getIntParam("user");
        mode = App.getIntParam("mode");
        layout = App.unpackLayout(App.getIntParam("layout"), mode);
        wheelangle = App.getIntParam("wheelangle") / 2;
        wheelangle0 = App.getIntParam("wheelangle0") / 2;
        wheelangle1 = App.getIntParam("wheelangle1") / 2;
        wheelangle2 = App.getIntParam("wheelangle2") / 2;
        angleDelta0 = Math.abs(wheelangle - wheelangle0);
        angleDelta1 = Math.abs(wheelangle - wheelangle1);
        angleDelta2 = Math.abs(wheelangle - wheelangle2);

        /*Log.d("DEBUG", "wheelangle = " + String.valueOf(wheelangle));
        Log.d("DEBUG", "wheelangle0 = " + String.valueOf(wheelangle0));
        Log.d("DEBUG", "wheelangle1 = " + String.valueOf(wheelangle1));
        Log.d("DEBUG", "wheelangle2 = " + String.valueOf(wheelangle2));
        Log.d("DEBUG", String.valueOf("----------------------------------------"));*/

        double wheelLength = 0;
        if (App.diamValue < 30) {
            wheelLength = Math.PI * App.diamValue * 25.4; //если введено малое значение значит переводим в дюймы
        } else {
            wheelLength = Math.PI * App.diamValue;
        }

        double alfaAngle = 0;
        int splitModeLength = 0;

        if (splitMode != 1) {
            alfaAngle = (indicationAngleLength / wheelLength) * 360;
        } else {
            splitModeLength = (int) ((App.diamValue * 25.4 * Math.PI) / spokeValue);
            splitModeLength *= 0.8;
            splitModeLength /= 2;

            if (splitModeLength < indicationAngleLength) {
                alfaAngle = (splitModeLength / wheelLength) * 360;
            } else {
                alfaAngle = (indicationAngleLength / wheelLength) * 360;
            }
        }

        allowableAngle = alfaAngle / 2;

        leftEdge0 = (int) (((wheelangle0 - allowableAngle) < 0) ? 360 + (wheelangle0 - allowableAngle) : (wheelangle0 - allowableAngle));
        rightEdge0 = (int) (wheelangle0 + allowableAngle);

        leftEdge1 = (int) (((wheelangle1 - allowableAngle) < 0) ? 360 + (wheelangle1 - allowableAngle) : (wheelangle1 - allowableAngle));
        rightEdge1 = (int) (wheelangle1 + allowableAngle);

        leftEdge2 = (int) (((wheelangle2 - allowableAngle) < 0) ? 360 + (wheelangle2 - allowableAngle) : (wheelangle2 - allowableAngle));
        rightEdge2 = (int) (wheelangle2 + allowableAngle);


        leftEdge0 = (leftEdge0 > 360) ? leftEdge0 - 360 : leftEdge0;
        rightEdge0 = (rightEdge0 > 360) ? rightEdge0 - 360 : rightEdge0;

        leftEdge1 = (leftEdge1 > 360) ? leftEdge1 - 360 : leftEdge1;
        rightEdge1 = (rightEdge1 > 360) ? rightEdge1 - 360 : rightEdge1;

        leftEdge2 = (leftEdge2 > 360) ? leftEdge2 - 360 : leftEdge2;
        rightEdge2 = (rightEdge2 > 360) ? rightEdge2 - 360 : rightEdge2;

        bob0Allowed = isBobAllowed(leftEdge0, rightEdge0);
        bob1Allowed = isBobAllowed(leftEdge1, rightEdge1);
        bob2Allowed = isBobAllowed(leftEdge2, rightEdge2);

        drawWheelsArrows();
        paramsBlinking();

        if (App.balanceState == BalanceParams.STATE_RULER) {
            int truemode = App.getIntParam("truemode");
            int autoalu = App.getIntParam("autoalu");
            if (truemode == 0) {
                twelveOClock.setVisibility(View.VISIBLE);
                calculatedOffset.setVisibility(View.GONE);
            } else {
                twelveOClock.setVisibility(View.GONE);
                calculatedOffset.setVisibility(View.VISIBLE);
            }
            if (autoalu == 1) {
                intelligence.setVisibility(View.VISIBLE);
            } else {
                intelligence.setVisibility(View.GONE);
            }
        } else {
            twelveOClock.setVisibility(View.GONE);
            calculatedOffset.setVisibility(View.GONE);
            intelligence.setVisibility(View.GONE);
        }

        boolean showWeights = App.balanceState == BalanceParams.STATE_IDLE ||
                App.balanceState == BalanceParams.STATE_RULER ||
                (App.balanceState == BalanceParams.STATE_BALANCE || (App.balanceState >= BalanceParams.STATE_BALANCE_CAL0 && App.balanceState <= BalanceParams.STATE_BALANCE_CAL3)) &&
                        (App.balanceSubstate == BalanceParams.BALANCE_IDLE || App.balanceSubstate >= BalanceParams.BALANCE_DECEL);

        if (showWeights && (weight1 != 0 || weight2 != 0 || weight3 != 0)) {
            wheelsDraw();
        }

        triggerSplitMode();
        wrapperDraw();
        bobsDraw();

        if (weightFlag) {
            float rndweight0 = App.getFloatParam("rndweight0");
            float rndweight1 = App.getFloatParam("rndweight1");
            float rndweight2 = App.getFloatParam("rndweight2");

            if (rndweight0 > 999) {
                leftWeight.setText(String.valueOf(999));
            } else {
                leftWeight.setText(App.getParamString("rndweight0"));
            }

            if (rndweight1 > 999) {
                rightWeight.setText(String.valueOf(999));
            } else {
                rightWeight.setText(App.getParamString("rndweight1"));
            }

            if (rndweight2 > 999) {
                rightWeight2.setText(String.valueOf(999));
            } else {
                rightWeight2.setText(App.getParamString("rndweight2"));
            }
        } else {
            float weight0 = App.getFloatParam("weight0");
            float weight1 = App.getFloatParam("weight1");
            float weight2 = App.getFloatParam("weight2");

            if (weight0 > 999) {
                leftWeight.setText(String.valueOf(999));
            } else {
                if (weight0 >= 100) {
                    leftWeight.setText(df2.format(weight0));
                } else {
                    leftWeight.setText(df.format(weight0));
                }
            }

            if (weight1 > 999) {
                rightWeight.setText(String.valueOf(999));
            } else {
                if (weight1 >= 100) {
                    rightWeight.setText(df2.format(weight1));
                } else {
                    rightWeight.setText(df.format(weight1));
                }
            }

            if (weight2 > 999) {
                rightWeight2.setText(String.valueOf(999));
            } else {
                if (weight2 >= 100) {
                    rightWeight2.setText(df2.format(weight2));
                } else {
                    rightWeight2.setText(df.format(weight2));
                }
            }
        }

        if (App.balanceState != BalanceParams.STATE_RULER) {
            if (!offset.getText().toString().equals(App.getParamString("offset"))) {
                if (App.offsetValue > 999) {
                    offset.setText(String.valueOf(999));
                } else {
                    offset.setText(App.getParamString("offset"));
                }
            }
            if (!diam.getText().toString().equals(App.getParamString("diam"))) {
                if (App.diamValue > 999) {
                    diam.setText(String.valueOf(999));
                } else {
                    diam.setText(App.getParamString("diam"));
                }
            }
            if (!width.getText().toString().equals(App.getParamString("width"))) {
                if (mode == BalanceParams.MODE_ALU && (layout == BalanceParams.LAYOUT_1_3 || layout == BalanceParams.LAYOUT_2_3)) {
                    int stick = App.getIntParam("stick");
                    if (stick != 0) {
                        width.setText(App.getParamString("stick"));
                    } else {
                        width.setText("Auto");
                    }
                } else {
                    if (App.widthValue > 999) {
                        width.setText(String.valueOf(999));
                    } else {
                        width.setText(App.getParamString("width"));
                    }
                }
            }
        }

        if (splitMode != 0) {
            setSpokeImage(spokeValue);
        } else {
            setDiskImage(mode);
        }

        setBobImage(mode, layout);

        if (user == 1) {
            userButton.setChecked(true);
        } else {
            userButton.setChecked(false);
        }

        //draw cover message
        if ((App.balanceState == BalanceParams.STATE_BALANCE || (App.balanceState >= BalanceParams.STATE_BALANCE_CAL0 && App.balanceState <= BalanceParams.STATE_BALANCE_CAL3))
                && App.balanceSubstate == BalanceParams.BALANCE_WAIT_COVER) {
            showCoverMessage();
        } else {
            hideCoverMessage();
        }

        // draw wheel message
        if (Math.floor(App.balanceErrors0 / (Math.pow(2, 18)) % 2) != 0) {
            showWheelMessage();
        } else {
            hideWheelMessage();
        }

        // handle progress states
        if (App.balanceState == BalanceParams.STATE_IDLE || App.balanceState == BalanceParams.STATE_RULER) {
            hideBalanceProgress();
        } else if (App.balanceState == BalanceParams.STATE_BALANCE || (App.balanceState >= BalanceParams.STATE_BALANCE_CAL0 && App.balanceState <= BalanceParams.STATE_BALANCE_CAL3)) {
            if (App.balanceSubstate == BalanceParams.BALANCE_IDLE) {
                hideBalanceProgress();
            } else if (App.balanceSubstate == BalanceParams.BALANCE_WAIT_COVER) {
                hideBalanceProgress();
            } else if (App.balanceSubstate >= BalanceParams.BALANCE_START && App.balanceSubstate < BalanceParams.BALANCE_AUTO_ROTATION) {
                showBalanceProgress();
            } else {
                hideBalanceProgress();
            }
        } else if ((App.balanceState >= BalanceParams.STATE_RULER_CAL0 && App.balanceState <= BalanceParams.STATE_RULER_CAL3) ||
                (App.balanceState >= BalanceParams.STATE_RRULER_3P_CAL0 && App.balanceState <= BalanceParams.STATE_RRULER_D_CAL1)) {
            hideBalanceProgress();
        }

        newBalanceState = App.balanceState;
        newBalanceSubstate = App.balanceSubstate;

        if (newBalanceState == BalanceParams.STATE_RULER && newBalanceSubstate == BalanceParams.RULER_WAIT && balanceState == BalanceParams.STATE_RULER && (balanceSubstate == BalanceParams.RULER_MEASURE || balanceSubstate == BalanceParams.RULER_MEASURE_L) ||
                newBalanceState == BalanceParams.STATE_RULER_CAL0 && newBalanceSubstate == BalanceParams.RULER_WAIT && balanceState == BalanceParams.STATE_RULER_CAL0 && (balanceSubstate == BalanceParams.RULER_MEASURE || balanceSubstate == BalanceParams.RULER_MEASURE_L)) {
            App.playSound("sounds/ru/ruler_success.wav");
        }

        if (App.errorFlag) {
            error.setVisibility(View.VISIBLE);
        } else {
            error.setVisibility(View.GONE);
        }

        // say about balance states
        if (newBalanceState == BalanceParams.STATE_BALANCE || (newBalanceState >= BalanceParams.STATE_BALANCE_CAL0 && newBalanceState <= BalanceParams.STATE_BALANCE_CAL3)) {
            if (newBalanceSubstate == BalanceParams.BALANCE_WAIT_COVER && balanceState == BalanceParams.STATE_IDLE) {
                App.playSound(BalanceParams.SOUND_NORMAL, "sounds/ru/push_cover.wav");
            } else if ((newBalanceSubstate >= BalanceParams.BALANCE_START && newBalanceSubstate <= BalanceParams.BALANCE_STABLE_SPEED) && (balanceState == BalanceParams.STATE_IDLE || (balanceState == newBalanceState
                    && balanceSubstate == BalanceParams.BALANCE_WAIT_COVER))) {
                App.playSound(BalanceParams.SOUND_NORMAL, "sounds/ru/accel.wav");
            } else if (newBalanceSubstate == BalanceParams.BALANCE_MEASURE && balanceSubstate != BalanceParams.BALANCE_MEASURE) {
                App.playSound(BalanceParams.SOUND_NORMAL, "sounds/ru/measure.wav");
            }
        }

        newBalanceResult = App.getIntParam("result");

        if (newBalanceResult != BalanceParams.RESULT_IDLE && balanceResult == BalanceParams.RESULT_IDLE) {
            if (newBalanceResult == BalanceParams.RESULT_SUCCESS) {
                if (newBalanceState == BalanceParams.STATE_BALANCE) {
                    if (weight1 > App.OVERLOAD_WEIGHT || weight2 > App.OVERLOAD_WEIGHT || weight3 > App.OVERLOAD_WEIGHT) {
                        App.playSound(BalanceParams.SOUND_NORMAL, "sounds/ru/overload.wav");
                    } else if (weight1 != 0 || weight2 != 0 || weight3 != 0) {
                        ArrayList<String> files = new ArrayList<>();
                        if (weight1 != 0 && weight1 <= 100) {
                            files.add("sounds/ru/left_" + String.valueOf(weight1) + ".wav");
                        }
                        if (weight2 != 0 && weight2 <= 100) {
                            files.add("sounds/ru/right_" + String.valueOf(weight2) + ".wav");
                        }
                        if (weight3 != 0 && weight3 <= 100) {
                            files.add("sounds/ru/right_" + String.valueOf(weight3) + ".wav");
                        }
                        String[] filesArr = new String[files.size()];
                        filesArr = files.toArray(filesArr);
                        App.playSound(BalanceParams.SOUND_NORMAL, filesArr);
                        Log.d("DEBUG", String.valueOf("sleva sprava on main screen"));
                    } else {
                        App.playSound(BalanceParams.SOUND_IMPORTANT, "sounds/ru/wheel_is_balanced.wav");
                    }
                }
            } else if (newBalanceState != BalanceParams.STATE_IDLE) {
                App.playSound(BalanceParams.SOUND_NORMAL, "sounds/ru/emergency_stop.wav");
            }
        }

        balanceState = newBalanceState;
        balanceSubstate = newBalanceSubstate;
        balanceResult = newBalanceResult;
    }

    @Subscribe
    public void updateParams(AppEvents action) {
        switch (action) {
            case CONNECTION_LOST:
            case NETWORK_UNAVAILABLE:
                connectionError.setVisibility(View.VISIBLE);
                break;
            case CONNECTION_ESTABLISHED:
                connectionError.setVisibility(View.GONE);
                break;
        }
    }

    private void wheelsDraw() {
        int wheelangleMachine = App.getIntParam("wheelangle") / 2;
        int wheelangleMachine0 = App.getIntParam("wheelangle0") / 2;
        int wheelangleMachine1 = App.getIntParam("wheelangle1") / 2;
        int wheelangleMachine2 = App.getIntParam("wheelangle2") / 2;

        rotateWheel(wheelLeft, wheelangle, wheelangleMachine);
        rotateWheel(wheelRight, wheelangle, wheelangleMachine);

        if (weight1 != 0) {
            rotateBob(leftBob, wheelangle - wheelangle0, wheelangleMachine - wheelangleMachine0);
        }

        if (weight2 != 0) {
            rotateBob(rightBob1, wheelangle - wheelangle1, wheelangleMachine - wheelangleMachine1);
        }

        if (weight3 != 0) {
            rotateBob(rightBob2, wheelangle - wheelangle2, wheelangleMachine - wheelangleMachine2);
        }
    }

    private void wrapperDraw() {
        drawLeft();
        drawRight();
    }

    private void drawLeft() {
        if (weight1 != 0) {
            leftBob.setVisibility(View.VISIBLE);
            if (bob0Allowed) {
                wheelLeftWrapper.setImageResource(R.drawable.left_green);
                leftWeight.setBackground(ContextCompat.getDrawable(App.getContext(), R.drawable.right_item_green));
                leftBob.setImageResource(R.drawable.plummet_on_wheel_green);
            } else {
                wheelLeftWrapper.setImageResource(R.drawable.left_red);
                leftWeight.setBackground(ContextCompat.getDrawable(App.getContext(), R.drawable.right_item_red));
                leftBob.setImageResource(R.drawable.plummet_on_wheel_red);
            }
        } else {
            leftBob.clearAnimation();
            leftBob.setVisibility(View.INVISIBLE);
            wheelLeftWrapper.setImageResource(R.drawable.left_grey);
            leftWeight.setBackground(ContextCompat.getDrawable(App.getContext(), R.drawable.right_item_grey));
        }
    }

    private void drawRight() {
        if (splitMode == 1) {
            if (weight2 != 0 || weight3 != 0) {
                if (weight2 != 0) {
                    rightBob1.setVisibility(View.VISIBLE);
                }
                if (weight3 != 0) {
                    rightBob2.setVisibility(View.VISIBLE);
                }
                if ((bob1Allowed)) {
                    if (weight2 != 0) {
                        rightWeight.setBackground(ContextCompat.getDrawable(App.getContext(), R.drawable.right_item_green));
                        wheelRightWrapper.setImageResource(R.drawable.left_green2);
                    } else {
                        rightWeight.setBackground(ContextCompat.getDrawable(App.getContext(), R.drawable.right_item_red));
                        wheelRightWrapper.setImageResource(R.drawable.middle_red);
                    }
                    rightWeight2.setBackground(ContextCompat.getDrawable(App.getContext(), R.drawable.left_item_red));
                    rightBob1.setImageResource(R.drawable.plummet_on_wheel_green);
                    rightBob2.setImageResource(R.drawable.plummet_on_wheel_red);
                } else if ((bob2Allowed)) {
                    rightWeight.setBackground(ContextCompat.getDrawable(App.getContext(), R.drawable.right_item_red));
                    if (weight3 != 0) {
                        rightWeight2.setBackground(ContextCompat.getDrawable(App.getContext(), R.drawable.left_item_green));
                        wheelRightWrapper.setImageResource(R.drawable.right_green2);
                    } else {
                        rightWeight2.setBackground(ContextCompat.getDrawable(App.getContext(), R.drawable.left_item_red));
                        wheelRightWrapper.setImageResource(R.drawable.middle_red);
                    }
                    rightBob1.setImageResource(R.drawable.plummet_on_wheel_red);
                    rightBob2.setImageResource(R.drawable.plummet_on_wheel_green);
                } else {
                    wheelRightWrapper.setImageResource(R.drawable.middle_red);
                    rightWeight.setBackground(ContextCompat.getDrawable(App.getContext(), R.drawable.right_item_red));
                    rightWeight2.setBackground(ContextCompat.getDrawable(App.getContext(), R.drawable.left_item_red));
                    rightBob1.setImageResource(R.drawable.plummet_on_wheel_red);
                    rightBob2.setImageResource(R.drawable.plummet_on_wheel_red);
                }
            } else {
                rightBob1.clearAnimation();
                rightBob1.setVisibility(View.INVISIBLE);
                rightBob2.clearAnimation();
                rightBob2.setVisibility(View.INVISIBLE);
                wheelRightWrapper.setImageResource(R.drawable.middle_grey);
                rightWeight.setBackground(ContextCompat.getDrawable(App.getContext(), R.drawable.right_item_grey));
                rightWeight2.setBackground(ContextCompat.getDrawable(App.getContext(), R.drawable.left_item_grey));
            }
        } else {
            rightBob2.clearAnimation();
            rightBob2.setVisibility(View.INVISIBLE);
            if (weight2 != 0) {
                rightBob1.setVisibility(View.VISIBLE);
                if ((bob1Allowed)) {
                    wheelRightWrapper.setImageResource(R.drawable.left_green);
                    rightWeight.setBackground(ContextCompat.getDrawable(App.getContext(), R.drawable.right_item_green));
                    rightBob1.setImageResource(R.drawable.plummet_on_wheel_green);
                } else {
                    wheelRightWrapper.setImageResource(R.drawable.left_red);
                    rightWeight.setBackground(ContextCompat.getDrawable(App.getContext(), R.drawable.right_item_red));
                    rightBob1.setImageResource(R.drawable.plummet_on_wheel_red);
                }
            } else {
                rightBob1.clearAnimation();
                rightBob1.setVisibility(View.INVISIBLE);
                wheelRightWrapper.setImageResource(R.drawable.left_grey);
                rightWeight.setBackground(ContextCompat.getDrawable(App.getContext(), R.drawable.right_item_grey));
            }
        }
    }

    private void setDiskImage(int mode) {
        diskImage.setImageResource(diskImages.get(mode));
    }

    private void setBobImage(int mode, int layout) {
        if (mode == BalanceParams.MODE_STAT) {
            bobImage.setImageResource(staticModeImages.get(layout));
        } else {
            bobImage.setImageResource(dynamicModeImages.get(layout));
        }
    }

    private void setSpokeImage(int spoke) {
        diskImage.setImageResource(diskSpokesImages.get(spoke));
    }

    private void rotateWheel(ImageView view, float from, float to) {
        RotateAnimation rAnim = new RotateAnimation(from, to, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rAnim.setFillEnabled(true);
        rAnim.setFillAfter(true);
        rAnim.setInterpolator(new LinearInterpolator());
        rAnim.setDuration(0);
        view.startAnimation(rAnim);
    }

    private void rotateBob(ImageView view, float from, float to) {
        RotateAnimation rAnim;

        FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) view.getLayoutParams();
        rAnim = new RotateAnimation(from, to, view.getWidth() / 2, lp.bottomMargin + (view.getHeight() / 2));

        rAnim.setFillEnabled(true);
        rAnim.setFillAfter(true);
        rAnim.setInterpolator(new LinearInterpolator());
        rAnim.setDuration(0);
        view.startAnimation(rAnim);
    }

    private void bobsDraw() {
        bob1.setVisibility(View.INVISIBLE);
        bob2.setVisibility(View.INVISIBLE);
        bob3.setVisibility(View.INVISIBLE);
        bob4.setVisibility(View.INVISIBLE);
        bob5.setVisibility(View.INVISIBLE);

        if (App.balanceState == BalanceParams.STATE_IDLE ||
                (App.balanceState == BalanceParams.STATE_BALANCE || (App.balanceState >= BalanceParams.STATE_BALANCE_CAL0 && App.balanceState <= BalanceParams.STATE_BALANCE_CAL3)) &&
                        (App.balanceSubstate == BalanceParams.BALANCE_IDLE || App.balanceSubstate >= BalanceParams.BALANCE_AUTO_ROTATION)) {
            showWeightControls();
            moveWeightControlsDown();
            afterRulerSetMode();
            if (weight1 != 0) {
                switch (layout) {
                    case BalanceParams.LAYOUT_1_3:
                    case BalanceParams.LAYOUT_1_4:
                    case BalanceParams.LAYOUT_1_5:
                        if ((bob0Allowed)) {
                            showBob(bob1, BalanceParams.BOB_GREEN);
                        } else {
                            showBob(bob1, BalanceParams.BOB_RED);
                        }
                        break;
                    case BalanceParams.LAYOUT_2_3:
                    case BalanceParams.LAYOUT_2_4:
                    case BalanceParams.LAYOUT_2_5:
                        if ((bob0Allowed)) {
                            showBob(bob2, BalanceParams.BOB_GREEN);
                        } else {
                            showBob(bob2, BalanceParams.BOB_RED);
                        }
                        break;
                }
            }
            if (weight2 != 0 || weight3 != 0) {
                switch (layout) {
                    case BalanceParams.LAYOUT_1_3:
                    case BalanceParams.LAYOUT_2_3:
                        if (((bob1Allowed) && weight2 != 0) || ((bob2Allowed) && weight3 != 0)) {
                            showBob(bob3, BalanceParams.BOB_GREEN);
                        } else {
                            showBob(bob3, BalanceParams.BOB_RED);
                        }
                        break;
                    case BalanceParams.LAYOUT_1_4:
                    case BalanceParams.LAYOUT_2_4:
                        if (((bob1Allowed) && weight2 != 0) || ((bob2Allowed) && weight3 != 0)) {
                            showBob(bob4, BalanceParams.BOB_GREEN);
                        } else {
                            showBob(bob4, BalanceParams.BOB_RED);
                        }
                        break;
                    case BalanceParams.LAYOUT_1_5:
                    case BalanceParams.LAYOUT_2_5:
                        if (((bob1Allowed) && weight2 != 0) || ((bob2Allowed) && weight3 != 0)) {
                            showBob(bob5, BalanceParams.BOB_GREEN);
                        } else {
                            showBob(bob5, BalanceParams.BOB_RED);
                        }
                        break;
                }
            }
        } else if (App.balanceState == BalanceParams.STATE_RULER) {
            hideWeightControls();
            int dist = App.getIntParam("weightdist");
            if (App.balanceSubstate == BalanceParams.RULER_MEASURE || App.balanceSubstate == BalanceParams.RULER_DONTSHOW) {
                if (mode != BalanceParams.MODE_STAT) {
                    switch (layout) {
                        case BalanceParams.LAYOUT_1_5:
                        case BalanceParams.LAYOUT_1_4:
                        case BalanceParams.LAYOUT_1_3:
                            setRulerPosition1();
                            break;
                        case BalanceParams.LAYOUT_2_5:
                        case BalanceParams.LAYOUT_2_3:
                        case BalanceParams.LAYOUT_2_4:
                            setRulerPosition2();
                            break;
                    }
                } else {
                    switch (layout) {
                        case BalanceParams.LAYOUT_1:
                            setRulerPosition1();
                            break;
                        case BalanceParams.LAYOUT_2:
                            setRulerPosition2();
                            break;
                        case BalanceParams.LAYOUT_3:
                            setRulerPosition3();
                            break;
                    }
                }
                offset.setText(App.getParamString("rofs"));
                drawBobsInMeasureMode();
            } else if (App.balanceSubstate == BalanceParams.RULER_MEASURE_L) {
                setRulerPosition3();
                width.setText(App.getParamString("rstick"));
                drawBobsInMeasureMode();
            } else if (App.balanceSubstate == BalanceParams.RULER_SHOW_L1) {
                beforeRulerSetMode();
                drawBobsInSetMode();
                showWeightControls(true);
                moveWeightControlsUp();
                setRulerPosition2();
                rulerDirectionValue.setText(String.valueOf(Math.abs(dist)));
                setRulerImageDirection(rulerSetDirection, dist);
            } else if (App.balanceSubstate == BalanceParams.RULER_SHOW_L2 || App.balanceSubstate == BalanceParams.RULER_SHOW_L3) {
                beforeRulerSetMode();
                drawBobsInSetMode();
                showWeightControls(true);
                moveWeightControlsUp();
                setRulerPosition3();
                rulerDirectionValue.setText(String.valueOf(Math.abs(dist)));
                setRulerImageDirection(rulerSetDirection, dist);
            }

            if (App.balanceSubstate == BalanceParams.RULER_WAIT) {
                ruler.setVisibility(View.GONE);
            }

            if (!diam.getText().toString().equals(App.getParamString("diam"))) {
                if (App.diamValue > 999) {
                    diam.setText(String.valueOf(999));
                } else {
                    diam.setText(App.getParamString("diam"));
                }
            }
        }
    }

    private void drawBobsInMeasureMode() {
        if ((mode != BalanceParams.MODE_STAT && (layout == BalanceParams.LAYOUT_1_3 || layout == BalanceParams.LAYOUT_1_4 || layout == BalanceParams.LAYOUT_1_5))
                || (mode == BalanceParams.MODE_STAT && layout == BalanceParams.LAYOUT_1)) {
            showBob(bob1, BalanceParams.BOB_YELLOW);
        }
        if ((mode != BalanceParams.MODE_STAT && (layout == BalanceParams.LAYOUT_2_3 || layout == BalanceParams.LAYOUT_2_4 || layout == BalanceParams.LAYOUT_2_5)) ||
                (mode == BalanceParams.MODE_STAT && layout == BalanceParams.LAYOUT_2)) {
            showBob(bob2, BalanceParams.BOB_YELLOW);
        }
        if ((mode != BalanceParams.MODE_STAT && (layout == BalanceParams.LAYOUT_1_3 || layout == BalanceParams.LAYOUT_2_3)) || (mode == BalanceParams.MODE_STAT && layout == BalanceParams.LAYOUT_3)) {
            showBob(bob3, BalanceParams.BOB_YELLOW);
        }
        if ((mode != BalanceParams.MODE_STAT && (layout == BalanceParams.LAYOUT_1_4 || layout == BalanceParams.LAYOUT_2_4)) || (mode == BalanceParams.MODE_STAT && layout == BalanceParams.LAYOUT_4)) {
            showBob(bob4, BalanceParams.BOB_YELLOW);
        }
        if ((mode != BalanceParams.MODE_STAT && (layout == BalanceParams.LAYOUT_1_5 || layout == BalanceParams.LAYOUT_2_5)) || (mode == BalanceParams.MODE_STAT && layout == BalanceParams.LAYOUT_5)) {
            showBob(bob5, BalanceParams.BOB_YELLOW);
        }
    }

    private void drawBobsInSetMode() {
        if (weight1 != 0) {
            if ((mode != BalanceParams.MODE_STAT && (layout == BalanceParams.LAYOUT_1_3 || layout == BalanceParams.LAYOUT_1_4 || layout == BalanceParams.LAYOUT_1_5))
                    || (mode == BalanceParams.MODE_STAT && layout == BalanceParams.LAYOUT_1)) {
                if ((bob0Allowed)) {
                    showBob(bob1, BalanceParams.BOB_GREEN);
                } else {
                    showBob(bob1, BalanceParams.BOB_RED);
                }
            }
            if ((mode != BalanceParams.MODE_STAT && (layout == BalanceParams.LAYOUT_2_3 || layout == BalanceParams.LAYOUT_2_4 || layout == BalanceParams.LAYOUT_2_5)) ||
                    (mode == BalanceParams.MODE_STAT && layout == BalanceParams.LAYOUT_2)) {
                if ((bob0Allowed)) {
                    showBob(bob2, BalanceParams.BOB_GREEN);
                } else {
                    showBob(bob2, BalanceParams.BOB_RED);
                }
            }
        }

        if (weight2 != 0 || weight3 != 0) {
            if ((mode != BalanceParams.MODE_STAT && (layout == BalanceParams.LAYOUT_1_3 || layout == BalanceParams.LAYOUT_2_3)) || (mode == BalanceParams.MODE_STAT && layout == BalanceParams.LAYOUT_3)) {
                if ((bob1Allowed) || (bob2Allowed)) {
                    showBob(bob3, BalanceParams.BOB_GREEN);
                } else {
                    showBob(bob3, BalanceParams.BOB_RED);
                }
            }
            if ((mode != BalanceParams.MODE_STAT && (layout == BalanceParams.LAYOUT_1_4 || layout == BalanceParams.LAYOUT_2_4)) || (mode == BalanceParams.MODE_STAT && layout == BalanceParams.LAYOUT_4)) {
                if ((bob1Allowed) || (bob2Allowed)) {
                    showBob(bob4, BalanceParams.BOB_GREEN);
                } else {
                    showBob(bob4, BalanceParams.BOB_RED);
                }
            }
            if ((mode != BalanceParams.MODE_STAT && (layout == BalanceParams.LAYOUT_1_5 || layout == BalanceParams.LAYOUT_2_5)) || (mode == BalanceParams.MODE_STAT && layout == BalanceParams.LAYOUT_5)) {
                if ((bob1Allowed) || (bob2Allowed)) {
                    showBob(bob5, BalanceParams.BOB_GREEN);
                } else {
                    showBob(bob5, BalanceParams.BOB_RED);
                }
            }
        }
    }

    private void moveWeightControlsUp() {
        controlUp(leftWeightContainer);
        controlUp(rightWeightContainer);
    }

    private void moveWeightControlsDown() {
        controlDown(leftWeightContainer);
        controlDown(rightWeightContainer);
    }

    private void controlUp(View container) {
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) container.getLayoutParams();
        layoutParams.removeRule(RelativeLayout.CENTER_IN_PARENT);
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
        container.setLayoutParams(layoutParams);
    }

    private void controlDown(View container) {
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) container.getLayoutParams();
        layoutParams.removeRule(RelativeLayout.ALIGN_PARENT_TOP);
        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
        container.setLayoutParams(layoutParams);
    }

    private void beforeRulerSetMode() {
        offsetContainer.setVisibility(View.GONE);
        diamContainer.setVisibility(View.GONE);
        widthContainer.setVisibility(View.GONE);
        rulerDirectionContainer.setVisibility(View.VISIBLE);
    }

    private void afterRulerSetMode() {
        offsetContainer.setVisibility(View.VISIBLE);
        diamContainer.setVisibility(View.VISIBLE);
        widthContainer.setVisibility(View.VISIBLE);
        rulerDirectionContainer.setVisibility(View.GONE);
    }

    private void setRulerImageDirection(ImageView rulerDirection, int dist) {
        int direction = 0;
        if (dist > 0) {
            direction = BalanceParams.DIRECTION_RIGHT;
        } else if (dist < 0) {
            direction = BalanceParams.DIRECTION_LEFT;
        } else if (dist == 0) {
            direction = BalanceParams.DIRECTION_UP;
        }

        switch (direction) {
            case BalanceParams.DIRECTION_LEFT:
                rulerDirection.setImageResource(R.drawable.ruler_set_arrow_left);
                break;
            case BalanceParams.DIRECTION_RIGHT:
                rulerDirection.setImageResource(R.drawable.ruler_set_arrow_right);
                break;
            case BalanceParams.DIRECTION_UP:
                rulerDirection.setImageResource(R.drawable.ruler_set_arrow_up);
                break;
        }
    }

    private void showBob(ImageView bob, int color) {
        int bobId = bob.getId();
        switch (bobId) {
            case R.id.bob_1:
                switch (color) {
                    case BalanceParams.BOB_YELLOW:
                        bob.setImageResource(R.drawable.bob_1);
                        break;
                    case BalanceParams.BOB_RED:
                        bob.setImageResource(R.drawable.bob_red_1);
                        break;
                    case BalanceParams.BOB_GREEN:
                        bob.setImageResource(R.drawable.bob_green_1);
                        break;
                    default:
                        bob.setImageResource(R.drawable.bob_1);
                        break;
                }
                break;
            case R.id.bob_2:
            case R.id.bob_3:
            case R.id.bob_4:
                switch (color) {
                    case BalanceParams.BOB_YELLOW:
                        bob.setImageResource(R.drawable.bob_2_3_4);
                        break;
                    case BalanceParams.BOB_RED:
                        bob.setImageResource(R.drawable.bob_red_2_3_4);
                        break;
                    case BalanceParams.BOB_GREEN:
                        bob.setImageResource(R.drawable.bob_green_2_3_4);
                        break;
                    default:
                        bob.setImageResource(R.drawable.bob_2_3_4);
                        break;
                }
                break;
            case R.id.bob_5:
                switch (color) {
                    case BalanceParams.BOB_YELLOW:
                        bob.setImageResource(R.drawable.bob_5);
                        break;
                    case BalanceParams.BOB_RED:
                        bob.setImageResource(R.drawable.bob_red_5);
                        break;
                    case BalanceParams.BOB_GREEN:
                        bob.setImageResource(R.drawable.bob_green_5);
                        break;
                    default:
                        bob.setImageResource(R.drawable.bob_5);
                        break;
                }
                break;
            default:
                break;
        }
        bob.setVisibility(View.VISIBLE);
    }

    private void hideWeightControls() {
        leftWeightContainer.setVisibility(View.GONE);
        rightWeightContainer.setVisibility(View.GONE);
        ruler.setVisibility(View.VISIBLE);
    }

    private void showWeightControls(boolean rulerFlag) {
        if (rulerFlag) {
            ruler.setVisibility(View.VISIBLE);
            leftWeightContainer.setVisibility(View.VISIBLE);
            rightWeightContainer.setVisibility(View.VISIBLE);
        } else {
            ruler.setVisibility(View.GONE);
            leftWeightContainer.setVisibility(View.VISIBLE);
            rightWeightContainer.setVisibility(View.VISIBLE);
        }
    }

    private void showWeightControls() {
        ruler.setVisibility(View.GONE);
        leftWeightContainer.setVisibility(View.VISIBLE);
        rightWeightContainer.setVisibility(View.VISIBLE);
    }

    private void setRulerPosition1() {
        /*if (App.getIntParam("r0") <= 56) { //TODO: понять зачем нужно это условие
            setRulerPositionDefault();
        } else {
            lp.leftMargin = App.dpToPx((int) getResources().getDimension(R.dimen.ruler_position_1_left));
            lp.topMargin = App.dpToPx((int) getResources().getDimension(R.dimen.ruler_position_1_top));
            rulerMiddle.getLayoutParams().width = App.dpToPx((int) getResources().getDimension(R.dimen.ruler_position_1_width));
            rulerMiddle.requestLayout();
        }*/

        lp.leftMargin = App.dpToPx((int) getResources().getDimension(R.dimen.ruler_position_1_left));
        lp.topMargin = App.dpToPx((int) getResources().getDimension(R.dimen.ruler_position_1_top));
        rulerMiddle.getLayoutParams().width = App.dpToPx((int) getResources().getDimension(R.dimen.ruler_position_1_width));
        rulerMiddle.requestLayout();
    }

    private void setRulerPosition2() {
        lp.leftMargin = App.dpToPx((int) getResources().getDimension(R.dimen.ruler_position_1_left));
        lp.topMargin = App.dpToPx((int) getResources().getDimension(R.dimen.ruler_position_2_top));
        rulerMiddle.getLayoutParams().width = App.dpToPx((int) getResources().getDimension(R.dimen.ruler_position_2_width));
        rulerMiddle.requestLayout();
    }

    private void setRulerPosition3() {
        lp.leftMargin = App.dpToPx((int) getResources().getDimension(R.dimen.ruler_position_1_left));
        lp.topMargin = App.dpToPx((int) getResources().getDimension(R.dimen.ruler_position_3_top));
        rulerMiddle.getLayoutParams().width = App.dpToPx((int) getResources().getDimension(R.dimen.ruler_position_3_width));
        rulerMiddle.requestLayout();
    }

    private void setRulerPositionDefault() {
        rulerMiddle.getLayoutParams().width = App.dpToPx(50);
        rulerMiddle.requestLayout();
        lp.leftMargin = App.dpToPx(0);
        lp.topMargin = App.dpToPx(120);
    }

    private void paramsBlinking() {
        newBalanceErrors0 = App.getIntParam("errors0");
        newBalanceErrors1 = App.getIntParam("errors1");
        newBalanceErrors2 = App.getIntParam("errors2");

        boolean widthNotEntered = Math.floor(newBalanceErrors0 / 64) % 2 != 0 && Math.floor(balanceErrors0 / 64) % 2 == 0;
        boolean diamNotEntered = Math.floor(newBalanceErrors0 / 16) % 2 != 0 && Math.floor(balanceErrors0 / 16) % 2 == 0;
        boolean ofsNotEntered = Math.floor(newBalanceErrors0 / 32) % 2 != 0 && Math.floor(balanceErrors0 / 32) % 2 == 0;

        boolean widthNotEnteredFlag = Math.floor(newBalanceErrors0 / 64) % 2 != 0;
        boolean diamNotEnteredFlag = Math.floor(newBalanceErrors0 / 16) % 2 != 0;
        boolean ofsNotEnteredFlag = Math.floor(newBalanceErrors0 / 32) % 2 != 0;

        if (widthNotEnteredFlag) {
            if (!widthBlinkFlag) {
                widthBlinkFlag = true;
                startBlinking(width);
            }
        } else {
            widthBlinkFlag = false;
            stopBlinking(width);
        }

        if (diamNotEnteredFlag) {
            if (!diamBlinkFlag) {
                diamBlinkFlag = true;
                startBlinking(diam);
            }
        } else {
            diamBlinkFlag = false;
            stopBlinking(diam);
        }

        if (ofsNotEnteredFlag) {
            if (!offsetBlinkFlag) {
                offsetBlinkFlag = true;
                startBlinking(offset);
            }
        } else {
            offsetBlinkFlag = false;
            stopBlinking(offset);
        }

        if (widthNotEntered && !diamNotEntered && !ofsNotEntered) {
            App.playSound(BalanceParams.SOUND_IMPORTANT, "sounds/ru/width_not_entered.wav");
        }
        else if (diamNotEntered && !widthNotEntered && !ofsNotEntered) {
            App.playSound(BalanceParams.SOUND_IMPORTANT, "sounds/ru/diam_not_entered.wav");
        }
        else if (ofsNotEntered && !widthNotEntered && !diamNotEntered) {
            App.playSound(BalanceParams.SOUND_IMPORTANT, "sounds/ru/ofs_not_entered.wav");
        }
        else if (widthNotEntered || diamNotEntered || ofsNotEntered) {
            App.playSound(BalanceParams.SOUND_IMPORTANT, "sounds/ru/sizes_not_entered.wav");
        }

        balanceErrors0 = newBalanceErrors0;
        balanceErrors1 = newBalanceErrors1;
        balanceErrors2 = newBalanceErrors2;
    }

    private void startBlinking(View v) {
        v.clearAnimation();
        v.setBackgroundResource(R.drawable.animation_red2);
        blinking(v, true);
    }

    private void stopBlinking(View v) {
        v.setBackgroundResource(R.drawable.bottom_panel_button_default);
        blinking(v, false);
    }

    private void blinking(View v, boolean flag) {
        if (flag) {
            Animation anim = new AlphaAnimation(0.0f, 1.0f);
            anim.setDuration(200); //You can manage the blinking time with this parameter
            anim.setStartOffset(20);
            anim.setRepeatMode(Animation.REVERSE);
            anim.setRepeatCount(Animation.INFINITE);
            v.startAnimation(anim);
        } else {
            v.clearAnimation();
        }
    }

    private void showBalanceProgress() {
        progressDialog.setRetainInstance(true);
        progressDialog.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        progressDialog.show(fragmentManager, "progressDialog");
    }

    public void hideBalanceProgress() {
        progressDialog.dismiss();
    }

    private void showCoverMessage() {
        coverMessage.setRetainInstance(true);
        coverMessage.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        coverMessage.show(fragmentManager, "coverMessage");
    }

    public void hideCoverMessage() {
        coverMessage.dismiss();
    }

    private void showWheelMessage() {
        wheelMessage.setRetainInstance(true);
        wheelMessage.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        wheelMessage.show(fragmentManager, "wheelMessage");
    }

    public void hideWheelMessage() {
        wheelMessage.dismiss();
    }

    private int quadrantNumber(int angle) {
        if (angle >= 0 && angle <= 90) {
            return 1;
        }
        if (angle >= 90 && angle <= 180) {
            return 2;
        }
        if (angle >= 180 && angle <= 270) {
            return 3;
        }
        if (angle >= 270 && angle <= 360) {
            return 4;
        }
        return 0;
    }

    private boolean isBobAllowed(int leftEdge, int rightEdge) {
        if (rightEdge > leftEdge) {
            return (wheelangle >= leftEdge && wheelangle <= rightEdge);
        } else {
            if (quadrantNumber(wheelangle) == quadrantNumber(rightEdge)) {
                return (wheelangle + 360 >= leftEdge && wheelangle <= rightEdge);
            } else {
                return (wheelangle >= leftEdge && wheelangle <= rightEdge + 360);
            }
        }
    }

    private void drawWheelsArrows() {
        if (
                weight0TopArrow != null &&
                        weight0BottomArrow != null &&
                        weight1TopArrow != null &&
                        weight1BottomArrow != null &&
                        weight2TopArrow != null &&
                        weight2BottomArrow != null
                ) {

            weight0TopArrow.setEdges(leftEdge0, rightEdge0);
            weight0BottomArrow.setEdges(leftEdge0, rightEdge0);
            weight1TopArrow.setEdges(leftEdge1, rightEdge1);
            weight1BottomArrow.setEdges(leftEdge1, rightEdge1);
            weight2TopArrow.setEdges(leftEdge2, rightEdge2);
            weight2BottomArrow.setEdges(leftEdge2, rightEdge2);

            if (weight1 != 0) {
                weight0TopArrow.setRed();
                weight0BottomArrow.setRed();
                if (bob0Allowed) {
                    weight0TopArrow.setGreen();
                    weight0BottomArrow.setGreen();
                } else {
                    weight0TopArrow.setGreenSize(wheelangle);
                    weight0BottomArrow.setGreenSize(wheelangle);
                }
            } else {
                weight0TopArrow.setGrey();
                weight0BottomArrow.setGrey();
            }

            if (weight2 != 0) {
                weight1TopArrow.setRed();
                weight1BottomArrow.setRed();
                if (bob1Allowed) {
                    weight1TopArrow.setGreen();
                    weight1BottomArrow.setGreen();
                } else {
                    weight1TopArrow.setGreenSize(wheelangle);
                    weight1BottomArrow.setGreenSize(wheelangle);
                }
            } else {
                weight1TopArrow.setGrey();
                weight1BottomArrow.setGrey();
            }

            if (weight3 != 0) {
                weight2TopArrow.setRed();
                weight2BottomArrow.setRed();
                if (bob2Allowed) {
                    weight2TopArrow.setGreen();
                    weight2BottomArrow.setGreen();
                } else {
                    weight2TopArrow.setGreenSize(wheelangle);
                    weight2BottomArrow.setGreenSize(wheelangle);
                }
            } else {
                weight2TopArrow.setGrey();
                weight2BottomArrow.setGrey();
            }
        }
    }

    private void triggerSplitMode() {
        if (splitMode == 1) {
            rightWeight2.setVisibility(View.VISIBLE);
            if (weight2TopArrow != null && weight2BottomArrow != null) {
                weight2TopArrow.setVisibility(View.VISIBLE);
                weight2BottomArrow.setVisibility(View.VISIBLE);
            }
        } else {
            rightWeight2.setVisibility(View.INVISIBLE);
            if (weight2TopArrow != null && weight2BottomArrow != null) {
                weight2TopArrow.setVisibility(View.INVISIBLE);
                weight2BottomArrow.setVisibility(View.INVISIBLE);
            }
        }
    }

    private void debugInfo() {
        Log.d("DEBUG", String.valueOf("-----------------------------"));
        Log.d("DEBUG", "leftEdge0 = " + String.valueOf(leftEdge0));
        Log.d("DEBUG", "rightEdge0 = " + String.valueOf(rightEdge0));
        Log.d("DEBUG", "leftEdge1 = " + String.valueOf(leftEdge1));
        Log.d("DEBUG", "rightEdge1 = " + String.valueOf(rightEdge1));
        Log.d("DEBUG", "leftEdge2 = " + String.valueOf(leftEdge2));
        Log.d("DEBUG", "rightEdge2 = " + String.valueOf(rightEdge2));
        Log.d("DEBUG", "wheelAngle = " + String.valueOf(wheelangle));
        Log.d("DEBUG", String.valueOf("-----------------------------"));
    }
}