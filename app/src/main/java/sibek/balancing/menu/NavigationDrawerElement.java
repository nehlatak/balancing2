package sibek.balancing.menu;

import sibek.balancing.App;

public class NavigationDrawerElement {
    public int icon;
    public String title;

    public NavigationDrawerElement(String title) {
        this.title = App.getStringFromResources(title);
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
