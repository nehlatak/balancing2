package sibek.balancing.menu;

import java.util.ArrayList;

import sibek.balancing.R;

public class NavigationMenu {
    public ArrayList<NavigationDrawerElement> menu = new ArrayList<>();

    public NavigationMenu() {
        NavigationDrawerElement element = new NavigationDrawerElement("main_screen");
        element.setIcon(R.drawable.main_icon_balansirovka);
        menu.add(element);

        NavigationDrawerElement element1 = new NavigationDrawerElement("user_1");
        element1.setIcon(R.drawable.main_icon_operator_1);
        menu.add(element1);

        NavigationDrawerElement element2 = new NavigationDrawerElement("user_2");
        element2.setIcon(R.drawable.main_icon_operator_2);
        menu.add(element2);

        NavigationDrawerElement element3 = new NavigationDrawerElement("calibration");
        element3.setIcon(R.drawable.main_icon_kalibrovka);
        menu.add(element3);

        NavigationDrawerElement element4 = new NavigationDrawerElement("diagnostics");
        element4.setIcon(R.drawable.main_icon_diagnostika);
        menu.add(element4);

        NavigationDrawerElement element5 = new NavigationDrawerElement("options");
        element5.setIcon(R.drawable.main_icon_nastroyka);
        menu.add(element5);
    }
}