package sibek.balancing.menu;

import android.support.v4.util.Pair;

import java.util.ArrayList;

import sibek.balancing.App;
import sibek.balancing.fragments.menu.OnMenuClick;
import sibek.balancing.tools.TypesParams;

public class MenuItem {
    public String title;
    public String description;
    public int position;
    public OnMenuClick method;
    public boolean password = false;
    public ArrayList<MenuItem> subItems = new ArrayList<>();
    private Pair<TypesParams, Object> value;
    private String machineParam;
    private int icon;

    public MenuItem() {
    }

    public MenuItem(int title, int description, String machineParam, Pair<TypesParams, Object> value) {
        this.title = App.getStringFromResources(title);
        this.description = App.getStringFromResources(description);
        this.machineParam = machineParam;
        this.value = value;
    }

    public MenuItem(int title, int description, String machineParam, Pair<TypesParams, Object> value, OnMenuClick method) {
        this.title = App.getStringFromResources(title);
        this.description = App.getStringFromResources(description);
        this.machineParam = machineParam;
        this.value = value;
        this.method = method;
    }

    public MenuItem(int title, int description, String machineParam, OnMenuClick method) {
        this.title = App.getStringFromResources(title);
        this.description = App.getStringFromResources(description);
        this.machineParam = machineParam;
        this.method = method;
    }

    public MenuItem(int title, int description) {
        this.title = App.getStringFromResources(title);
        this.description = App.getStringFromResources(description);
    }

    public MenuItem(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public MenuItem(int title, int description, OnMenuClick method) {
        this.title = App.getStringFromResources(title);
        this.description = App.getStringFromResources(description);
        this.method = method;
    }

    public MenuItem(int title, int description, String machineParam) {
        this.title = App.getStringFromResources(title);
        this.description = App.getStringFromResources(description);
        this.machineParam = machineParam;
    }

    public String getMachineParam() {
        return machineParam;
    }

    public ArrayList<MenuItem> getSubItems() {
        return subItems;
    }

    public Pair<TypesParams, Object> getValue() {
        return value;
    }

    public int getIcon() {
        return icon;
    }

    public void setValue(Pair<TypesParams, Object> value) {
        this.value = value;
    }

    public void setSubItems(ArrayList<MenuItem> subItems) {
        this.subItems = subItems;
    }

    public MenuItem setIcon(int icon) {
        this.icon = icon;
        return this;
    }

    public MenuItem setOnClickListener(OnMenuClick method) {
        this.method = method;
        return this;
    }

    public MenuItem setDescription(String description) {
        this.description = description;
        return this;
    }

    public MenuItem setDescription(int descriptionId) {
        this.description = App.getStringFromResources(descriptionId);;
        return this;
    }

    public MenuItem setMachineParam(String machineParam) {
        this.machineParam = machineParam;
        return this;
    }

    public MenuItem setMethod(OnMenuClick method) {
        this.method = method;
        return this;
    }

    public MenuItem setPosition(int position) {
        this.position = position;
        return this;
    }

    public MenuItem setPassword(boolean password) {
        this.password = password;
        return this;
    }

    public MenuItem setTitle(String title) {
        this.title = title;
        return this;
    }

    public MenuItem setTitle(int titleId) {
        this.title = App.getStringFromResources(titleId);;
        return this;
    }

    public void click() {
        if (method != null) {
            method.onClick();
        }
    }
}