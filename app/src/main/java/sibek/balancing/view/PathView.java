package sibek.balancing.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import java.text.ParseException;

import sibek.balancing.R;
import sibek.balancing.tools.SvgPathParser;

public class PathView extends View {
    private Paint figurePaint;
    private Paint rectPaint;
    private Path figurePath;
    private Context context;
    private Rect rect;
    private SvgPathParser svgParser = new SvgPathParser();
    private int greenSize = 0;
    private Matrix scaleMatrix = new Matrix();
    private RectF rectF = new RectF();
    private String svgString;
    private int orientation;
    private int width;
    private int height;

    private int leftEdge = 0;
    private int rightEdge = 0;

    public PathView(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public PathView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViewParams(attrs);
        this.context = context;
        init();
    }

    public PathView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initViewParams(attrs);
        this.context = context;
        init();
    }

    private void initViewParams(AttributeSet attrs) {
        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.PathView);
        svgString = a.getString(R.styleable.PathView_view_svg);
        orientation = a.getInt(R.styleable.PathView_orientation, 0);
        a.recycle();
    }

    private void init() {
        figurePaint = new Paint();
        figurePaint.setStyle(Paint.Style.FILL);
        figurePaint.setAntiAlias(true);
        figurePaint.setColor(Color.parseColor("#b3b3b3"));

        rectPaint = new Paint();
        rectPaint.setStyle(Paint.Style.FILL);
        rectPaint.setColor(Color.parseColor("#00923d"));

        rect = new Rect(0, 0, 0, 0);

        try {
            figurePath = svgParser.parsePath(svgString);
        } catch (ParseException e) {
            Log.d("DEBUG", String.valueOf("Parse error!"));
            figurePath = new Path();
            figurePath.lineTo(10, 0);
            figurePath.lineTo(10, 10);
            figurePath.lineTo(0, 10);
            figurePath.close();
        }

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2
                && Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            setLayerType(LAYER_TYPE_SOFTWARE, null);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        figurePath.computeBounds(rectF, true);
        scaleMatrix.setScale(width / rectF.width(), height / rectF.height(), rectF.centerX(), rectF.centerY());
        figurePath.transform(scaleMatrix);
        figurePath.computeBounds(rectF, true);
        figurePath.offset((width / 2) - rectF.centerX(), (height / 2) - rectF.centerY());
        canvas.clipPath(figurePath);
        canvas.drawPath(figurePath, figurePaint);

        if (orientation == 0) {
            rect.set(0, 0, width, greenSize);
        } else {
            rect.set(0, height - greenSize, width, height);
        }
        canvas.drawRect(rect, rectPaint);
    }

    public void setGreen() {
        greenSize = height;
        this.invalidate();
    }

    public void setGrey() {
        figurePaint.setColor(Color.parseColor("#b3b3b3"));
        greenSize = 0;
        this.invalidate();
    }

    public void setRed() {
        greenSize = 0;
        figurePaint.setColor(Color.parseColor("#ff0000"));
        this.invalidate();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        width = MeasureSpec.getSize(widthMeasureSpec);
        height = MeasureSpec.getSize(heightMeasureSpec);
    }

    public void setGreenSize(int angle) {
        /*<enum name="upDown" value="0" />
        <enum name="downUp" value="1" />*/
        if (orientation == 1) {
            if (leftEdge - 180 >= 0) {
                if (angle >= leftEdge - 180 && angle <= leftEdge) {
                    int localAngle = angle;
                    int localLeftEdge = leftEdge;
                    int localTransparentLeftEdge = leftEdge - 180;
                    while (localTransparentLeftEdge > 0) {
                        localTransparentLeftEdge--;
                        localLeftEdge--;
                        localAngle--;
                    }

                    int percent = localAngle * 100 / localLeftEdge;
                    int resultSize = (percent * (height)) / 100;

                    if (resultSize > 0) {
                        greenSize = resultSize;
                        this.invalidate();
                    }
                }
            } else {
                if (angle <= rightEdge || angle >= rightEdge + 180) {
                    int localAngle = angle;
                    int localRightEdge = rightEdge;
                    int localTransparentRightEdge = rightEdge + 180;
                    while (localTransparentRightEdge < 360) {
                        localRightEdge++;
                        localTransparentRightEdge++;
                        localAngle++;
                    }
                    if (localAngle >= 360) {
                        localAngle -= 360;
                    }

                    int percent = localAngle * 100 / localRightEdge;
//                    percent = 100 - percent;
                    int resultSize = (percent * (height)) / 100;

                    if (resultSize > 0) {
                        greenSize = resultSize;
                        this.invalidate();
                    }
                }
            }
        } else {
            if (rightEdge - 180 >= 0) {
                if (angle <= rightEdge - 180 || angle >= rightEdge) {
                    int localAngle = angle;
                    int localRightEdge = rightEdge;
                    int localTransparentRightEdge = rightEdge - 180;
                    while (localRightEdge < 360) {
                        localRightEdge++;
                        localTransparentRightEdge++;
                        localAngle++;
                    }
                    if (localAngle >= 360) {
                        localAngle -= 360;
                    }

                    int percent = localAngle * 100 / localTransparentRightEdge;
                    percent = 100 - percent;
                    int resultSize = (percent * (height)) / 100;

                    if (resultSize > 0) {
                        greenSize = resultSize;
                        this.invalidate();
                    }
                }
            } else {
                if (angle >= leftEdge && angle <= leftEdge + 180) {
                    int localAngle = angle;
                    int localLeftEdge = leftEdge;
                    int localTransparentLeftEdge = leftEdge + 180;
                    while (localLeftEdge > 0) {
                        localTransparentLeftEdge--;
                        localLeftEdge--;
                        localAngle--;
                    }

                    int percent = localAngle * 100 / localTransparentLeftEdge;
                    percent = 100 - percent;
                    int resultSize = (percent * (height)) / 100;

                    if (resultSize > 0) {
                        greenSize = resultSize;
                        this.invalidate();
                    }
                }
            }
        }
    }

    public void setEdges(int leftEdge, int rightEdge) {
        this.leftEdge = leftEdge;
        this.rightEdge = rightEdge;
    }
}