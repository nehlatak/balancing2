package sibek.balancing.view;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.SurfaceHolder;

import java.util.Random;

import sibek.balancing.App;
import sibek.balancing.oscilloscope.Oscilloscope;
import sibek.balancing.oscilloscope.graphic.GLRenderer;
import sibek.balancing.oscilloscope.pointsProvider;
import sibek.balancing.tools.CommonUtils;

public class CustomGLSurfaceView extends GLSurfaceView {
    private final GLRenderer mRenderer;
    private ScaleGestureDetector SGD;
    private final float TOUCH_SCALE_FACTOR = 180.0f / 320;
    private float mPreviousX;
    private float mPreviousY;
    private static final int NONE = 0;
    private static final int DRAG = 1;
    private static final int ZOOM = 2;
    private int mode = NONE;
    public volatile boolean testFlag = true;
    private Thread testPointsThread;

    public CustomGLSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);

        // Create an OpenGL ES 2.0 context.
        setEGLContextClientVersion(2);

        // Set the Renderer for drawing on the GLSurfaceView
        mRenderer = new GLRenderer(this);
        setRenderer(mRenderer);

        // Render the view only when there is a change in the drawing data
//        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY); //рендерим при вызове requestRender();
        setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY); //рендерим в бесконечном цикле

        SGD = new ScaleGestureDetector(App.getContext(), new ScaleListener());
    }

    public GLRenderer getRenderer() {
        return mRenderer;
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        super.surfaceCreated(holder);
        mRenderer.setWidthHeight(getWidth(), getHeight());

//        startTestThread();
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        SGD.onTouchEvent(e);

        float x = e.getX();
        float y = e.getY();

        switch (e.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                mode = DRAG;
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP:
                mode = NONE;
                break;
            case MotionEvent.ACTION_MOVE:
                if (mode == DRAG) {
                    float dx = x - mPreviousX;
                    float dy = y - mPreviousY;
                    mRenderer.setTranslateFactor(dx, dy);
//                    requestRender();
                }
                break;
        }

        mPreviousX = x;
        mPreviousY = y;
        return true;
    }

    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        private float scaleFactor = 1;

        @Override
        public boolean onScaleBegin(ScaleGestureDetector detector) {
            mode = ZOOM;
            return true;
        }

        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            scaleFactor *= detector.getScaleFactor();

//            Log.d("ScaleListener", "detector.getScaleFactor() = " + String.valueOf(detector.getScaleFactor()));
//            Log.d("ScaleListener", "scaleFactor = " + String.valueOf(scaleFactor));

            mRenderer.setScaleFactor(scaleFactor);
//            requestRender();
            return true;
        }
    }

    public void addVertices(float[] vertices) {
        mRenderer.addVertices(vertices);
//        requestRender();
    }

    public void recalcOffsets() {
        mRenderer.recalcOffsets();
    }

    public void startTestThread() {
        testPointsThread = new Thread() {
            public void run() {
                float[] pointsY = pointsProvider.getPoints();
                float[] bigArray;

                bigArray = CommonUtils.arrayConcat(pointsY, pointsY);

                for (int i = 0; i < 249; i++) {
                    bigArray = CommonUtils.arrayConcat(bigArray, pointsY);
                }

                int i = 0;
                while (!interrupted()) {
                    if (i == 400) {
                        i = 0;
                    }

                    boolean flag = false;
                    float element;
                    float element2;
                    float element3;

                    Random rand = new Random();

                    if (i < 200 && flag == false) {
                        element = rand.nextInt(1962753222) - 981376611;
                        element2 = rand.nextInt(1962753222) - 981376611;
                        element3 = rand.nextInt(1962753222) - 981376611;
                    } else {
                        element = bigArray[i];
                        element2 = rand.nextInt(1962753222) - 981376611;
                        element3 = rand.nextInt(1962753222) - 981376611;
                        flag = true;
                    }

                    float[] elements = new float[3];
                    switch (Oscilloscope.getOscMode()) {
                        case QEP:
                            elements = new float[3];
                            elements[0] = element / 1000000;
                            elements[1] = element2 / 1000000;
                            elements[2] = element3 / 1000000;
                            addVertices(elements);
                            break;
                        case FFT:
                        case SAMPLES:
                            elements = new float[2];
                            elements[0] = element / 1000000;
                            elements[1] = element2 / 1000000;
                            addVertices(elements);
                            break;
                    }

                    //addVertices(elements);
                    i++;

//                    requestRender();

                    /*try {
                        Thread.sleep(430);
                    } catch (InterruptedException e) {
                        Log.d("CustomGLSurfaceView", e.toString());
                    }*/
                }
            }
        };
        testPointsThread.start();
    }

    public void stopTestThread() {
        testPointsThread.interrupt();
        testPointsThread = null;
    }
}