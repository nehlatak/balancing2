package sibek.balancing.model;

import com.orm.SugarRecord;

public class TimeStatistic extends SugarRecord<TimeStatistic> {
    public String date;
    public int totaltime;
    public int idletime;
    public int balancetime;
    public int worktime;

    public TimeStatistic() {
    }

    public TimeStatistic(String date, int totaltime, int idletime, int balancetime, int worktime) {
        this.date = date;
        this.totaltime = totaltime;
        this.idletime = idletime;
        this.balancetime = balancetime;
        this.worktime = worktime;
    }
}
