package sibek.balancing.model;

import com.orm.SugarRecord;

public class BalancingError extends SugarRecord<BalancingError> {
    public int timestamp;
    public int code;

    public BalancingError() {
    }

    public BalancingError(int timestamp, int code) {
        this.timestamp = timestamp;
        this.code = code;
    }
}