package sibek.balancing.model;

import com.orm.SugarRecord;

public class ResultStatistic extends SugarRecord<ResultStatistic> {
    public int time;
    public int user;
    public int mode;
    public int layout;
    public float width;
    public float diam;
    public int offset;
    public int splitMode;
    public int numsp;
    public float weight0;
    public float weight1;
    public float weight2;
    public float angle0;
    public float angle1;
    public float angle2;
    public int result;

    public ResultStatistic() {
    }

    public ResultStatistic(int time, int user, int mode, int layout, float width, float diam, int offset, int splitMode, int numsp, float weight0, float weight1, float weight2, float angle0, float angle1, float angle2, int result) {
        this.time = time;
        this.user = user;
        this.mode = mode;
        this.layout = layout;
        this.width = width;
        this.diam = diam;
        this.offset = offset;
        this.splitMode = splitMode;
        this.numsp = numsp;
        this.weight0 = weight0;
        this.weight1 = weight1;
        this.weight2 = weight2;
        this.angle0 = angle0;
        this.angle1 = angle1;
        this.angle2 = angle2;
        this.result = result;
    }
}