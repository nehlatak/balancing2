package sibek.balancing.activities;

import android.os.Bundle;
import android.support.v4.util.Pair;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import sibek.balancing.App;
import sibek.balancing.R;
import sibek.balancing.adapter.CustomListViewAdapter;
import sibek.balancing.menu.MenuItem;
import sibek.balancing.model.BalancingError;
import sibek.balancing.tools.TypesParams;

public class ErrorsActivity extends BaseActivity {
    private ListView errorsList;
    private Toolbar toolbar;
    private ArrayList<MenuItem> errorsItems = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_errors);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        List<BalancingError> errors = BalancingError.find(BalancingError.class, null, null, null, "timestamp DESC", null);
        for (BalancingError error : errors) {
            String title = "error_" + String.valueOf(error.code) + "_header";
            String text = "error_" + String.valueOf(error.code) + "_text";
            String errorTitle = App.getStringFromResources(title);
            String errorDescr = App.getStringFromResources(text);
            MenuItem listItem = new MenuItem(errorTitle, errorDescr);
            listItem.setIcon(R.drawable.main_icon_error);

            Date date = new Date(error.timestamp * 1000L);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String formattedDate = sdf.format(date);

            Pair<TypesParams, Object> itemValue = new Pair<>(TypesParams.STRING, (Object) formattedDate);
            listItem.setValue(itemValue);
            errorsItems.add(listItem);
        }

        errorsList = (ListView) findViewById(R.id.errorslist);
        CustomListViewAdapter customAdapter = new CustomListViewAdapter(App.getContext(), errorsItems);
        errorsList.setAdapter(customAdapter);
    }
}