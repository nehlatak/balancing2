package sibek.balancing.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ShareCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.squareup.otto.Subscribe;

import java.io.File;

import sibek.balancing.App;
import sibek.balancing.R;
import sibek.balancing.fragments.FragmentDrawer;
import sibek.balancing.fragments.MainScreenFragment;
import sibek.balancing.fragments.menu.CalibrationFragment;
import sibek.balancing.fragments.menu.DiagnosticsFragment;
import sibek.balancing.fragments.menu.SettingsFragment;
import sibek.balancing.fragments.menu.UserFragment;
import sibek.balancing.menu.NavigationMenu;
import sibek.balancing.router.VarsRouter;
import sibek.balancing.router.routes.Default;
import sibek.balancing.router.routes.Route;
import sibek.balancing.tools.AppEvents;
import sibek.balancing.tools.NetworkUtils;

public class MainActivity extends BaseActivity implements FragmentDrawer.FragmentDrawerListener {
    private Toolbar mToolbar;
    private FragmentDrawer drawerFragment;
    private Fragment fragment = null;
    private FragmentManager fragmentManager;
    private boolean doubleBackToExitPressedOnce = false;
    private MenuItem toMainScreenMenuItem;
    private MenuItem shareValuesMenuItem;
    private ProgressDialog pd;
    private static VarsRouter router = VarsRouter.getInstance();
    private Route defRoute = new Default();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        fragmentManager = getSupportFragmentManager();

        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        drawerFragment = (FragmentDrawer)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);
        drawerFragment.setDrawerListener(this);

        // display the first navigation drawer view on app launch
        displayView(0);
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }

    public void displayView(int position) {
        NavigationMenu navMenu = new NavigationMenu();
        String title = navMenu.menu.get(position).title;

        switch (position) {
            case 0:
                App.setIntParam("user", 0);
                fragment = new MainScreenFragment();
                ((MainScreenFragment) fragment).setFragmentManager(fragmentManager);
                break;
            case 1:
                App.setIntParam("user", 0);
                fragment = new UserFragment();
                break;
            case 2:
                App.setIntParam("user", 1);
                fragment = new UserFragment();
                break;
            case 3:
                fragment = new CalibrationFragment();
                break;
            case 4:
                fragment = new DiagnosticsFragment();
                break;
            case 5:
                fragment = new SettingsFragment();
                break;
            default:
                break;
        }

        if (fragment != null) {
            /*FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction()
                    .replace(R.id.container_body, fragment)
                    .addToBackStack(null);
            fragmentTransaction.commit();*/

            FragmentTransaction fTrans = fragmentManager.beginTransaction();
            fTrans.replace(R.id.container_body, fragment);
            fTrans.commit();

            // set the toolbar title
            getSupportActionBar().setTitle(title);
        }

        this.invalidateOptionsMenu();
    }

    @Override
    public void onBackPressed() {
        if (fragmentManager.getBackStackEntryCount() > 0) {
            fragmentManager.popBackStack();
        } else {

            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            App.showMessage("Нажмите еще раз чтобы выйти");

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        toMainScreenMenuItem = menu.findItem(R.id.action_main_screen);
        shareValuesMenuItem = menu.findItem(R.id.share);

        if (fragment != null && !(fragment instanceof MainScreenFragment)) {
            toMainScreenMenuItem.setVisible(true);
            shareValuesMenuItem.setVisible(true);
        } else {
            toMainScreenMenuItem.setVisible(false);
            shareValuesMenuItem.setVisible(false);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_main_screen:
                displayView(0);
                break;
            case R.id.share:
                pd = new ProgressDialog(MainActivity.this);
                pd.setTitle("Сбор статистики...");
                pd.setMessage("Пожалуйста подождите.");
                pd.setCancelable(false);
                pd.setIndeterminate(true);
                pd.show();

                App.startSaveStatistic();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Subscribe
    public void updateParams(AppEvents action) {
        switch (action) {
            case STATISTICS_BUILDED:
                if (pd != null) {
                    pd.dismiss();
                }
                App.stopSaveStatistic();
                App.getCacheFilesList();

                File file = new File(App.getStatisticFilePath());
                if (file.exists() && !file.isDirectory()) {
                    final Uri uri = FileProvider.getUriForFile(this, "sibek.balancing.fileprovider", file);
                    final Intent intent = ShareCompat.IntentBuilder.from(this)
                            .setType("application/octet-stream")
                            .setSubject(getString(R.string.share_params_subject))
//                            .setEmailTo(new String[]{"nehlatak90@gmail.com"})
                            .setText(getString(R.string.share_params_text))
                            .setStream(uri)
                            .setChooserTitle(R.string.share_params_title)
                            .createChooserIntent()
                            .addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    startActivity(intent);
                }
                break;
            case CONNECTION_ESTABLISHED:

                break;
        }
    }

    @Override
    protected void onDestroy() {
        if (pd != null) {
            pd.dismiss();
        }
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();
        router.setRoute(defRoute);
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkConnectionExist();
    }

    private void checkConnectionExist() {
        if (!NetworkUtils.isNetworkAvailable()) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(
                    MainActivity.this);
            builder.setTitle(R.string.network_is_unreachable_title);
            builder.setMessage(R.string.network_is_unreachable_text);
            builder.setPositiveButton(R.string.network_activate_button_title,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(final DialogInterface dialogInterface, final int i) {
                            startActivity(new Intent(android.provider.Settings.ACTION_WIFI_SETTINGS));
                        }
                    });
            builder.setNeutralButton(R.string.network_is_unreachable, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            builder.setCancelable(true);
            builder.create().show();
        }
    }
}



/*
public class MainActivity extends Activity {
    private ScaleGestureDetector mScaleDetector;
    private float mScaleFactor = 1.f;
    private EditText text1;
    private float previousFactor = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);






        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        Button button = (Button) findViewById(R.id.button);
        text1 = (EditText) findViewById(R.id.editText1);

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.d("DEBUG_R", String.valueOf(App.getParam("r1")));
            }
        });

        mScaleDetector = new ScaleGestureDetector(App.getContext(), new ScaleListener());
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        mScaleDetector.onTouchEvent(e);
        return true;
    }

    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            mScaleFactor *= detector.getScaleFactor();
            float value = 0;
            value = (mScaleFactor > previousFactor) ? Float.valueOf(text1.getText().toString()) + 1 : Float.valueOf(text1.getText().toString()) - 1;
            text1.setText(String.valueOf(value));
            previousFactor = mScaleFactor;
            return true;
        }
    }
}*/
