package sibek.balancing.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import sibek.balancing.App;
import sibek.balancing.router.VarsRouter;
import sibek.balancing.router.routes.Default;
import sibek.balancing.router.routes.Route;

public abstract class BaseActivity extends AppCompatActivity {
    private Unbinder unbinder;
    private VarsRouter router = VarsRouter.getInstance();
    private Route defRoute = new Default();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // remove title
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
        App.bus.register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        App.bus.unregister(this);
        unbinder.unbind();
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        unbinder = ButterKnife.bind(this);
        //тут можно заюзать ButterKnife и т.д. либы
    }
}