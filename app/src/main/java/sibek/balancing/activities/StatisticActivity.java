package sibek.balancing.activities;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import sibek.balancing.App;
import sibek.balancing.R;
import sibek.balancing.oscilloscope.SpinnerData;
import sibek.balancing.statistic.Statistic;
import sibek.balancing.statistic.StatisticByDays;
import sibek.balancing.statistic.StatisticByInches;
import sibek.balancing.statistic.StatisticByPlummet;
import sibek.balancing.statistic.StatisticByTime;
import sibek.balancing.tools.BalanceParams;

public class StatisticActivity extends BaseActivity {
    private Toolbar toolbar;
    private LinearLayout lr;
    private Spinner selectType;
    private Button prevMonth;
    private Button nextMonth;
    private TextView month;
    private TextView year;
    private Calendar calendar;
    private int mode = 0;
    private Statistic statisticGraph;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_statistic);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        lr = (LinearLayout) findViewById(R.id.chart);

        calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, 1);

        selectType = (Spinner) findViewById(R.id.all_disks);

        String statisticType;
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                statisticType = null;
            } else {
                statisticType = extras.getString("type");
            }
        } else {
            statisticType = (String) savedInstanceState.getSerializable("type");
        }

        final SpinnerData items[];

        switch (statisticType) {
            case "byDays": {
                items = new SpinnerData[3];
                items[0] = new SpinnerData("Все диски", 10);
                items[1] = new SpinnerData("Легкосплавные", BalanceParams.MODE_ALU);
                items[2] = new SpinnerData("Штампованные", BalanceParams.MODE_STEEL);

                statisticGraph = new StatisticByDays(calendar);
                getSupportActionBar().setTitle(App.getStringFromResources(R.string.stats_disks_text));
            }
            break;
            case "byInches": {
                items = new SpinnerData[3];
                items[0] = new SpinnerData("Все диски", 10);
                items[1] = new SpinnerData("Легкосплавные", BalanceParams.MODE_ALU);
                items[2] = new SpinnerData("Штампованные", BalanceParams.MODE_STEEL);

                statisticGraph = new StatisticByInches(calendar);
                getSupportActionBar().setTitle(App.getStringFromResources(R.string.stats_inches_text));
            }
            break;
            case "byPlummet": {
                items = new SpinnerData[3];
                items[0] = new SpinnerData("Все диски", 10);
                items[1] = new SpinnerData("Легкосплавные", BalanceParams.MODE_ALU);
                items[2] = new SpinnerData("Штампованные", BalanceParams.MODE_STEEL);

                statisticGraph = new StatisticByPlummet(calendar);
                getSupportActionBar().setTitle(App.getStringFromResources(R.string.stats_weights_text));
            }
            break;
            case "byTime": {
                items = new SpinnerData[3];
                items[0] = new SpinnerData("Общее время", 0);
                items[1] = new SpinnerData("Время работы", 1);
                items[2] = new SpinnerData("Время простоя", 2);

                statisticGraph = new StatisticByTime(calendar);
                getSupportActionBar().setTitle(App.getStringFromResources(R.string.stats_time_text));
            }
            break;
            default: {
                items = new SpinnerData[3];
                items[0] = new SpinnerData("Все диски", 10);
                items[1] = new SpinnerData("Легкосплавные", BalanceParams.MODE_ALU);
                items[2] = new SpinnerData("Штампованные", BalanceParams.MODE_STEEL);
            }
            break;
        }

        ArrayAdapter<SpinnerData> adapter = new ArrayAdapter<SpinnerData>(this, android.R.layout.simple_spinner_item, items);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        selectType.setAdapter(adapter);
        selectType.setSelection(0, false);

        selectType.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        mode = items[position].getValue();
                        buildGraph();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                }
        );
        selectType.getBackground().setColorFilter(Color.parseColor("#01542a"), PorterDuff.Mode.SRC_ATOP);

        prevMonth = (Button) findViewById(R.id.prev_month);
        prevMonth.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.d("DEBUG", String.valueOf("prev month"));
                calendar.add(Calendar.MONTH, -1);
                buildGraph();
            }
        });

        nextMonth = (Button) findViewById(R.id.next_month);
        nextMonth.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.d("DEBUG", String.valueOf("next month"));
                calendar.add(Calendar.MONTH, 1);
                buildGraph();
            }
        });

        year = (TextView) findViewById(R.id.year);
        month = (TextView) findViewById(R.id.month);
    }

    @Override
    protected void onResume() {
        super.onResume();
        buildGraph();
    }

    private void buildGraph() {
        lr.removeAllViews();
        lr.addView(statisticGraph.getGraph(mode));

        SimpleDateFormat monthDate = new SimpleDateFormat("LLLL");
        String monthName = monthDate.format(calendar.getTime());
        month.setText(monthName);
        year.setText(String.valueOf(calendar.get(Calendar.YEAR)));
    }
}