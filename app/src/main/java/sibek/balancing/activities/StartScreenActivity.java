package sibek.balancing.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import sibek.balancing.App;
import sibek.balancing.R;
import sibek.balancing.tools.BalanceParams;

public class StartScreenActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_start_screen);

//        textView = (TextView) findViewById(R.id.textView);
        Button startButton = (Button) findViewById(R.id.start_work);
        Button calibrationButton = (Button) findViewById(R.id.calibration_start);
        Button machineSearchButton = (Button) findViewById(R.id.machine_search);
        Button closeButton = (Button) findViewById(R.id.close_button);
        TextView firstTitle = (TextView) findViewById(R.id.first_title);
        TextView secondTitle = (TextView) findViewById(R.id.second_title);
        TextView urlTitle = (TextView) findViewById(R.id.link);

        Typeface sansBold = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Bold.ttf");
        Typeface sansRegular = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular.ttf");

        firstTitle.setTypeface(sansBold);
        secondTitle.setTypeface(sansRegular);
        urlTitle.setTypeface(sansRegular);

        startButton.setTypeface(sansBold);
        calibrationButton.setTypeface(sansBold);
        machineSearchButton.setTypeface(sansBold);

        startButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(App.getContext(), MainActivity.class);
                startActivity(intent);
                App.playSound(BalanceParams.SOUND_NORMAL, "sounds/ru/balance.wav");
            }
        });

        calibrationButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                App.sendCommand("keycal0");
                App.playSound(BalanceParams.SOUND_NORMAL, "sounds/ru/calibration_wheel.wav");
            }
        });

        closeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });

        machineSearchButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                App.showMessage("MACHINE SEARCH");
            }
        });

        urlTitle.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String url = "http://www.sibek.ru/catalog/4/balancing_machine";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });
    }
}