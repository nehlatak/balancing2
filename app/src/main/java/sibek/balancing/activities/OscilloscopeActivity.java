package sibek.balancing.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.squareup.otto.Subscribe;

import java.util.ArrayList;

import sibek.balancing.R;
import sibek.balancing.oscilloscope.ChannelMode;
import sibek.balancing.oscilloscope.Oscilloscope;
import sibek.balancing.oscilloscope.Samples;
import sibek.balancing.oscilloscope.SpinnerData;
import sibek.balancing.view.CustomGLSurfaceView;

public class OscilloscopeActivity extends BaseActivity {
    private CustomGLSurfaceView GLView;
    private Oscilloscope oscilloscope = Oscilloscope.getInstance();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_oscilloscope);

        GLView = (CustomGLSurfaceView) findViewById(R.id.GLview);
        final Button playButton = (Button) findViewById(R.id.play);
        final Button pauseButton = (Button) findViewById(R.id.pause);
        final Button stopButton = (Button) findViewById(R.id.stop);
        final Button clockwiseButton = (Button) findViewById(R.id.clockwise);
        final Button anticlockwiseButton = (Button) findViewById(R.id.anticlockwise);
        final Button qepButton = (Button) findViewById(R.id.qep);

        final Spinner ch1 = (Spinner) findViewById(R.id.ch1);
        final Spinner ch2 = (Spinner) findViewById(R.id.ch2);

        final SpinnerData items[] = new SpinnerData[10];
        items[0] = new SpinnerData("RAW_1DIFF", ChannelMode.OSC_RAW_1DIFF);
        items[1] = new SpinnerData("IIR_1", ChannelMode.OSC_IIR_1);
        items[2] = new SpinnerData("FIR_1", ChannelMode.OSC_FIR_1);
        items[3] = new SpinnerData("RAW_2DIFF", ChannelMode.OSC_RAW_2DIFF);
        items[4] = new SpinnerData("IIR_2", ChannelMode.OSC_IIR_2);
        items[5] = new SpinnerData("FIR_2", ChannelMode.OSC_FIR_2);

        items[6] = new SpinnerData("RAW1", ChannelMode.OSC_RAW1);
        items[7] = new SpinnerData("RAW2", ChannelMode.OSC_RAW2);
        items[8] = new SpinnerData("RAW3", ChannelMode.OSC_RAW3);
        items[9] = new SpinnerData("RAW4", ChannelMode.OSC_RAW4);

        ArrayAdapter<SpinnerData> adapter = new ArrayAdapter<SpinnerData>(this, android.R.layout.simple_spinner_item, items);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        ch1.setAdapter(adapter);
        ch1.setSelection(0, false);
        ch1.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        oscilloscope.setOSCmode();
                        GLView.recalcOffsets();
                        SpinnerData d1 = items[position];
                        SpinnerData d2 = items[ch2.getSelectedItemPosition()];
                        int ch1 = d1.getValue();
                        int ch2 = d2.getValue();
                        oscilloscope.setChannels(ch1, ch2);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                }
        );

        ch2.setAdapter(adapter);
        ch2.setSelection(0, false);
        ch2.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        oscilloscope.setOSCmode();
                        GLView.recalcOffsets();
                        SpinnerData d1 = items[ch1.getSelectedItemPosition()];
                        SpinnerData d2 = items[position];
                        int ch1 = d1.getValue();
                        int ch2 = d2.getValue();
                        oscilloscope.setChannels(ch1, ch2);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                }
        );

        playButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                playButton.setVisibility(View.GONE);
                pauseButton.setVisibility(View.VISIBLE);
                oscilloscope.play();
            }
        });

        pauseButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                playButton.setVisibility(View.VISIBLE);
                pauseButton.setVisibility(View.GONE);
                oscilloscope.pause();
            }
        });

        clockwiseButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                oscilloscope.rotateClockwise();
                clockwiseButton.setVisibility(View.GONE);
                anticlockwiseButton.setVisibility(View.GONE);
                stopButton.setVisibility(View.VISIBLE);
            }
        });

        anticlockwiseButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                oscilloscope.rotateAntiClockwise();
                clockwiseButton.setVisibility(View.GONE);
                anticlockwiseButton.setVisibility(View.GONE);
                stopButton.setVisibility(View.VISIBLE);
            }
        });

        stopButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                oscilloscope.stop();
                clockwiseButton.setVisibility(View.VISIBLE);
                anticlockwiseButton.setVisibility(View.VISIBLE);
                stopButton.setVisibility(View.GONE);
            }
        });

        qepButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                oscilloscope.setQEPmode();
                GLView.recalcOffsets();
                oscilloscope.setChannels(1, 1);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        GLView.onResume();

        Oscilloscope.isOscActivated.set(true);
        Oscilloscope.responseFlag.set(true);
        oscilloscope.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        oscilloscope.disconnect();
        GLView.onPause();
    }

    @Subscribe
    public void updateParams(ArrayList<Samples> samples) {
        for (Samples sample : samples) {
            GLView.addVertices(sample.getVertices());
        }
    }
}