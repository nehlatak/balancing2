package sibek.balancing.activities;

import android.app.ActionBar;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;

import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.HashMap;

import sibek.balancing.App;
import sibek.balancing.R;
import sibek.balancing.fragments.MessageFragment;
import sibek.balancing.fragments.WizardProgressFragment;
import sibek.balancing.tools.BalanceParams;
import sibek.balancing.wizard.CustomViewPager;
import sibek.balancing.wizard.MainStep;
import sibek.balancing.wizard.balance.BalanceStep1;
import sibek.balancing.wizard.balance.BalanceStep2;
import sibek.balancing.wizard.balance.BalanceStep3;
import sibek.balancing.wizard.balance.BalanceStep4;
import sibek.balancing.wizard.balance.BalanceStep5;
import sibek.balancing.wizard.ruler.RulerStep1;
import sibek.balancing.wizard.ruler.RulerStep2;
import sibek.balancing.wizard.ruler.RulerStep3;
import sibek.balancing.wizard.ruler.RulerStep4;
import sibek.balancing.wizard.ruler.RulerStep5;


public class WizardActivity extends BaseActivity {
    private WizardProgressFragment progressDialog = new WizardProgressFragment();
    private MessageFragment coverMessage = new MessageFragment();
    private MessageFragment wheelMessage = new MessageFragment();
    private FragmentManager fragmentManager;
    private ViewPager pager;
    private PagerAdapter pagerAdapter;
    private Button cancel;
    private Button next;
    private int balanceState = BalanceParams.STATE_IDLE;
    private int balanceSubstate = BalanceParams.BALANCE_IDLE;
    private ArrayList<MainStep> steps = new ArrayList<>();
    private boolean endFlag = false;
    private boolean stepRestore = false;
    private int stepCount = 0;
    private int[] wizardStates = {
            BalanceParams.STATE_BALANCE_CAL0,
            BalanceParams.STATE_BALANCE_CAL1,
            BalanceParams.STATE_BALANCE_CAL2,
            BalanceParams.STATE_BALANCE_CAL3,
            BalanceParams.STATE_BALANCE_CAL4,
            BalanceParams.STATE_RRULER_3P_CAL0,
            BalanceParams.STATE_RRULER_3P_CAL1,
            BalanceParams.STATE_RRULER_3P_CAL2,
            BalanceParams.STATE_RRULER_D_CAL0,
            BalanceParams.STATE_RRULER_D_CAL1,
            BalanceParams.STATE_RULER
    };
    private String wizard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wizard_layout);

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                wizard = null;
            } else {
                wizard = extras.getString("wizard");
            }
        } else {
            wizard = (String) savedInstanceState.getSerializable("wizard");
        }

        if (wizard == null) {
            finish();
        }

        steps.clear();

        switch (wizard) {
            case "balance":
                steps.add(new BalanceStep1());
                steps.add(new BalanceStep2());
                steps.add(new BalanceStep3());
                steps.add(new BalanceStep4());
                steps.add(new BalanceStep5());
                break;
            case "ruler":
                steps.add(new RulerStep1());
                steps.add(new RulerStep2());
                steps.add(new RulerStep3());
                steps.add(new RulerStep4());
                steps.add(new RulerStep5());
                break;
            case "diam":
                steps.add(new RulerStep4());
                steps.add(new RulerStep5());
                break;
        }

        pager = (CustomViewPager) findViewById(R.id.pager);
        next = (Button) findViewById(R.id.next_step);
        cancel = (Button) findViewById(R.id.cancel_wizard);

        pagerAdapter = new MyFragmentPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(pagerAdapter);

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                toNextStep();
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int curPage = pager.getCurrentItem() + 1;
                if (curPage <= (pagerAdapter.getCount() - 1)) {
                    pager.setCurrentItem(curPage);
                } else {
                    toNextStep();
                    endFlag = true;
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                App.sendCommand("stop");
                finish();
            }
        });

        coverMessage.setMessageText(App.getContext().getString(R.string.push_cover));
        coverMessage.setBackground(R.drawable.lower_cover);
        wheelMessage.setMessageText(App.getContext().getString(R.string.rotate_wheel));
        wheelMessage.setBackground(R.drawable.scroll_wheel);
    }

    @Override
    protected void onResume() {
        super.onResume();
        endFlag = false;
        if (this.fragmentManager == null) {
            this.fragmentManager = getSupportFragmentManager();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        App.sendCommand("stop");
    }

    private void toNextStep() {
        if (wizard.equals("balance")) {
            if (stepCount != 1) {
                if (balanceState >= BalanceParams.STATE_BALANCE_CAL0 && balanceState <= BalanceParams.STATE_BALANCE_CAL3) {
                    App.sendCommand("start");
                } else {
                    App.sendCommand("enter");
                }
            }
            stepCount++;
        } else {
            if (balanceState >= BalanceParams.STATE_BALANCE_CAL0 && balanceState <= BalanceParams.STATE_BALANCE_CAL3) {
                App.sendCommand("start");
            } else {
                App.sendCommand("enter");
            }
        }
    }

    private class MyFragmentPagerAdapter extends FragmentPagerAdapter {
        public MyFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return steps.get(position);
        }

        @Override
        public int getCount() {
            return steps.size();
        }
    }

    @Subscribe
    public void setParams(HashMap<String, Float> params) {
        balanceState = App.getIntParam("state");
        balanceSubstate = App.getIntParam("substate");

        // handle progress states
        if (balanceState == BalanceParams.STATE_IDLE || balanceState == BalanceParams.STATE_RULER) {
            hideBalanceProgress();
            if (endFlag) {
                finish();
            }
        } else if (balanceState == BalanceParams.STATE_BALANCE || (balanceState >= BalanceParams.STATE_BALANCE_CAL0 && balanceState <= BalanceParams.STATE_BALANCE_CAL3)) {
            if (balanceSubstate == BalanceParams.BALANCE_IDLE) {
                hideBalanceProgress();
            } else if (balanceSubstate == BalanceParams.BALANCE_WAIT_COVER) {
                hideBalanceProgress();
            } else if (balanceSubstate >= BalanceParams.BALANCE_START && balanceSubstate < BalanceParams.BALANCE_AUTO_ROTATION) {
                showBalanceProgress();
            } else {
                hideBalanceProgress();
            }
        } else if ((balanceState >= BalanceParams.STATE_RULER_CAL0 && balanceState <= BalanceParams.STATE_RULER_CAL3) ||
                (balanceState >= BalanceParams.STATE_RRULER_3P_CAL0 && balanceState <= BalanceParams.STATE_RRULER_D_CAL1)) {
            hideBalanceProgress();
        }

        //draw cover message
        if ((balanceState == BalanceParams.STATE_BALANCE || (balanceState >= BalanceParams.STATE_BALANCE_CAL0 && balanceState <= BalanceParams.STATE_BALANCE_CAL3))
                && balanceSubstate == BalanceParams.BALANCE_WAIT_COVER) {
            showCoverMessage();
            disableControls();
        } else {
            hideCoverMessage();
            enableControls();
        }

        // draw wheel message
        if (Math.floor(App.balanceErrors0 / (Math.pow(2, 18)) % 2) != 0) {
            showWheelMessage();
            disableControls();
        } else {
            hideWheelMessage();
            enableControls();
        }

        if (balanceSubstate == BalanceParams.BALANCE_IDLE) {
            enableControls();
            if (App.zeroFlag && balanceState == BalanceParams.STATE_BALANCE_CAL1 && stepCount == 2) {
                next.setEnabled(false);
            } else {
                next.setEnabled(true);
            }
        } else {
            disableControls();
        }

        if (balanceState == BalanceParams.STATE_RULER) {
            disableControls();
        }

        if (!contains(wizardStates, balanceState)) {
            finish();
        }
    }

    private boolean contains(int[] array, int value) {
        for (int item : array) {
            if (item == value) {
                return true;
            }
        }
        return false;
    }

    private void disableControls() {
        next.setEnabled(false);
        cancel.setEnabled(false);
    }

    private void enableControls() {
        next.setEnabled(true);
        cancel.setEnabled(true);
    }

    private void showBalanceProgress() {
        progressDialog.setRetainInstance(true);
        progressDialog.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        progressDialog.show(fragmentManager, "progressDialog");
    }

    public void hideBalanceProgress() {
        progressDialog.dismiss();
    }

    @Override
    public void onBackPressed() {
        App.sendCommand("stop");
        finish();
    }

    private void showCoverMessage() {
        coverMessage.setRetainInstance(true);
        coverMessage.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        coverMessage.show(fragmentManager, "coverMessage");
    }

    public void hideCoverMessage() {
        coverMessage.dismiss();
    }

    private void showWheelMessage() {
        wheelMessage.setRetainInstance(true);
        wheelMessage.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        wheelMessage.show(fragmentManager, "wheelMessage");
    }

    public void hideWheelMessage() {
        wheelMessage.dismiss();
    }
}