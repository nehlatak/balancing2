package sibek.balancing.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;

import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import sibek.balancing.App;
import sibek.balancing.R;
import sibek.balancing.router.VarsRouter;
import sibek.balancing.router.routes.BleWizard;
import sibek.balancing.router.routes.Route;
import sibek.balancing.wizard.MainStep;
import sibek.balancing.wizard.ble_ruler.BleRulerStep1;
import sibek.balancing.wizard.ble_ruler.BleRulerStep2;
import sibek.balancing.wizard.ble_ruler.BleRulerStep3;
import sibek.balancing.wizard.ble_ruler.BleRulerStep4;
import sibek.balancing.wizard.ble_ruler.BleRulerStep5;

/**
 * Created by dracula on 25.10.2016.
 */

public class BleRulerWizardActivity extends BaseActivity {
    private ArrayList<MainStep> steps = new ArrayList<>();
    private PagerAdapter pagerAdapter;
    private FragmentManager fragmentManager;
    private VarsRouter router = VarsRouter.getInstance();
    private Route wizardRoute = new BleWizard();

    @BindView(R.id.pager)
    ViewPager pager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        App.sendCommand("rcal 0 1");

        setContentView(R.layout.activity_ble_ruler_wizard_layout);

        setSteps();

        pagerAdapter = new MyFragmentPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(pagerAdapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        router.setRoute(wizardRoute);
    }

    @OnClick(R.id.next_step)
    public void next() {
        int curPage = pager.getCurrentItem();
        curPage++;

        toNextStep();

        if (curPage <= (pagerAdapter.getCount() - 1)) {
            pager.setCurrentItem(curPage);
        } else {
            finish();
        }
    }

    @OnClick(R.id.cancel_wizard)
    public void cancel() {
        App.sendCommand("stop");
        finish();
    }

    private void setSteps() {
        steps.clear();
        steps.add(new BleRulerStep1());
        steps.add(new BleRulerStep2());
        steps.add(new BleRulerStep3());
        steps.add(new BleRulerStep4());
        steps.add(new BleRulerStep5());
    }

    private class MyFragmentPagerAdapter extends FragmentPagerAdapter {
        public MyFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return steps.get(position);
        }

        @Override
        public int getCount() {
            return steps.size();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (this.fragmentManager == null) {
            this.fragmentManager = getSupportFragmentManager();
        }
    }

    private void toNextStep() {
        App.sendCommand("enter");
    }
}