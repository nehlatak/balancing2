package sibek.balancing.statistic;

import android.graphics.Color;

import com.orm.query.Condition;
import com.orm.query.Select;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.BarChart;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import sibek.balancing.App;
import sibek.balancing.model.TimeStatistic;

public class StatisticByTime extends Statistic {
    public StatisticByTime(Calendar calendar) {
        super(calendar);
    }

    private int daysInMonth;

    @Override
    public GraphicalView getGraph(int mode) {
        this.mode = mode;
        daysInMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);

        // Now we create the renderer
        XYSeriesRenderer seriesRenderer = new XYSeriesRenderer();
        seriesRenderer.setColor(Color.BLUE);
        // Include low and max value
        seriesRenderer.setDisplayBoundingPoints(true);
        seriesRenderer.setColor(Color.parseColor("#00569a"));

        // Finaly we create the multiple series renderer to control the graph
        XYMultipleSeriesRenderer multipleSeriesRenderer = new XYMultipleSeriesRenderer();
        multipleSeriesRenderer.addSeriesRenderer(seriesRenderer);

        multipleSeriesRenderer.setLabelsTextSize(App.dpToPx(20));
        multipleSeriesRenderer.setLegendTextSize(App.dpToPx(15));
        multipleSeriesRenderer.setMargins(new int[]{30, 30, 30, 30});
        multipleSeriesRenderer.setShowLegend(false);

        // We want to avoid black border
        multipleSeriesRenderer.setMarginsColor(Color.argb(0x00, 0xff, 0x00, 0x00)); // transparent margins
        // Disable Pan on two axis
        multipleSeriesRenderer.setPanEnabled(false, false);
        multipleSeriesRenderer.setZoomEnabled(false, false);

        multipleSeriesRenderer.setShowGrid(true); // we show the grid
        multipleSeriesRenderer.setXLabels(daysInMonth);
        multipleSeriesRenderer.setBarSpacing(1);

        return ChartFactory.getBarChartView(App.getContext(), getDataset(), multipleSeriesRenderer, BarChart.Type.DEFAULT);
    }

    private XYMultipleSeriesDataset getDataset() {
        XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
        dataset.addSeries(getSeries());
        return dataset;
    }

    private XYSeries getSeries() {
        XYSeries series = new XYSeries("");
        int xValue = 1;

        while (xValue <= daysInMonth) {
            Date day = getDay(xValue, calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.YEAR));

            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            String calendarDay = dateFormat.format(day);

            long yValue = 0;

            List<TimeStatistic> result = Select.from(TimeStatistic.class).where(Condition.prop("date").eq(calendarDay)).list();
            if (result.size() > 0) {
                TimeStatistic row = result.get(0);

                switch (mode) {
                    case 0:
                        yValue = TimeUnit.SECONDS.toHours(row.totaltime);
                        break;
                    case 1:
                        yValue = TimeUnit.SECONDS.toHours(row.balancetime) + TimeUnit.SECONDS.toHours(row.worktime);
                        break;
                    case 2:
                        yValue = TimeUnit.SECONDS.toHours(row.idletime);
                        break;
                }
            }

            series.add(xValue, yValue);
            xValue++;
        }
        return series;
    }
}