package sibek.balancing.statistic;

import android.graphics.Color;
import android.util.Log;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.BarChart;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import sibek.balancing.App;
import sibek.balancing.model.ResultStatistic;
import sibek.balancing.tools.BalanceParams;

public class StatisticByInches extends Statistic {
    private int inchesCount;

    public StatisticByInches(Calendar calendar) {
        super(calendar);
    }

    @Override
    public GraphicalView getGraph(int mode) {
        this.mode = mode;

        // Now we create the renderer
        XYSeriesRenderer seriesRenderer = new XYSeriesRenderer();
        seriesRenderer.setColor(Color.BLUE);
        // Include low and max value
        seriesRenderer.setDisplayBoundingPoints(true);
        seriesRenderer.setColor(Color.parseColor("#00569a"));

        // Finaly we create the multiple series renderer to control the graph
        XYMultipleSeriesRenderer multipleSeriesRenderer = new XYMultipleSeriesRenderer();
        multipleSeriesRenderer.addSeriesRenderer(seriesRenderer);

        multipleSeriesRenderer.setLabelsTextSize(App.dpToPx(20));
        multipleSeriesRenderer.setLegendTextSize(App.dpToPx(15));
        multipleSeriesRenderer.setMargins(new int[]{30, 30, 30, 30});
        multipleSeriesRenderer.setShowLegend(false);

        // We want to avoid black border
        multipleSeriesRenderer.setMarginsColor(Color.argb(0x00, 0xff, 0x00, 0x00)); // transparent margins
        // Disable Pan on two axis
        multipleSeriesRenderer.setPanEnabled(false, false);
        multipleSeriesRenderer.setZoomEnabled(false, false);

        inchesCount = 24;

        multipleSeriesRenderer.setShowGrid(true); // we show the grid
        multipleSeriesRenderer.setXLabels(12);
        multipleSeriesRenderer.setBarSpacing(1);

        return ChartFactory.getBarChartView(App.getContext(), getDataset(), multipleSeriesRenderer, BarChart.Type.DEFAULT);
    }

    private XYMultipleSeriesDataset getDataset() {
        XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
        dataset.addSeries(getSeries());
        return dataset;
    }

    private XYSeries getSeries() {
        XYSeries series = new XYSeries("");
        int xValue = 12;
        Date dayStart = getDay(1, calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.YEAR));
        Date dayEnd = getDay(calendar.getActualMaximum(Calendar.DAY_OF_MONTH), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.YEAR));
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        int startOfDayInSeconds = 0;
        int endOfDayInSeconds = 0;
        try {
            startOfDayInSeconds = getStartOfDayInSeconds(dateFormat.format(dayStart));
            endOfDayInSeconds = getEndOfDayInSeconds(dateFormat.format(dayEnd));
        } catch (ParseException e) {
            Log.d("DEBUG", e.toString());
        }

        while (xValue <= inchesCount) {
            long yValue = 0;
            if (mode == BalanceParams.MODE_ALU || mode == BalanceParams.MODE_STEEL) {
                yValue = ResultStatistic.count(ResultStatistic.class, "date(time)>=date(?) AND date(time)<=date(?) AND result = 1 AND mode = ? AND diam = ?", new String[]{String.valueOf(startOfDayInSeconds), String.valueOf(endOfDayInSeconds), String.valueOf(mode), String.valueOf(xValue)});
            } else {
                yValue = ResultStatistic.count(ResultStatistic.class, "date(time)>=date(?) AND date(time)<=date(?) AND result = 1 AND diam = ?", new String[]{String.valueOf(startOfDayInSeconds), String.valueOf(endOfDayInSeconds), String.valueOf(xValue)});
            }

            series.add(xValue, yValue);
            xValue++;
        }
        return series;
    }
}