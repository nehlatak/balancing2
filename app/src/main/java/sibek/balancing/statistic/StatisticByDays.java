package sibek.balancing.statistic;

import android.graphics.Color;
import android.util.Log;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.BarChart;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import sibek.balancing.App;
import sibek.balancing.model.ResultStatistic;
import sibek.balancing.tools.BalanceParams;

public class StatisticByDays extends Statistic {
    public StatisticByDays(Calendar calendar) {
        super(calendar);
    }

    private int daysInMonth;

    @Override
    public GraphicalView getGraph(int mode) {
        this.mode = mode;
        daysInMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);

        // Now we create the renderer
        XYSeriesRenderer seriesRenderer = new XYSeriesRenderer();
        seriesRenderer.setColor(Color.BLUE);
        // Include low and max value
        seriesRenderer.setDisplayBoundingPoints(true);
        seriesRenderer.setColor(Color.parseColor("#00569a"));

        // Finaly we create the multiple series renderer to control the graph
        XYMultipleSeriesRenderer multipleSeriesRenderer = new XYMultipleSeriesRenderer();
        multipleSeriesRenderer.addSeriesRenderer(seriesRenderer);

        multipleSeriesRenderer.setLabelsTextSize(App.dpToPx(20));
        multipleSeriesRenderer.setLegendTextSize(App.dpToPx(15));
        multipleSeriesRenderer.setMargins(new int[]{30, 30, 30, 30});
        multipleSeriesRenderer.setShowLegend(false);

        // We want to avoid black border
        multipleSeriesRenderer.setMarginsColor(Color.argb(0x00, 0xff, 0x00, 0x00)); // transparent margins
        // Disable Pan on two axis
        multipleSeriesRenderer.setPanEnabled(false, false);

        multipleSeriesRenderer.setShowGrid(true); // we show the grid
        multipleSeriesRenderer.setXLabels(daysInMonth);
        multipleSeriesRenderer.setBarSpacing(1);
        multipleSeriesRenderer.setZoomEnabled(false, false);

        return ChartFactory.getBarChartView(App.getContext(), getDataset(), multipleSeriesRenderer, BarChart.Type.DEFAULT);
    }

    private XYSeries getSeries() {
        XYSeries series = new XYSeries("");
        int xValue = 1;
        while (xValue <= daysInMonth) {
            Date day = getDay(xValue, calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.YEAR));

            try {
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                int startOfDayInSeconds = getStartOfDayInSeconds(dateFormat.format(day));
                int endOfDayInSeconds = getEndOfDayInSeconds(dateFormat.format(day));
                long yValue = 0;
                if (mode == BalanceParams.MODE_ALU || mode == BalanceParams.MODE_STEEL) {
                    yValue = ResultStatistic.count(ResultStatistic.class, "date(time)>=date(?) AND date(time)<=date(?) AND result = 1 AND mode = ? ", new String[]{String.valueOf(startOfDayInSeconds), String.valueOf(endOfDayInSeconds), String.valueOf(mode)});
                } else {
                    yValue = ResultStatistic.count(ResultStatistic.class, "date(time)>=date(?) AND date(time)<=date(?) AND result = 1", new String[]{String.valueOf(startOfDayInSeconds), String.valueOf(endOfDayInSeconds)});
                }
                series.add(xValue, yValue);
            } catch (ParseException e) {
                Log.d("DEBUG", e.toString());
                return null;
            }
            xValue++;
        }
        return series;
    }

    private XYMultipleSeriesDataset getDataset() {
        XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
        dataset.addSeries(getSeries());
        return dataset;
    }
}