package sibek.balancing.statistic;

import android.util.Log;

import org.achartengine.GraphicalView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public abstract class Statistic {
    protected Calendar calendar;
    protected int mode;

    public Statistic(Calendar calendar) {
        this.calendar = calendar;
    }

    public abstract GraphicalView getGraph(int mode);

    protected int getStartOfDayInSeconds(String date) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(format.parse(date));
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        return (int) (calendar.getTimeInMillis() / 1000);
    }

    protected int getEndOfDayInSeconds(String date) throws ParseException {
        return getStartOfDayInSeconds(date) + (24 * 60 * 60) - 1;
    }

    protected Date getDay(int day, int month, int year) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String dateInString = day + "/" + month + "/" + year;

        try {
            return formatter.parse(dateInString);
        } catch (ParseException e) {
            Log.d("DEBUG", e.toString());
            return null;
        }
    }
}