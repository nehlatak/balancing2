package sibek.balancing.network;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import sibek.balancing.App;
import sibek.balancing.router.machine_vars.VarsParams;
import sibek.balancing.router.VarsRouter;
import sibek.balancing.oscilloscope.Oscilloscope;
import sibek.balancing.tools.AppEvents;

public class TcpClientSupport extends Thread {
    private ConnectionService service;
    private TcpClient tcpConnection;
    private final Handler mainThread = new Handler(Looper.getMainLooper());
    private VarsRouter router = VarsRouter.getInstance();
    private VarsParams varsParams;
    private final long POLLING_FREQUENCY = 100;

    public TcpClientSupport(ConnectionService service, TcpClient tcpClient) {
        this.service = service;
        this.tcpConnection = tcpClient;
        Log.d("TELNET", String.valueOf("Create new support!"));
        this.service.setResponseFlag(true);
    }

    @Override
    public void run() {
        while (service.getConnectFlag() && !isInterrupted()) {
            if (!Oscilloscope.isOscActivated.get()) {
                if (!tcpConnection.isClosed()) {
                    try {
//                        if (!App.sendCommand("state")) {
                        varsParams = router.getRouteVars();
                        if (!App.sendCommand(varsParams.getCommand())) {
                            service.setResponseFlag(false);
                            mainThread.post(new Runnable() {
                                @Override
                                public void run() {
                                    App.bus.post(AppEvents.CONNECTION_LOST);
                                }
                            });
                        } else {
                            service.setResponseFlag(true);
                            mainThread.post(new Runnable() {
                                @Override
                                public void run() {
                                    App.bus.post(AppEvents.CONNECTION_ESTABLISHED);
                                }
                            });
                        }
                        Thread.sleep(POLLING_FREQUENCY);
                    } catch (InterruptedException e) {
                        Log.d("TELNET", "support interrupt exception1 - " + e.toString());
                        Thread.currentThread().interrupt();
                    }
                } else {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        Log.d("TELNET", "support interrupt exception2 - " + e.toString());
                        Thread.currentThread().interrupt();
                    }
                }
            } else {
                Thread.yield();
            }
        }
    }
}