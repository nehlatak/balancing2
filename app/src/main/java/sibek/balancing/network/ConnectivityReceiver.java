package sibek.balancing.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import sibek.balancing.App;
import sibek.balancing.tools.AppEvents;
import sibek.balancing.tools.NetworkUtils;

public class ConnectivityReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        App.bus.register(this);

        if (NetworkUtils.isNetworkAvailable()) {
            Log.d("TELNET", "network available!");
            App.bus.post(AppEvents.NETWORK_AVAILABLE);
        } else {
            Log.d("TELNET", "network unavailable!");
            App.bus.post(AppEvents.NETWORK_UNAVAILABLE);
        }
    }
};