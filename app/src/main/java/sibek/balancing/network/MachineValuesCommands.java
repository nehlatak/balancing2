package sibek.balancing.network;

import java.util.ArrayList;

import sibek.balancing.App;

/**
 * Created by dracula on 09.11.2016.
 */

public class MachineValuesCommands {
    private static ArrayList<String> commands = new ArrayList<>();

    static {
        commands.add("vlist");
        commands.add("clist");
    }

    public static ArrayList<String> getCommands() {
        return commands;
    }
}