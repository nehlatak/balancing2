package sibek.balancing.network;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;

import sibek.balancing.App;
import sibek.balancing.router.VarsRouter;
import sibek.balancing.oscilloscope.Oscilloscope;
import sibek.balancing.tools.AppEvents;

public class TcpClient extends Thread {
    private String command = "";
    private Socket socket = null;
    private PrintWriter out;
    private BufferedReader in;
    private String response = "";
    private TcpClientSupport tcpClientSupport;
    private int port = 23;
    private int timeout = 5000;
    private final Handler mainThread = new Handler(Looper.getMainLooper());
    private ConnectionService service;
    private String result = "";
    private static VarsRouter router = VarsRouter.getInstance();

    public TcpClient(ConnectionService service) {
        this.service = service;
    }

    @Override
    public void run() {
        App.bus.register(this);

        while (service.getConnectFlag()) {
            if (socket != null && !socket.isClosed()) {
                try {
                    socket.setSoTimeout(timeout);
                    Log.d("TcpClient", "Connected!");
                    tcpClientSupport = new TcpClientSupport(service, this);
                    tcpClientSupport.start();

                    while (service.getResponseFlag()) {
                        try {
                            response = in.readLine();

//                            Log.d("TcpClient", "response = " + String.valueOf(response));

                            if (!response.equals(result)) {
                                result = response;
//                                responseProcessing(response);
                                service.triggerResponseHandlers(result);
                            }
                            response = "";
                        } catch (SocketTimeoutException e) {
                            //станок ничего не прислал в течении setSoTimeout()
                            if (!Oscilloscope.isOscActivated.get()) {
                                service.setResponseFlag(false);
                            }
                        }
                        catch (NullPointerException e) {
                            e.printStackTrace();
                            Log.d("TcpClient", e.toString());
                        }
                    }
                } catch (ConnectException e) {
                    Log.d("TcpClient", "Cant connect!");
                } catch (IOException e) {
                    Log.d("TcpClient", e.toString());
                } finally {
                    try {
                        if (socket != null) {
                            socket.close();
                            if (tcpClientSupport != null) {
                                Log.d("TcpClient", "Trying to interrupt support thread!");
                                tcpClientSupport.interrupt();
                                tcpClientSupport.join();
                            }
                            mainThread.post(new Runnable() {
                                @Override
                                public void run() {
                                    App.bus.post(AppEvents.CONNECTION_LOST);
                                }
                            });
                            Log.d("TcpClient", "Connection was closed!");
                            Log.d("TcpClient", "---------------------------------");
                        }
                    } catch (IOException | InterruptedException e) {
                        Log.d("TcpClient", e.toString());
                    }
                }
            } else {
                connect();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    Log.d("TcpClient", e.toString());
                }
            }
        }
    }

    public boolean sendCommand(String command) {
        String routeCommand = router.getRouteVars().getCommand();
        this.command = command + "\r\n";
        if (out != null && !out.checkError() && !isClosed() && !isInterrupted()) {
            out.print(this.command);
            out.flush();
            if (!command.equals(routeCommand)) {
                Log.d("TcpClient", "Command \"" + command + "\" is send!");
            }
            return true;
        } else {
            Log.d("TcpClient", "Can't send command = \"" + command + "\". Connection is closed!");
            return false;
        }
    }

    public void close() {
        if (!this.isClosed()) {
            service.setConnectFlag(false);
            service.setResponseFlag(false);
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    Log.d("TcpClient", "Socket close error!" + e.toString());
                }
            }
        }
    }

    public void connect() {
        try {
            socket = new Socket();
            Log.d("TcpClient", "Trying to connect!");
//            socket.connect(new InetSocketAddress(ip, port), timeout);
            socket.connect(new InetSocketAddress(App.getProfileStringParam("server_addr"), port), timeout);
            out = new PrintWriter(socket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            service.setConnectFlag(true);
        } catch (SocketTimeoutException e) {
            Log.d("TcpClient", "Cant connect by timeout!");
        } catch (IOException e) {
            Log.d("TcpClient", e.toString());
        }
    }

    public synchronized boolean isClosed() {
        if (socket != null) {
            return socket.isClosed();
        }
        return true;
    }
}