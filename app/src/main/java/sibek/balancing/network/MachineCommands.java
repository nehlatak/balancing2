package sibek.balancing.network;

import java.util.ArrayList;

import sibek.balancing.App;

/**
 * Created by dracula on 21.09.2016.
 */

public final class MachineCommands {
    private static ArrayList<String> commands = new ArrayList<>();

    static {
        commands.add("state");
        if (!App.oldMachineType) {//TODO: убрать после отказа от старой платы
            commands.add("vval");
            commands.add("cval");
        }
    }

    public static ArrayList<String> getCommands() {
        return commands;
    }
}