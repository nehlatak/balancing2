package sibek.balancing.network;

import android.util.Log;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicBoolean;

import sibek.balancing.App;
import sibek.balancing.network.response_handlers.UDPResponseHandler;
import sibek.balancing.oscilloscope.OscMode;
import sibek.balancing.oscilloscope.Oscilloscope;
import sibek.balancing.oscilloscope.Samples;
import sibek.balancing.tools.CommonUtils;

public class UdpClient extends Thread {
    private DatagramSocket udpSocket;
    private DatagramPacket receivePacket;
    private int timeout = 5000;
    private int serverPort = 16666;
    private volatile UDPResponseHandler responseHandler;
    private ArrayList<float[]> vertices = new ArrayList<>();
    private InetAddress IPAddress;

    @Override
    public void run() {
        byte[] receiveData = new byte[512];
        try {
            while (!isInterrupted()) {
                if (Oscilloscope.responseFlag.get()) {

                    if (udpSocket == null) {
                        udpSocket = new DatagramSocket();
                        udpSocket.setSoTimeout(timeout);
                        sendStartPacket();
                        Log.d("UdpClient", "create udp new socket!");
//                        IPAddress = InetAddress.getByName("192.168.16.192");
                        IPAddress = InetAddress.getByName(App.getProfileStringParam("server_addr"));
                    }

                    while (Oscilloscope.responseFlag.get()) {
                        try {
                            receivePacket = new DatagramPacket(receiveData, receiveData.length);
                            udpSocket.receive(receivePacket);

                            byte[] buf = Arrays.copyOf(receivePacket.getData(), receivePacket.getLength());

                            responseHandler.doAction(buf);
                        } catch (SocketTimeoutException e) {
                            Log.d("UdpClient", e.toString());
                        }
                    }
                } else {
                    Thread.yield();
                    Log.d("UdpClient", "waiting...");
                }
            }
        } catch (UnknownHostException e) {
            Log.d("UdpClient", e.toString());
        } catch (SocketException e) {
            Log.d("UdpClient", e.toString());
        } catch (IOException e) {
            Log.d("UdpClient", e.toString());
        } finally {
            if (udpSocket != null) {
                udpSocket.close();
            }
            Thread.currentThread().interrupt();
            Log.d("UdpClient", "close and interrupt thread!");
        }
    }

    public void sendStartPacket() {
        sendPacket("1");
    }

    public void sendPacket(final String sentence) {
        (new Thread() {
            public void run() {
                try {
                    Thread.sleep(100);
                    byte[] sendData = sentence.getBytes();
                    DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, serverPort);
                    udpSocket.send(sendPacket);
                    Log.d("UdpClient", String.valueOf(sentence + " was send!"));
                } catch (IOException | InterruptedException e) {
                    Log.d("UdpClient", e.toString());
                }
            }
        }).start();
    }

    public void setResponseHandler(UDPResponseHandler responseHandler) {
        this.responseHandler = responseHandler;
    }
}