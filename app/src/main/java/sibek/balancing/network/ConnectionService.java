package sibek.balancing.network;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.squareup.otto.Subscribe;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import sibek.balancing.App;
import sibek.balancing.R;
import sibek.balancing.activities.MainActivity;
import sibek.balancing.network.response_handlers.CurrentRouteResponseHandler;
import sibek.balancing.network.response_handlers.TCPResponseHandler;
import sibek.balancing.tools.AppEvents;
import sibek.balancing.tools.NetworkUtils;

public class ConnectionService extends Service {
    private int wheelangleMachine = 0;
    private int wheelangleMachine0 = 0;
    private int wheelangleMachine1 = 0;
    private int wheelangleMachine2 = 0;

    private TcpClient tcpConnection = null;
    private boolean connected = true;        //флаг активности подключения
    private boolean responseFlag = true;     //флаг о том, пришел ли ответ от станка
    private static Map<String, String> curMachineState = new HashMap<>();
    private ConcurrentHashMap<String, TCPResponseHandler> handlers = new ConcurrentHashMap<>();
    public final Object nSync = new Object();
    private final String stateKey = "stateKey";

    public ConnectionService() {
        addResponseHandler(stateKey, new CurrentRouteResponseHandler());
    }

    private final IBinder mBinder = new LocalBinder();

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public class LocalBinder extends Binder {
        public ConnectionService getService() {
            return ConnectionService.this;
        }
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    private static final Class<?>[] mSetForegroundSignature = new Class[]{
            boolean.class};
    private static final Class<?>[] mStartForegroundSignature = new Class[]{
            int.class, Notification.class};
    private static final Class<?>[] mStopForegroundSignature = new Class[]{
            boolean.class};

    private NotificationManager mNM;
    private Method mSetForeground;
    private Method mStartForeground;
    private Method mStopForeground;
    private Object[] mSetForegroundArgs = new Object[1];
    private Object[] mStartForegroundArgs = new Object[2];
    private Object[] mStopForegroundArgs = new Object[1];

    void invokeMethod(Method method, Object[] args) {
        try {
            method.invoke(this, args);
        } catch (InvocationTargetException e) {
            // Should not happen.
            Log.w("ApiDemos", "Unable to invoke method", e);
        } catch (IllegalAccessException e) {
            // Should not happen.
            Log.w("ApiDemos", "Unable to invoke method", e);
        }
    }

    /**
     * This is a wrapper around the new startForeground method, using the older
     * APIs if it is not available.
     */
    void startForegroundCompat(int id, Notification notification) {
        // If we have the new startForeground API, then use it.
        if (mStartForeground != null) {
            mStartForegroundArgs[0] = Integer.valueOf(id);
            mStartForegroundArgs[1] = notification;
            invokeMethod(mStartForeground, mStartForegroundArgs);
            return;
        }

        // Fall back on the old API.
        mSetForegroundArgs[0] = Boolean.TRUE;
        invokeMethod(mSetForeground, mSetForegroundArgs);
        mNM.notify(id, notification);
    }

    /**
     * This is a wrapper around the new stopForeground method, using the older
     * APIs if it is not available.
     */
    void stopForegroundCompat(int id) {
        // If we have the new stopForeground API, then use it.
        if (mStopForeground != null) {
            mStopForegroundArgs[0] = Boolean.TRUE;
            invokeMethod(mStopForeground, mStopForegroundArgs);
            return;
        }

        // Fall back on the old API.  Note to cancel BEFORE changing the
        // foreground state, since we could be killed at that point.
        mNM.cancel(id);
        mSetForegroundArgs[0] = Boolean.FALSE;
        invokeMethod(mSetForeground, mSetForegroundArgs);
    }

    @Override
    public void onCreate() {
        App.bus.register(this);

        String serverAddress = App.getProfileStringParam("server_addr");
        if (!serverAddress.isEmpty()) {
            if (NetworkUtils.isNetworkAvailable()) {
                this.connectToMachine();
            }
        }

        mNM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        try {
            mStartForeground = getClass().getMethod("startForeground",
                    mStartForegroundSignature);
            mStopForeground = getClass().getMethod("stopForeground",
                    mStopForegroundSignature);
            return;
        } catch (NoSuchMethodException e) {
            // Running on an older platform.
            mStartForeground = mStopForeground = null;
        }
        try {
            mSetForeground = getClass().getMethod("setForeground",
                    mSetForegroundSignature);
        } catch (NoSuchMethodException e) {
            throw new IllegalStateException(
                    "OS doesn't have Service.startForeground OR Service.setForeground!");
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String noteString = "Connected!";
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.plug_and_plug_connection)
                        .setContentTitle("Machine connection");
//                        .setContentText(noteString);
        Notification note = mBuilder.build();

        startForegroundCompat(666, note);
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.closeConnection();
        App.bus.unregister(this);
        stopForegroundCompat(1);
    }

    private boolean isForeground(String myPackage) {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> runningTaskInfo = manager.getRunningTasks(1);
        ComponentName componentInfo = runningTaskInfo.get(0).topActivity;
        return componentInfo.getPackageName().equals(myPackage);
    }

    public boolean sendCommand(String telnetCommand) {
        if (tcpConnection != null) {
            return tcpConnection.sendCommand(telnetCommand);
        } else {
            return false;
        }
    }

    public void setConnectFlag(boolean connect) {
        synchronized (nSync) {
            this.connected = connect;
            App.connectFlag = connect;
        }
    }

    public boolean getConnectFlag() {
        synchronized (nSync) {
            return this.connected;
        }
    }

    public void setResponseFlag(boolean responseFlag) {
        synchronized (nSync) {
            this.responseFlag = responseFlag;
        }
    }

    public boolean getResponseFlag() {
        synchronized (nSync) {
            return this.responseFlag;
        }
    }

    private void connectToMachine() {
        this.setConnectFlag(true);
        this.setResponseFlag(true);
        tcpConnection = new TcpClient(this);
        tcpConnection.start();
    }

    private boolean isConnected() {
        if (tcpConnection == null) {
            return false;
        }
        return !tcpConnection.isClosed();
    }

    private void closeConnection() {
        if (tcpConnection != null) {
            tcpConnection.close();
            try {
                tcpConnection.join();
                Log.d("TELNET", String.valueOf("Connection close waiting"));
                Log.d("TELNET", String.valueOf("---------------------------------"));
            } catch (InterruptedException e) {
                Log.d("TELNET", e.toString());
            }
        }
    }

    @Subscribe
    public void actionHandle(AppEvents action) {
        switch (action) {
            case NETWORK_AVAILABLE:
                if (!isConnected()) {
                    if (!App.getProfileStringParam("server_addr").isEmpty()) {
                        Log.d("DEBUG", String.valueOf("connect on receiver action"));
                        connectToMachine();
                    }
                }
                break;
            case NETWORK_UNAVAILABLE:
                Log.d("DEBUG", String.valueOf("disconnect on receiver action"));
                closeConnection();
                break;
            case IP_CHANGED:
                Log.d("DEBUG", String.valueOf("IP_CHANGED"));
                closeConnection();
                if (NetworkUtils.isNetworkAvailable()) {
                    connectToMachine();
                }
                break;
        }
    }

    @Subscribe
    public void setParams(HashMap<String, Float> params) {
        if (wheelangleMachine != App.getIntParam("wheelangle") || wheelangleMachine0 != App.getIntParam("wheelangle0") || wheelangleMachine1 != App.getIntParam("wheelangle1") || wheelangleMachine2 != App.getIntParam("wheelangle2")) {
            wheelangleMachine = App.getIntParam("wheelangle");
            wheelangleMachine0 = App.getIntParam("wheelangle0");
            wheelangleMachine1 = App.getIntParam("wheelangle1");
            wheelangleMachine2 = App.getIntParam("wheelangle2");
            if (!isForeground(getPackageName())) {
                Intent i = new Intent();
                i.setClass(App.getContext(), MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
            }
        }
    }

    public static void saveMachineParamValue(String param, String value) {
        curMachineState.put(param, value);
    }

    public static Map<String, String> getCurMachineState() {
        return curMachineState;
    }

    public void addResponseHandler(String key, TCPResponseHandler handler) {
        handlers.put(key, handler);
    }

    public void triggerResponseHandlers(String response) {
        for (Map.Entry<String, TCPResponseHandler> entry : handlers.entrySet()) {
            TCPResponseHandler handler = entry.getValue();
            handler.doAction(response);
        }
    }

    public void removeHandler(String key) {
        if (!handlers.isEmpty()) {
            handlers.remove(key);
        }
    }
}