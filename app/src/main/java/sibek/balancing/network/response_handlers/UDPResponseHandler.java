package sibek.balancing.network.response_handlers;

import java.util.ArrayList;

/**
 * Created by dracula on 30.09.2016.
 */

public interface UDPResponseHandler {
    public void doAction(byte[] response);
}