package sibek.balancing.network.response_handlers;

import android.util.Log;

import java.util.ArrayList;
import java.util.Iterator;

import sibek.balancing.App;
import sibek.balancing.network.MachineValuesCommands;
import sibek.balancing.tools.AppEvents;

/**
 * Created by dracula on 09.11.2016.
 */

public class StaticParamsHandler implements TCPResponseHandler {
    private ArrayList<String> commands;

    public StaticParamsHandler() {
        commands = MachineValuesCommands.getCommands();
    }

    @Override
    public void doAction(String response) {
        if (commands.size() > 0) {
            Iterator<String> i = commands.iterator();
            while (i.hasNext()) {
                String s = i.next();
                if (response.contains(s)) {
                    App.setProfileStringParam(s, response);
                    i.remove();
                    Log.d("StaticParamsHandler", "Command " + String.valueOf(s) + " is saved to preference!");
                }
            }
        } else {
            App.bus.post(AppEvents.MACHINE_VALUES_COLLECTED);
        }
    }
}