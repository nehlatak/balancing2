package sibek.balancing.network.response_handlers;

import android.util.Log;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import sibek.balancing.App;
import sibek.balancing.network.MachineCommands;
import sibek.balancing.tools.AppEvents;

/**
 * Created by dracula on 21.09.2016.
 */

public class CurrentMachineStateHandler implements TCPResponseHandler {
    private ArrayList<String> commands = MachineCommands.getCommands();
    private Map<String, String> results = new HashMap<>();

    @Override
    public void doAction(String response) {
        Log.d("DEBUG", "response = " + String.valueOf(response));
        if (results.size() == commands.size()) {
            String fullResult = "";
            for (Map.Entry<String, String> entry : results.entrySet()) {
                String commandValue = entry.getKey();
                String responseValue = entry.getValue();
                String tempStr = commandValue + "\n" + responseValue + "\n";
                fullResult += tempStr;
            }
            Log.d("DEBUG", "fullResult = " + String.valueOf(fullResult));

            buildStatisticFile(fullResult);

            App.bus.post(AppEvents.STATISTICS_BUILDED);
        }
        for (String command : commands) {
            /*if (command.equals("state")) { //@TODO: хак который нужно будет убрать после изменения state от платы
                command = "params";
            }*/
            if (response.contains(command)) {
                results.put(command, response);
            }
        }
    }

    private void buildStatisticFile(String res) {
        try {
            OutputStream output = new FileOutputStream(App.getStatisticFilePath());
            PrintWriter printWriter = new PrintWriter(output);
            printWriter.write(res);
            printWriter.close();
            Log.d("DEBUG", "file stat is builded!");
        } catch (IOException e) {
            Log.d("InfoFileCreator", e.toString());
        }
    }
}
