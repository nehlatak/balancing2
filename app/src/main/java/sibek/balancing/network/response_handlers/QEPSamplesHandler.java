package sibek.balancing.network.response_handlers;

import android.util.Log;

import java.util.ArrayList;

import sibek.balancing.App;
import sibek.balancing.oscilloscope.Samples;
import sibek.balancing.tools.CommonUtils;

/**
 * Created by dracula on 30.09.2016.
 */

public class QEPSamplesHandler implements UDPResponseHandler {
    private ArrayList<Samples> samplesList = new ArrayList<>();

    @Override
    public void doAction(byte[] response) {
        samplesList.clear();

        int length = (response.length  - 4) * 8;
        for (int i = 0; i < length; i += 3) {
            boolean qep_sample1 = (response[(i + 0) / 8 + 4] & (1 << (i + 0) % 8)) != 0;
            boolean qep_sample2 = (response[(i + 1) / 8 + 4] & (1 << (i + 1) % 8)) != 0;
            boolean qep_sample3 = (response[(i + 2) / 8 + 4] & (1 << (i + 2) % 8)) != 0;

            Samples sm = new Samples();
            float[] samples = new float[3];

            samples[0] = qep_sample1 ? 1000 : 0;
            samples[1] = qep_sample2 ? 1000 : 0;
            samples[2] = qep_sample3 ? 1000 : 0;

            sm.setVertices(samples);

            samplesList.add(sm);
        }

        App.bus.post(samplesList);


        /*int length = (response.length);
        for (int i = 0; i < length - 4; i += 3) {

            int i1 = i + 4;
            int i2 = i + 5;
            int i3 = i + 6;

            Samples sm = new Samples();
            float[] samples = new float[3];

            samples[0] = ((response[i1] & 0x01) != 0) ? 0 : 1000;
            samples[1] = ((response[i1] & 0x02) != 0) ? 0 : 1000;
            samples[2] = ((response[i1] & 0x04) != 0) ? 0 : 1000;

            sm.setVertices(samples);
            samplesList.add(sm);

            samples[0] = ((response[i1] & 0x08) != 0) ? 0 : 1000;
            samples[1] = ((response[i1] & 0x10) != 0) ? 0 : 1000;
            samples[2] = ((response[i1] & 0x20) != 0) ? 0 : 1000;

            sm.setVertices(samples);
            samplesList.add(sm);

            samples[0] = ((response[i1] & 0x40) != 0) ? 0 : 1000;
            samples[1] = ((response[i1] & 0x80) != 0) ? 0 : 1000;
            samples[2] = ((response[i2] & 0x01) != 0) ? 0 : 1000;

            sm.setVertices(samples);
            samplesList.add(sm);

            samples[0] = ((response[i2] & 0x02) != 0) ? 0 : 1000;
            samples[1] = ((response[i2] & 0x04) != 0) ? 0 : 1000;
            samples[2] = ((response[i2] & 0x08) != 0) ? 0 : 1000;

            sm.setVertices(samples);
            samplesList.add(sm);

            samples[0] = ((response[i2] & 0x10) != 0) ? 0 : 1000;
            samples[1] = ((response[i2] & 0x20) != 0) ? 0 : 1000;
            samples[2] = ((response[i2] & 0x40) != 0) ? 0 : 1000;

            sm.setVertices(samples);
            samplesList.add(sm);

            samples[0] = ((response[i2] & 0x80) != 0) ? 0 : 1000;
            samples[1] = ((response[i3] & 0x01) != 0) ? 0 : 1000;
            samples[2] = ((response[i3] & 0x02) != 0) ? 0 : 1000;

            sm.setVertices(samples);
            samplesList.add(sm);

            samples[0] = ((response[i3] & 0x04) != 0) ? 0 : 1000;
            samples[1] = ((response[i3] & 0x08) != 0) ? 0 : 1000;
            samples[2] = ((response[i3] & 0x10) != 0) ? 0 : 1000;

            sm.setVertices(samples);
            samplesList.add(sm);

            samples[0] = ((response[i3] & 0x20) != 0) ? 0 : 1000;
            samples[1] = ((response[i3] & 0x40) != 0) ? 0 : 1000;
            samples[2] = ((response[i3] & 0x80) != 0) ? 0 : 1000;

            sm.setVertices(samples);
            samplesList.add(sm);
        }

        App.bus.post(samplesList);*/
    }
}