package sibek.balancing.network.response_handlers;

/**
 * Created by dracula on 21.09.2016.
 */

public interface TCPResponseHandler {
    public void doAction(String response);
}