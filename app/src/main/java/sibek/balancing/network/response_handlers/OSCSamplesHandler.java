package sibek.balancing.network.response_handlers;

import java.util.ArrayList;

import sibek.balancing.App;
import sibek.balancing.oscilloscope.Samples;

/**
 * Created by dracula on 30.09.2016.
 */

public class OSCSamplesHandler implements UDPResponseHandler {
    private ArrayList<Samples> samplesList = new ArrayList<>();

    @Override
    public void doAction(byte[] response) {
        samplesList.clear();

        for (int i = 4; i < response.length; i += 6) {
            int sample1 = (response[i + 0] & 0xFF) | (response[i + 1] & 0xFF) << 8 | (response[i + 2] & 0x3F) << 16;
            int sample2 = (response[i + 2] >> 6 & 0x03) | (response[i + 3] & 0xFF) << 2 | (response[i + 4] & 0xFF) << 10 | (response[i + 5] & 0x0F) << 18;
            if ((sample1 & 0x00200000) != 0)
                sample1 |= 0xFFC00000;
            if ((sample2 & 0x00200000) != 0)
                sample2 |= 0xFFC00000;

            Samples sm = new Samples();
            float[] samples = new float[2];

            samples[0] = sample1;
            samples[1] = sample2;

            sm.setVertices(samples);

            samplesList.add(sm);
        }

        App.bus.post(samplesList);
    }
}