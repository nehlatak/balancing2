package sibek.balancing.network.response_handlers;

import android.os.Handler;
import android.os.Looper;
import android.support.v4.util.Pair;
import android.util.Log;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import sibek.balancing.App;
import sibek.balancing.network.ConnectionService;
import sibek.balancing.router.machine_vars.VarsParams;
import sibek.balancing.router.VarsRouter;
import sibek.balancing.tools.TypesParams;

/**
 * Created by dracula on 21.09.2016.
 */

public class CurrentRouteResponseHandler implements TCPResponseHandler {
    private HashMap<String, Pair> paramsMap = new HashMap<>();
    private VarsRouter router = VarsRouter.getInstance();
    private Handler mainThread = new Handler(Looper.getMainLooper());
    private String curCommand = "";

    @Override
    public void doAction(String response) {
        Log.i("CurrentRouteResponse", "New response has come!");
//        Log.d("CurrentRouteResponse", String.valueOf(response));
        VarsParams routeVarsParams = router.getRouteVars();

        List<String> varParams = routeVarsParams.getParams();
        String command = routeVarsParams.getCommand();
        Integer[] floatParams = routeVarsParams.getFloatParams();

        if (!curCommand.equals(command)) {
            curCommand = command;
            paramsMap = new HashMap<>();
        }

        /*if (command.equals("state")) {//@TODO:хак - нужно переделать ответ с платы
            command = "params";
        }*/

        if (response.contains(command)) {
            ConnectionService.saveMachineParamValue(command, response);
            String[] responseChunks = response.split(command);

            Log.d("CurrentRoute", "varsValues = " + String.valueOf(Arrays.asList(responseChunks)));

            for (String chunk : responseChunks) {
                String[] varsValues = (chunk.trim()).split(" ");

                if (varsValues.length > 1) {
                    /*for (int varValuesCount = 0; varValuesCount < varsValues.length; varValuesCount++) {
                        try {
                            if (App.isExists(floatParams, varValuesCount)) {
                                Pair<TypesParams, Float> parameter = new Pair<>(TypesParams.FLOAT, Float.parseFloat(varsValues[varValuesCount]));
                                paramsMap.put(varParams.get(varValuesCount), parameter);
                            } else {
                                Pair<TypesParams, Integer> parameter = new Pair<>(TypesParams.INT, Integer.parseInt(varsValues[varValuesCount]));
                                paramsMap.put(varParams.get(varValuesCount), parameter);
                            }
                        } catch (NumberFormatException ex) {
                            Log.w("CurrentRouteResponse", String.valueOf("Wrong string from machine!"));
                            Log.w("CurrentRouteResponse", "Machine answer: \"" + response + "\"");
                        }
                    }*/

                    int index = 0;
                    for (String varsValue : varsValues) {
                        try {
                            if (App.isExists(floatParams, index)) {
                                Pair<TypesParams, Float> parameter = new Pair<>(TypesParams.FLOAT, Float.parseFloat(varsValue));
                                paramsMap.put(varParams.get(index), parameter);
                            } else {
                                Pair<TypesParams, Integer> parameter = new Pair<>(TypesParams.INT, Integer.parseInt(varsValue));
                                paramsMap.put(varParams.get(index), parameter);
                            }
                        } catch (NumberFormatException ex) {
                            Log.w("CurrentRouteResponse", String.valueOf("Wrong string from machine!"));
                            Log.w("CurrentRouteResponse", "Machine answer: \"" + response + "\"");
                        }

                        index++;
                    }


                    mainThread.post(new Runnable() {
                        @Override
                        public void run() {
                            Log.i("CurrentRouteResponse", "Post paramsMap to Main Thread!");
                            App.bus.post(paramsMap);
                        }
                    });
                }
            }
        } else {
            Log.w("CurrentRouteResponse", "Machine answer does not have cur command: \"" + response + "\"");
        }
    }
}